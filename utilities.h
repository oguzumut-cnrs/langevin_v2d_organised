
#pragma once
#include "structures.h"
#include "namespaces.h"
#include "environement.h"

#include "input_output.h"
#include "principals.h"
#include "timer.h"
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include "ap.h"


void box_calculations(tensors& F);

void initiate_tensors(tensors& F);
int check_tensors(tensors& F);

string IntToStr(int n);
string IntToStr_normal(int n) ;
string RealToStr_normal(double n) ;

void write_to_a_file(coordinates& c, int t);


void write_to_a_file_en(coordinates& c, forces_std& fo, int t);




void write_to_a_file_en_2(coordinates& c, forces& fo, int t);

void write_to_a_file_en_3(coordinates& c,  double &func, alglib::real_1d_array &grad, int t);

void read_from_a_file(coordinates& c);


//DEVELOP MORE HERE
///////////////////
void read_from_a_file_lengths(tensors& F);




double maxval(std::vector<double>& m);

double minval(std::vector<double>& m);
std::vector<double> sum_of_sq_mn(std::vector<double>& m,std::vector<double>& n);

void test_knn2(coordinates& c,int nn);

bool fexists(const char *fileName);
void read_input_file();
	

struct shearing_vectors create_vectors(const struct coordinates& c, double angle);
struct tensors  shear_using_dual_lattice(const struct shearing_vectors& base1, double alpha);


void coarse_grain(tilde_coordinates& tilde_c,coordinates& c, double angle);
std::vector<double>  wiener(boost::mt19937& );
double coarse_grain_spline(tilde_coordinates& ,coordinates& ,  matrix2D& );