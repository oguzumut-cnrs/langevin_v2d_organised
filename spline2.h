#pragma once
#include <iostream>
#include "structures.h"
#include "timer.h"
#include "dlib.h"
#include "reductions.h"
#include "utilities.h"
#include "datatable.h"
#include "bspline.h"
using std::cout;
using std::endl;
// #include "/Users/salman/splinter/splinter/src/bspline.cpp"
// #include "/Users/salman/splinter/splinter/src/bsplinebasis.cpp"
// #include "/Users/salman/splinter/splinter/src/bsplinebasis1d.cpp"
// #include "/Users/salman/splinter/splinter/src/bsplinebuilder.cpp"
// #include "/Users/salman/splinter/splinter/src/datapoint.cpp"
// #include "/Users/salman/splinter/splinter/src/datatable.cpp"
// #include "/Users/salman/splinter/splinter/src/function.cpp"
// #include "/Users/salman/splinter/splinter/src/knots.cpp"
// #include "/Users/salman/splinter/splinter/src/mykroneckerproduct.cpp"
// #include "/Users/salman/splinter/splinter/src/serializer.cpp"
// #include "/Users/salman/splinter/splinter/src/utilities.cpp"

#include <bsplinebuilder.h>

using namespace SPLINTER;

double energy_spline(struct  cella_stru& cr );



double c11_spline(struct  cella_stru& cr );

double c22_spline(struct  cella_stru& cr );


double c12_spline(struct  cella_stru& cr );

BSpline spline(tilde_coordinates& tilde_c,coordinates& c);


void coo_func_for_splines(double& theta);

struct eu_stru eigen_energy(tilde_coordinates& ,coordinates& ,matrix2D& ); 

double energy_withF(tilde_coordinates&,coordinates&,matrix2D& );