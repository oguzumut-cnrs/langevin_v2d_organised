
#include "common3_alglib.h"
#include "ap.h"

// #include "structures.h"


void function1_grad(const alglib::real_1d_array &m, double &func, alglib::real_1d_array &grad, void *ptr) 

// double rosen_relax (const alglib::real_1d_array &m, double &func, alglib::real_1d_array &grad, void *ptr)
/*
	This function computes the function; i.e. the total energy. */
{

	int n=atom_number::n_atom;
	tilde_coordinates tilde_c;
	static double ener=0;
	double ener_t1;
	static double delta_ener=0;
	tilde_c.x.resize(n);
	tilde_c.y.resize(n);
	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 
	
	static int step = 1;
	static int step2 = 0;
	static int t = 0;


	for(int i=0;i<n;i++)
	{
		tilde_c.x[i] = m[i];
		tilde_c.y[i]  =m[i+n];	
	}
	
	tensors F;
	initiate_tensors(F);


	force_energy_relax::calculation::getInstance().forces_with_nbl_CG(tilde_c,F);	
	double dt = simulation_parameters::dt;
	double ener2 = 0;
	
	for(int i=0;i<n;i++)
	{
// 		if(force_energy::calculation::getInstance().c.boundary[i]==1)
// 			continue;
		ener2+=0.5*force_energy_relax::calculation::getInstance().fo.e(i);
		
		
		grad[i]  =  -force_energy_relax::calculation::getInstance().fo.x(i);
		grad[i+n]=  -force_energy_relax::calculation::getInstance().fo.y(i);
		
		if(force_energy::calculation::getInstance().c.boundary[i]==1){
			grad[i]  = 0;
			grad[i+n]  = 0;
		}

	}

		func = ener2;
		if(step%100 ==0 ){
			
			double grad_sum=0.;
			
			for(int i=0;i<n;++i)
				grad_sum += pow(grad[i],2) + pow(grad[i+n],2);

			cout<< std::scientific << std::setprecision(16)<<"function value inside LFBGS: "<<func
			<<" grad_sum value inside LFBGS: "<<grad_sum<<endl;
// 			coordinates c;
// 			c.x.resize(0);
// 			c.y.resize(0);
			transform_to_original_coordinates(force_energy::calculation::getInstance().c,tilde_c,F);
			write_to_a_file_en_3(force_energy::calculation::getInstance().c,func,grad,step2);
			
			step =1;
			step2++;
		}
		
		
	//step2++;
    ++step;

		
// 	return ener2/(1.0*n);
}






