#include "forces.h"
using namespace SPLINTER;

// double lennard_jones_energy(double r)
// {
// 	int it=0;
// 	int jt=0;
// 	namespace lj = lennard_jones;
// 	
// 	double n_sur_m = (double) lj::nn/  (double)lj::mm;
// 	n_sur_m=2;
// // 	double energy=0;
// 	
// 
// 	matrix2D a;
// 	matrix2D r0;
// 
// 
// 	a(0,0) = lj::a_11;
// 	a(0,1) = lj::a_12;
// 	a(1,0) = lj::a_12;
// 	a(1,1) = lj::a_22;
// 
// 	r0(0,0) = lj::r0_11;
// 	r0(0,1) = lj::r0_12;
// 	r0(1,0) = lj::r0_12;
// 	r0(1,1) = lj::r0_22;
// 
// // 	double energy = a(it,jt) *(pow((r0(it,jt)/r),lj::nn)- n_sur_m*pow((r0(it,jt)/r),lj::mm));
// 	double energy = pow(1./r,8.)- 2.*pow((1./r),4);
// // 	DenseVector x(1);
// // 	x(0)=r;
// // 	double energy =  bspline_lj.eval(x);
// // 	double energy = g_s(r);
// 
// 	return energy;
// 
// }
// 
// 
// 
// 
// double lennard_jones_force(double r)
// {
// 	namespace lj = lennard_jones;
// 	
// 	int it=0;
// 	int jt=0;
// 	matrix2D a;
// 	matrix2D r0;
// 	a(0,0) = lj::a_11;
// 	a(0,1) = lj::a_12;
// 	a(1,0) = lj::a_12;
// 	a(1,1) = lj::a_22;
// 
//     r0(0,0) = lj::r0_11;
// 	r0(0,1) = lj::r0_12;
// 	r0(1,0) = lj::r0_12;
// 	r0(1,1) = lj::r0_22;
// 	
// 
// // 	double f0 = lj::nn*a(it,jt) * ( pow((r0(it,jt)/r),lj::nn-1) -pow((r0(it,jt)/r),lj::mm-1)  )*(r0(it,jt)/(r*r*r));
// 	double f0 = 8.* ( pow(1./r,7.) -pow(1./r,3)  )/pow(r,3.);
// // 	double f0 = -g_s.deriv(1,r)/r;
// 
// // 	DenseVector x(1);
// // 	x(0)=r;
// // 	DenseMatrix jac = bspline_lj.evalJacobian(x);
// // 	double f0 = -jac(0)/r;
//  
// 	return f0;
// //    f0 = nn*a(it,jt) * ( (r0(it,jt)/r)**(nn-1) - (r0(it,jt)/r)**(mm-1) ) * r0(it,jt)/r**3
// //    e0 =    a(it,jt) * ( (r0(it,jt)/r)**nn - n_sur_m*((r0(it,jt)/r)**mm ) )
// 
// }


void forces_with_nbl(tilde_coordinates&  tilde_c,forces_std& fo,tensors& F )
{
	double r_cutoff_sq = pow(info_2D::r_cutoff,2.0);
	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 
	int n_atom = atom_number::n_atom;
	double e0,f0,r2,r;


// ----- initialisation virials -----

	F.virial(0,0) = 0;F.virial(1,0) = 0;
	F.virial(0,1) = 0;F.virial(1,1) = 0;

// ----- calcul des forces -------------------------

// 				int nn;
// 				double mm;
// 				int n_sur_m = nn/float(mm);
	
// 	cout<<"inside forces_with_nbl"<<"\n";
	


	for(int k=0;k<fo.x.size();k++)   
		fo.x[k]=0. ;	
	for(int k=0;k<fo.y.size();k++)   
		fo.y[k]=0. ;	
	for(int k=0;k<fo.e.size();k++)   
		fo.e[k]=0. ;	
	
// 	cout << "1. size: " << fo.x.size() << '\n';
// 	cout << "2. size: " << fo.y.size() << '\n';
// 	cout << "3. size: " << fo.e.size() << '\n';
// 	cout << "4. size: " << pairing::n_pair << '\n';




	for(int n=0;n<pairing::n_pair;n++)
	{


		int i = environment::Parameters::getInstance().nbl_i[n] ;
		int j = environment::Parameters::getInstance().nbl_j[n] ;
		
// 					cout<<"inside forces_with_nbl i,j = "<<i<<" "<<j<<" "<<"\n";
// 					cout<<"inside forces_with_nbl j = "<<j<<"\n";


		double dx_tilde = tilde_c.x[i] - tilde_c.x[j];
		double dy_tilde = tilde_c.y[i] - tilde_c.y[j];

		if(abs(dx_tilde) > 0.5) 
			dx_tilde -=   copysign(1., dx_tilde) ;
		if(abs(dy_tilde) > 0.5) 
			dy_tilde -=   copysign(1., dy_tilde) ;

		double dx = F.grad_def(0,0)*length0_x*dx_tilde + F.grad_def(0,1)*length0_y*dy_tilde ;
		double dy = F.grad_def(1,0)*length0_x*dx_tilde + F.grad_def(1,1)*length0_y*dy_tilde ;
		r2 =  dx*dx + dy*dy ;

	if( r2 >= r_cutoff_sq ) continue;
// 		if( r2 < r_cutoff_sq )
// 		{
			 r = sqrt(r2);

			//    it = species(i)
			//    jt = species(j)
// 			namespace lj = lennard_jones;
			//    
			f0 =  environment::Parameters::lennard_jones_force (r);
			e0 =  environment::Parameters::lennard_jones_energy(r);


			double dfx = f0 * dx;
			double dfy = f0 * dy;

			//    !---- forces -----

			fo.x[i] += dfx;
			fo.y[i] += dfy;

			fo.x[j] -= dfx;
			fo.y[j] -= dfy;

			fo.e[i] += e0;
			fo.e[j] += e0;

			//    !--- virials -----

			F.virial(0,0) += dfx*dx_tilde*length0_x;  
			F.virial(0,1) += dfx*dy_tilde*length0_y;  

			F.virial(1,0) += dfy*dx_tilde*length0_x;  
			F.virial(1,1) += dfy*dy_tilde*length0_y; 
// 		}
	}

	//spatial integration of energy
// 	fo.sum_ener = std::accumulate(fo.e.begin(),fo.e.end(),0);
// 	cout<<"fo.sum forces "<<fo.sum_ener<<"\n";


}	

void forces_with_nbl_par(tilde_coordinates&  tilde_c,forces_std& fo,tensors& F )
{

	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 
	int n_atom = atom_number::n_atom;
// 				double e0,f0,r2,r;
	double inv_visco_x = 1.0/pow(length0_x,2.);  // new
	double inv_visco_y = 1.0/pow(length0_y,2.);  // new
	double dt=simulation_parameters::dt;
	double temperature=simulation_parameters::temperature;
	double amp_noise = sqrt(2.*dt*temperature); //(24.*dt*temperature);





// ----- initialisation virials -----

	F.virial(0,0) = 0;F.virial(1,0) = 0;
	F.virial(0,1) = 0;F.virial(1,1) = 0;

// ----- calcul des forces -------------------------

	forces_std  f_temp_par;


	f_temp_par.x.resize( pairing::n_pair );
	f_temp_par.y.resize( pairing::n_pair );
	f_temp_par.e.resize( pairing::n_pair );


	for(int k=0;k<fo.x.size();k++)
	{   
		fo.x[k]=0. ;	
		fo.y[k]=0. ;	
		fo.e[k]=0. ;
	}

	int n_threads=simulation_parameters::n_threads;
	parallel_for(n_threads, 0, pairing::n_pair, [&](long n){
	double e0,f0,r2,r;
	double r_cutoff_sq = pow(info_2D::r_cutoff,2.0);




// 				for(int n=0;n<pairing::n_pair;n++)
// 				{
// 					cout<<"inside forces_with_nbl n = "<<n<<"\n";
	// int i = cell.nbl_i(n);
	// int j = cell.nbl_j(n);
	
	

		int i = environment::Parameters::getInstance().nbl_i[n] ;
		int j = environment::Parameters::getInstance().nbl_j[n] ;
		
// 					cout<<"inside forces_with_nbl i,j = "<<i<<" "<<j<<" "<<"\n";
// 					cout<<"inside forces_with_nbl j = "<<j<<"\n";


		double dx_tilde = tilde_c.x[i] - tilde_c.x[j];
		double dy_tilde = tilde_c.y[i] - tilde_c.y[j];

		if(abs(dx_tilde) > 0.5) 
			dx_tilde -=   copysign(1., dx_tilde) ;
		if(abs(dy_tilde) > 0.5) 
			dy_tilde -=   copysign(1., dy_tilde) ;

		double dx = F.grad_def(0,0)*length0_x*dx_tilde + F.grad_def(0,1)*length0_y*dy_tilde ;
		double dy = F.grad_def(1,0)*length0_x*dx_tilde + F.grad_def(1,1)*length0_y*dy_tilde ;
		r2 =  dx*dx + dy*dy ;

// 	if( r2 >= r_cutoff_sq ) continue;
		if( r2 < r_cutoff_sq )
		{
			 r = sqrt(r2);

			//    it = species(i)
			//    jt = species(j)
// 			namespace lj = lennard_jones;
			//    
			f0 =  environment::Parameters::lennard_jones_force (r);
			e0 =  environment::Parameters::lennard_jones_energy(r);
			double dfx = f0 * dx;
			double dfy = f0 * dy;

			//    !---- forces -----

// 						fo.x(i) += dfx;
// 						fo.y(i) += dfy;
// 
// 						fo.x(j) -= dfx;
// 						fo.y(j) -= dfy;
// 
// 						fo.e(i) += e0;
// 						fo.e(j) += e0;

			f_temp_par.x[n] = dfx;
			f_temp_par.y[n] = dfy;
			f_temp_par.e[n] = e0;



			//    !--- virials -----

// 						F.virial(0,0) += dfx*dx_tilde*length0_x;  
// 						F.virial(0,1) += dfx*dy_tilde*length0_y;  
// 
// 						F.virial(1,0) += dfy*dx_tilde*length0_x;  
// 						F.virial(1,1) += dfy*dy_tilde*length0_y; 
		}
	//}
	 });
	 
	 
	for(int n=0;n<pairing::n_pair;n++)
	{
	   int i = environment::Parameters::getInstance().nbl_i[n] ;
	   int j = environment::Parameters::getInstance().nbl_j[n] ;

	   fo.x[i]  += f_temp_par.x[n];
	   fo.y[i]  += f_temp_par.y[n];

	   fo.x[j]  -= f_temp_par.x[n];
	   fo.y[j]  -= f_temp_par.y[n];

	   fo.e[i]  += f_temp_par.e[n];   
	   fo.e[i]  += f_temp_par.e[n];   
	   
	}
   


}		
	
#include <random>
	
tilde_coordinates new_coordinates_2D(tilde_coordinates&  tilde_c,
forces_std& fo, 
random_forces_std& rf, 
tilde_coordinates&  cumul,
tensors& F,
const double dt,
const double temperature )
{

	int n_atom = atom_number::n_atom;
	double x = simulation_parameters::dt_predictor;
	double dt_in=simulation_parameters::dt/x;
	double amp_noise = sqrt(2.*dt*temperature); //(24.*dt*temperature);
	double length0_x=lengths::length0_x;
	double length0_y=lengths::length0_y;
	//for stcohastic term
	
	
	tilde_coordinates  c_tilde_new;
	forces_std  f_temp;
	
    c_tilde_new.x.resize( n_atom );
    c_tilde_new.y.resize( n_atom );

    f_temp.x.resize( n_atom );
    f_temp.y.resize( n_atom );

//   for(int i=0;i<n_atom;i++) c_tilde_new.x[i]=0;
//   for(int i=0;i<n_atom;i++) c_tilde_new.y[i]=0;
//!------ time step on atom coordinates -----


	double inv_visco_x = 1.0/pow(length0_x,2.);  // new
	double inv_visco_y = 1.0/pow(length0_y,2.);  // new
	
//     std::random_device rd;
//     std::mt19937 gen(rd());
//     std::uniform_real_distribution<> dis(0, 1);	
//     for (int n = 0; n < n_atom; ++n) {
//         rf.x[n]  = dis(gen) ;
//         rf.y[n]  = dis(gen) ;
//     }
	

	for(int i=0;i<n_atom;i++)
	{ 
		
		
// 		f_temp.x[i] = inv_visco_x * ( fo.x[i]*F.grad_def(0,0)*length0_x     
// 								    + fo.y[i]*F.grad_def(1,0)*length0_x ) ;
// 		f_temp.y[i] = inv_visco_y * ( fo.x[i]*F.grad_def(0,1)*length0_y     
// 								    + fo.y[i]*F.grad_def(1,1)*length0_y ) ; 
// 				          
// 
// 		double dx_tilde =   f_temp.x[i]*dt  + sqrt(inv_visco_x) * amp_noise*(rf.x[i]-0.5) ;                       
// 
// 		double dy_tilde =   f_temp.y[i]*dt  + sqrt(inv_visco_y) * amp_noise*(rf.y[i]-0.5) ;  
		
		double dx_tilde =   inv_visco_x * ( fo.x[i]*F.grad_def(0,0)*length0_x*dt     
								          + fo.y[i]*F.grad_def(1,0)*length0_x*dt )   
				          + sqrt(inv_visco_x) * amp_noise*(rf.x[i]-0.0) ;                       

		double dy_tilde =   inv_visco_y * ( fo.x[i]*F.grad_def(0,1)*length0_y*dt     
								          + fo.y[i]*F.grad_def(1,1)*length0_y*dt ) 
				          + sqrt(inv_visco_y) * amp_noise*(rf.y[i]-0.0) ;                
		  
		
		c_tilde_new.x[i] = tilde_c.x[i]+ dx_tilde ;
		c_tilde_new.y[i] = tilde_c.y[i]+ dy_tilde ;

// 		if ( c_tilde_new.x[i]  < 0 )  
// 			 c_tilde_new.x[i] +=  1.0;
// 		
// 		if ( c_tilde_new.x[i] >= 1 )  
// 			 c_tilde_new.x[i] -=  1.0;
// 
// 		if ( c_tilde_new.y[i] < 0 )  
// 			 c_tilde_new.y[i] += 1.0;
// 			
// 		if ( c_tilde_new.y[i] >= 1 )  
// 			 c_tilde_new.y[i] -= 1.0;

// 		cumul.x[i] += F.grad_def(0,0)*length0_x*dx_tilde + F.grad_def(0,1)*length0_y*dy_tilde ;
// 		cumul.y[i] += F.grad_def(1,0)*length0_x*dx_tilde + F.grad_def(1,1)*length0_y*dy_tilde ;
				
             
	}

// 	fo.x  = f_temp.x;
// 	fo.y  = f_temp.y;
	
// 	return c_tilde_new;
// 	for(int i=0;i<n_atom;i++)
// 	{ 
// 		tilde_c.x[i]= c_tilde_new.x[i];
// 		tilde_c.y[i]= c_tilde_new.y[i];	
// 	}
	return c_tilde_new;

}

