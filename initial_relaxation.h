// 	
// 	
// 	//WRITE INITIAL CONDITION

#pragma once
#ifndef  _INITIAL_RELAXATION_H_
#define  _INITIAL_RELAXATION_H_
#include "structures.h"
#include "timer.h"
#include "dlib.h"
#include "namespaces.h"
#include "common2.h"
#include "common3_alglib.h"

#include "utilities.h"
void initial_relaxation(coordinates& c,tilde_coordinates& tilde_c);
void  homo_loading_relaxation(coordinates& c,tilde_coordinates& tilde_c,tilde_coordinates& tilde_cfix,struct tensors& F);
void copy_m_to_tilde(column_vector& m, tilde_coordinates& tilde_c);
void copy_m_to_tilde_alglib(const alglib::real_1d_array& m , tilde_coordinates& tilde_c);
// void apply_BC(struct conf_stru& c, double& load);
#endif




