#include "namespaces.h"

int dimensions::nx;
int dimensions::ny;
int dimensions::nz;

void dimensions::setNum(int value1, int value2, int value3 )
{
   nx=value1;
   ny=value2;
   nz=value3;
} 


   double simulation_parameters::temperature;
   double simulation_parameters::dt;
   double simulation_parameters::dt_predictor;
   int simulation_parameters::save_period;
   int simulation_parameters::n_threads;


void simulation_parameters::setNum(double value1,double value2,double value3,int value4,int value5)
{
   temperature=value2;
   dt=value1;
   dt_predictor=value3;
   save_period =value4;
   n_threads =  value5;
}

int simulation_duration::n_duration;

void simulation_duration::setNum(int value1)
{
	n_duration=value1;	
}
 
string memory_file::answer;
string memory_file::filename_lengths;
string memory_file::filename;
string memory_file::filename_graddef;
void memory_file::setName(string value1, string value2,string value3, string value4)
{
   answer  =value1;
   filename=value2;
   filename_lengths =value3;
   filename_graddef =value4;
     
} 

string numerical_method::answer;
string numerical_method::answer2;


void numerical_method::setName(string value1,string value2)
{
   answer  =value1;
   answer2 = value2;
} 


double  lengths::length0_x;
double  lengths::length0_y;
void lengths::setNum(double value1,double value2)
{
   length0_x=value1;
   length0_y=value2;
} 

 
int  atom_number::n_atom;


void atom_number::setNum(int value1)
{
   n_atom=value1;
} 
 
int  pairing::n_pair; 

void pairing::setNum(int value1)
{
   n_pair=value1;
} 




	 double  box_informations::cell_length_x_tilde;
     double  box_informations::cell_length_y_tilde;
     double  box_informations::cell_length_x;
     double  box_informations::cell_length_y;
     int     box_informations::nb_cell_x;
     int     box_informations::nb_cell_y;
     int     box_informations::n_atom_per_cell;
     int     box_informations::n_pair_max;


void box_informations::setNum_cell(double value1,double value2)
{
	cell_length_x_tilde=value1;
	cell_length_y_tilde=value2;
} 
 
void box_informations::setNum_cell_nb(int value1,int value2,int value3,int value4)
{
	nb_cell_x       =value1;
	nb_cell_y       =value2;
	n_atom_per_cell =value3;
	n_pair_max      =value4;
} 





	 double      info_2D::d1 ;
	 double      info_2D::d2 ; 
	 double      info_2D::r_cutoff;
	 double      info_2D::r_skin;
	 double      info_2D::r_verlet ;
	 double      info_2D::cell_length;
	 int         info_2D:: n_pair_max_per_atom;

  

void info_2D::setNum(double value1,double value2)
{
	r_skin   = value1;
	r_cutoff = value2;

    r_verlet    = r_cutoff + r_skin;
	cell_length = r_verlet*1.05;
	
// 	d1 = 1.;
//     d2 = sqrt(3.)/2.; 
    n_pair_max_per_atom=100;
	double gamma = 1.;//pow((4./3.),(1./4.));
	 d1 = gamma;
	 d2 = gamma*(sqrt(3.0)/2.0);

} 


	 int lennard_jones::nn;
	 int lennard_jones::mm;
	 double lennard_jones::n_sur_m ;
	 double lennard_jones::a_11;
	 double lennard_jones::a_12;
	 double lennard_jones::a_22;
	 double lennard_jones::a_21;

	 double lennard_jones::r0_11;
	 double lennard_jones::r0_12;
	 double lennard_jones::r0_21;
	 double lennard_jones::r0_22;
	 
	 void lennard_jones::setNum(){
	  nn =8;
	  mm =4;
	  n_sur_m = (double) nn / (double)mm;
	  a_11=1.;
	  a_12=1.;
	  a_21=1.;
	  a_22=1.;

	  r0_11=1.;
	  r0_12=1.;
	  r0_21=1.;
	  r0_22=1.; 
	 
	 }

inline bool SameSign(float a, float b) 
{
    return (bool)signbit(a) == (bool)signbit(b);
}


// 	double  grain_properties::theta;
// 	double  grain_properties::theta_rd;
// 	double  grain_properties::shrink;
// 	double  grain_properties::grain_radius;     







// read(10,*) tamp,ensemble
// read(10,*) tamp,rot_free