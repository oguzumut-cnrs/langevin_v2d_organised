#include <iostream>
#include "structures.h"
#include "spline2.h"
#include "datatable.h"
#include "bspline.h"
using std::cout;
using std::endl;
// #include "/Users/salman/splinter/splinter/src/bspline.cpp"
// #include "/Users/salman/splinter/splinter/src/bsplinebasis.cpp"
// #include "/Users/salman/splinter/splinter/src/bsplinebasis1d.cpp"
// #include "/Users/salman/splinter/splinter/src/bsplinebuilder.cpp"
// #include "/Users/salman/splinter/splinter/src/datapoint.cpp"
// #include "/Users/salman/splinter/splinter/src/datatable.cpp"
// #include "/Users/salman/splinter/splinter/src/function.cpp"
// #include "/Users/salman/splinter/splinter/src/knots.cpp"
// #include "/Users/salman/splinter/splinter/src/mykroneckerproduct.cpp"
// #include "/Users/salman/splinter/splinter/src/serializer.cpp"
// #include "/Users/salman/splinter/splinter/src/utilities.cpp"

#include <bsplinebuilder.h>

using namespace SPLINTER;

double energy_spline(struct  cella_stru& cr )
{
	double en;
	const char *fileName = "/Users/salman/splines/en.bspline";
	static BSpline energy(fileName);
	double sqdetc;
	sqdetc = sqrt(cr.c11*cr.c22-cr.c12*cr.c12);
	DenseVector x(2);
	x(0) = cr.c22/sqdetc;
	x(1) = cr.c12/sqdetc;

	en = energy.eval(x)   ;
	return en;

}


double c11_spline(struct  cella_stru& cr )
{
	double c11;
	const char *fileName = "/Users/salman/splines/c11.bspline";
	static BSpline energy(fileName);
	double sqdetc;
	sqdetc = sqrt(cr.c11*cr.c22-cr.c12*cr.c12);
	DenseVector x(2);
	x(0) = cr.c22/sqdetc;
	x(1) = cr.c12/sqdetc;

	c11 = energy.eval(x)   ;
	return c11;

}

double c22_spline(struct  cella_stru& cr )
{
	double c22;
	const char *fileName = "/Users/salman/splines/c22.bspline";
	static BSpline energy(fileName);
	double sqdetc;
	sqdetc = sqrt(cr.c11*cr.c22-cr.c12*cr.c12);
	DenseVector x(2);
	x(0) = cr.c22/sqdetc;
	x(1) = cr.c12/sqdetc;

	c22 = energy.eval(x)   ;
	return c22;

}


double c12_spline(struct  cella_stru& cr )
{
	double c12;
	const char *fileName = "/Users/salman/splines/c12.bspline";
	static BSpline energy(fileName);
	double sqdetc;
	sqdetc = sqrt(cr.c11*cr.c22-cr.c12*cr.c12);
	DenseVector x(2);
	x(0) = cr.c22/sqdetc;
	x(1) = cr.c12/sqdetc;

	c12 = energy.eval(x)   ;
	return c12;

}

BSpline spline(tilde_coordinates& tilde_c,coordinates& c)
{
    
    struct cella_stru metric,s;
    
    
    // Create new DataTable to manage samples
    DataTable samples,samples2,samples3;

    // Sample the function
    DenseVector x(2);
    double y;
    double burgers =1.;
    struct  cella_stru metricr;
//     std::vector<double>  keep;
//     keep.resize(0);

    
//     for(int k = 0; k < 100; k++)
//     {
    	cout<<"meshing"<<endl;
    	for(int i = 0; i < 100; i++)
    	{   
    		for(int j = 0; j < 150; j++)
        	{
        
				// Sample function at x
				x(0) = .5 + i*0.02;
				x(1) =    j*0.005;
// 				x(0) = 1.+ x(1)*x(1)  ;
// 				x(2) = (1.+ x(1)*x(1))/x(0);
				metric.c12 = x(1);
				metric.c22 = x(0);
	 			metric.c11 = (1. + x(1)*x(1))/x(0);
	 			///not really understood
// 			    metricr =  riduci(metric);
			    metricr =  metric;

		        matrix2D cc;
// 		    	matrix<double> eigenvalues(2,1);
		    	cc(0,0)=metricr.c11;
		    	cc(0,1)=metricr.c12;
		    	cc(1,0)=metricr.c12;
		    	cc(1,1)=metricr.c22;
				
				struct eu_stru var=eigen_energy(tilde_c,c,cc);

				samples.addSample(x,var.energy);
        }
    }

//     BSpline bspline3 = BSpline::Builder(samples).degree(3).build();
    cout<<"interpolation"<<endl;
    BSpline bspline3 = BSpline::Builder(samples).degree(3).build();
    fstream spline_stream;
    string filename = "spline_energy";
	cout<<"writing"<<endl;
	spline_stream.open (filename, fstream::in | fstream::out | fstream::app);
    struct shearing_vectors base1= create_vectors(c,0.);
	double alpha=-1;
	   		
	   			   

// 				for(int j = 0; j < 500; j++)
				while(alpha<1.)
				{
		
					tensors F = shear_using_dual_lattice(base1,alpha);
					matrix2D cg,flocal;
					cg = trans(F.grad_def)*F.grad_def;
					// Sample function at x
					int i=0;
					x(1) =   cg(0,1);
					x(0) =   cg(1,1) ;
										
 
					metric.c22 = x(0);
					metric.c12 = x(1);
		 			metric.c11 = cg(0,0);

			   		

				    matrix2D cc;
		    		cc(0,0)=F.grad_def(0,0);
		    		cc(0,1)=F.grad_def(0,1);
		    		cc(1,0)=F.grad_def(1,0);
		    		cc(1,1)=F.grad_def(1,1);
		    		
					double y=energy_withF(tilde_c,c,cc);
					
					metricr =  riduci(metric);
					x(1) = metricr.c12;
					x(0) = metricr.c22;
					spline_stream<<alpha<< " "  << bspline3.eval(x) <<" "<< y<<endl;
					alpha+=0.025;
				}

			spline_stream.close();
// 	double wall0;
// 	double cpu0 ;
// 	double wall1;
// 	double cpu1 ;
// 	double sum=0;
// 	double sum2=0;
// 
// 
// 
// 	cout << "--------------------"<<endl;
//     
// 	wall0 = get_wall_time(); cpu0  = get_cpu_time();
//     cout << "Function at x:                 " << energy_zanzotto(cr,burgers)               << endl;
// 	wall1 = get_wall_time(); cpu1  = get_cpu_time();
// 	
// 	cout << "ANALYTICS :"<<endl; 
// 	cout << "--------------------"<<endl;
// 	cout << std::scientific << std::setprecision(7)<< "Wall Time = " << wall1 - wall0 << endl;
// 	cout << std::scientific << std::setprecision(7)<< "CPU Time  = " << cpu1  - cpu0  << endl;
// 	cout << "--------------------"<<endl;
//  
//  	wall0 = get_wall_time(); cpu0  = get_cpu_time();
//     cout << "Cubic B-spline at x:           " << bspline3.eval(x)   << endl;
//  	wall1 = get_wall_time(); cpu1  = get_cpu_time();
// 	
// 	cout << "SPLINES :"<<endl; 
// 	cout << "--------------------"<<endl;
// 	cout << std::scientific << std::setprecision(7)<< "Wall Time = " << wall1 - wall0 << endl;
// 	cout << std::scientific << std::setprecision(7)<< "CPU Time  = " << cpu1  - cpu0  << endl;
// 
//     cout << "-----------------------------------------------------" << endl;
// 
// 
	const char *fileName = "enlin.bspline";
	bspline3.save(fileName);
	BSpline loadedBSpline(fileName);




 return loadedBSpline;
}

struct eu_stru eigen_energy(tilde_coordinates& tilde_c ,coordinates& c,matrix2D& cc ){

	struct eu_stru euvar;
	eigenvalue_decomposition<matrix<double> > eig(cc);
	matrix2D Diag=diagm(eig.get_real_eigenvalues());
	// 				cout<<"CC eigenvalues"<< Diag(0,0)<< " "<<Diag(0,1)<< " "<<Diag(1,0)<<" "<<Diag(1,1)<<endl;  
	matrix2D SqrtDiag;
	SqrtDiag(0,0)=sqrt(Diag(0,0));
	SqrtDiag(0,1)=sqrt(Diag(0,1));
	SqrtDiag(1,0)=sqrt(Diag(1,0));
	SqrtDiag(1,1)=sqrt(Diag(1,1));
	matrix2D UU = eig.get_pseudo_v()*SqrtDiag*trans(eig.get_pseudo_v());
// 	cout<<"det UU= "<<  UU(0,0)*UU(1,1)-UU(0,1)*UU(1,0)<<endl;
	double detUU = UU(0,0)*UU(1,1)-UU(0,1)*UU(1,0);
	if(abs(detUU -1.) > 0.001)
		cout<<"warning; detU= "<<abs(detUU -1.)<<endl;
	// 				cout<<"UU eigenvalues"<< SqrtDiag(0,0)<< " "<<SqrtDiag(0,1)<< " "<<SqrtDiag(1,0)<<" "<<SqrtDiag(1,1)<<endl; 
	// 				cout<<"UU"<< UU(0,0)<< " "<<UU(0,1)<< " "<<UU(1,0)<<" "<<UU(1,1)<<endl;

	euvar.energy =coarse_grain_spline(tilde_c,c,UU);
	euvar.U.c11=UU(0,0);
	euvar.U.c22=UU(1,1);
	euvar.U.c12=UU(0,1);
	return euvar;
}


double energy_withF(tilde_coordinates& tilde_c ,coordinates& c,matrix2D& F ){

	double energy =coarse_grain_spline(tilde_c,c, F);
	return energy;
}



// void coo_func_for_splines(double& theta)
// {
// 	
// 	
// 	BSpline bspline3 = spline();
// 	DenseVector xx(2);
// 	
// 	std::vector<cella_stru>  coo;
// 	std::vector<double>  energy;
// 	
// 	double alpha = .0;
// 	double alpha_last = 4.;
// 	double dx = 0.01;
// 	int limit = abs(alpha_last)/dx;
// // 	int limit = .6/dx;
// 	matrix_stru rot;
// 	rot.m11 =  cos(theta);
// 	rot.m22 =  cos(theta);
// 	rot.m12 = -sin(theta);
// 	rot.m21 =  sin(theta);
// 	
// 	base_stru n,nr;
// 	base_stru a,ar;
// 	base_stru co;
// 	
// 	//SQUARE LATTICE
// 	co.e1[0] = 1.;
// 	co.e1[1] = 0.;
// 	co.e2[0] = 0.;
// 	co.e2[1] = 1.;
// 	//NORMAL TO GLIDING PLANE 
// 	n.e1[0]  = 0. ;
// 	n.e1[1]  = 1. ;
// 	//GLIDING PLANE 
// 	a.e1[0]  = 1. ;
// 	a.e1[1]  = 0. ;
// 	//NORMAL TO GLIDING PLANE 
// 	
// 	nr.e1[0] = rot.m11*n.e1[0] + rot.m12*n.e1[1];
// 	nr.e1[1] = rot.m21*n.e1[0] + rot.m22*n.e1[1];
// 	ar.e1[0] = rot.m11*a.e1[0] + rot.m12*a.e1[1];
// 	ar.e1[1] = rot.m21*a.e1[0] + rot.m22*a.e1[1];
// 
// 
// 	double sfe1 = nr.e1[0]*co.e1[0] + nr.e1[1]*co.e1[1];
// 	double sfe2 = nr.e1[0]*co.e2[0] + nr.e1[1]*co.e2[1];
// 
// 	cout<<"limit= "<<limit<<endl;
// 	while(alpha<=alpha_last)
// 	{
// 		base_stru c;
// 	
// 
// 		//Real vectors
// 		c.e1[0] = 1. + alpha*sfe1*ar.e1[0];
// 		c.e1[1] = 0. + alpha*sfe1*ar.e1[1];
// 		c.e2[0] = 0. + alpha*sfe2*ar.e1[0];
// 		c.e2[1] = 1. + alpha*sfe2*ar.e1[1];
// 
// 	
// // 		cout<<"alpha= "<<alpha<<endl;
// 
// 	
// 		base_stru r;
// 
// 	
// 		r= riduci_vectors(c);
// 
// 		struct matrix_stru m=find_integer_matrix(c,r);
// 	
// 
// 		struct cella_stru metric_a = faicella(c);
// 
// 		//Lagrange reduced metric to the fundementale domain
// 		struct cella_stru metric_r = faicella(r);
// 
// 	
// 		//Lagrange reduced metric to the fundementale domain using another approach	
// 		cella_stru metric_rr = riduci(metric_a);
// 	    double burgers = 1;
//     
// 
// 		cella_stru sr,spr;
// 		sr =  stress_easy(metric_r,burgers);
// 		spr = stress_easy(metric_rr,burgers);
// // 		struct cella_stru mypoint = {metric_a.c11, metric_a.c22, metric_a.c12};
// //      coo.push_back(mypoint);
// // 		energy.push_back(energy_zanzotto(metric_rr,burgers));
// 		
//         xx(0) = metric_r.c22; xx(1) = metric_r.c12;
// // 		cout<<alpha<<" "<<bspline3.eval(x)<<" "<<energy_zanzotto(metric_rr,burgers)<<endl;
// // 		cout<<alpha<<" "<<bspline3.eval(x)<<" "<<sr.c11<<endl;
// 
// // 		cout<<"alpha: "<<alpha<<" "<<xx(0)<<" "<<xx(1)<<" "<<" "<<metric_r.c22<<endl;
// // 		double x = metric_r.c11;
// // 		double y = metric_r.c22;
// // 		double z = metric_r.c12;
// // 		double dytilde = -(x*y)/(2.*pow(x*y - pow(z,2),1.5)) + 1./sqrt(x*y - pow(z,2));
// 
// // 		DenseMatrix jac =  bspline3.evalJacobian(xx);
// 		
// 		cout<<alpha<<" "<<bspline3.eval(xx) <<" "<<energy_zanzotto(metric_rr,burgers)<<endl;
// // 		cout<<alpha<<" "<<c12_spline(metric_r) <<" "<<sr.c12<<endl;
// // 		cout<<alpha<<" "<<bspline3.eval(xx) <<" "<<sr.c12<<endl;
// 
// 
// 		alpha+=dx;
// 		
// 
// 	}
// 		
// 
// 	cout<<"check size of vectors; size of coo= "<<coo.size()<<endl;
// 	cout<<"check size of vectors; size of coo= "<<energy.size()<<endl;
// 
// 	
// 
// }



