	
#include "input_output.h"




  

	
	bool fexists(const char *fileName)
	{
		std::ifstream infile(fileName);
		return infile.good();
	}
	void read_input_file()
	{
		std::ifstream file("./langevin_inputcpp");
		string str;
		// nx,ny,nz
		getline(file, str, '?');
		file >> str;
		int nx, ny, nz;
		file >> nx >> ny >> nz;
		dimensions::setNum(nx,ny,nz);
		
		getline(file, str, '?');
		file >> str;
		string ensemble;
		file >> ensemble;
		cout<<"ensemble= "<<ensemble<<endl;
		
		getline(file, str, '?');
		file >> str;
		int n_total;
		file >> n_total;
		cout<<"number of iteration total= "<<n_total<<endl;
		simulation_duration::setNum(n_total);

		
		
		getline(file, str, '?');
		file >> str;
		double dt,temperature,dt_p;
		int save,n_cpu;
		file >> dt >> temperature >> dt_p >> save >> n_cpu;
		simulation_parameters::setNum(dt,temperature,dt_p,save,n_cpu);
 		
		getline(file, str, '?');
		file >> str;
		string memory_answer;
		file >> memory_answer;

 		getline(file, str, '?');
		file >> str;
		string name;
		file >> name;
		
 
  		getline(file, str, '?');
		file >> str;
		string name_lengts;
		file >> name_lengts;
		

  		getline(file, str, '?');
		file >> str;
		string name_graddef;
		file >> name_graddef;
		
		

 		memory_file::setName(memory_answer,name,name_lengts,name_graddef);
 		cout<<"memory_answer= "<<memory_answer<<endl;
		cout<<"memory_name= "<<name<<endl;
 		cout<<"memory_lengts= "<<name_lengts<<endl;
		cout<<"memory_name_graddef= "<<name_graddef<<endl;


 		getline(file, str, '?');
		file >> str;
		string numerical_method2;
		file >> numerical_method2;


 		getline(file, str, '?');
		file >> str;
		string numerical_method;
		file >> numerical_method;
		
		numerical_method::setName(numerical_method,numerical_method2);
		cout<<"numerical_method= "<<numerical_method<<endl;
		cout<<"numerical_method2= "<<numerical_method2<<endl;
	 	
	 	
	 	getline(file, str, '?');
		file >> str;
		double skin,cut;
		file >> skin >> cut;
		info_2D::setNum(skin,cut);
		cout<<"skin= "<<info_2D::r_skin<<endl;
		cout<<"cut= "<<info_2D::r_cutoff<<endl;
		
	
		
	}