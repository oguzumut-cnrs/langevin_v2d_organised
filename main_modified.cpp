// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This is an example illustrating the use the general purpose non-linear 
    optimization routines from the dlib C++ Library.

    The library provides implementations of the conjugate gradient,  BFGS,
    L-BFGS, and BOBYQA optimization algorithms.  These algorithms allow you to
    find the minimum of a function of many input variables.  This example walks
    though a few of the ways you might put these routines to use.

*/


#include <iostream>
#include <functional> //without .h

#include </Users/salman/dlib-19.1/dlib/matrix.h>
#include "/Users/salman/dlib-19.1/dlib/threads.h"
#include </Users/salman/dlib-19.1/dlib/optimization.h>
#include "/Users/salman/dlib-19.1/dlib/graph_utils_threaded.h"
#include "/Users/salman/dlib-19.1/dlib/threads.h"
#include </Users/salman/dlib-19.1/dlib/gui_widgets.h>
#include </Users/salman/dlib-19.1/dlib/image_transforms.h>


#include <fstream>
#include <string>
#include <sstream>
#include <cmath>        // std::abs
#include <time.h>       /* time */
#include <math.h>
#include <float.h>

//#include "mkl_dfti.h"

#include <algorithm>
#include <iomanip>
#include <stack>
#include <vector>
typedef unsigned char byte;
using namespace std;
// #include "/Users/salman/dlib-19.1/dlib/threads.h"
// #include "/Users/salman/dlib-19.1/dlib/misc_api.h"  // for dlib::sleep
// #include "/Users/salman/dlib-18.17/dlib/all/source.cpp"  // for dlib::sleep



#define pi 3.14159265358979323846264338327950288419716939937510

#include "boost/multi_array.hpp"


// #include </Users/salman/dlib-19.1/dlib/matrix.h>

using namespace dlib;
typedef matrix<double,0,1> column_vector;
typedef matrix<int,0,1> column_vector_int;

typedef matrix<double,2,2> matrix2D;

#include <cassert>

#include "structures.h"

#include "common.h"
#include "allocate.h"
#include "initial_conditions.h"

#include "principals.h"
#include "forces.h"

namespace bi=box_informations;
namespace sp=simulation_parameters;


void box_calculations(tensors& F)
{
	matrix2D grad_def_inv=inv(F.grad_def); 

	double length0_x  =lengths::length0_x;
	double length0_y  =lengths::length0_y;
	double cell_length=info_2D::cell_length;

	double cell_length_x_tilde = cell_length * sqrt(pow(grad_def_inv(0,0),2.) + pow(grad_def_inv(0,1),2.) ) / length0_x;
	double cell_length_y_tilde = cell_length * sqrt(pow(grad_def_inv(1,0),2.) + pow(grad_def_inv(1,1),2.) ) / length0_y;


	int nb_cell_x = int( 1.0 / cell_length_x_tilde );
	int nb_cell_y = int( 1.0 / cell_length_y_tilde );

	cell_length_x_tilde = 1.0/nb_cell_x;
	cell_length_y_tilde = 1.0/nb_cell_y; 

    box_informations::setNum_cell(cell_length_x_tilde,cell_length_y_tilde);

	int n_atom_per_cell = 3 * atom_number::n_atom / ((double ) nb_cell_x*nb_cell_y);
	int n_pair_max = info_2D::n_pair_max_per_atom * atom_number::n_atom;
	
	box_informations::setNum_cell_nb(nb_cell_x,nb_cell_y,n_atom_per_cell,n_pair_max);
    
}

void initiate_tensors(tensors& F)
{
	F.grad_def(0,0) = 1.;
	F.grad_def(1,1) = 1.;
	F.grad_def(0,1) = 0.;
	F.grad_def(1,0) = 0.;
	
	F.virial(0,0) = 0.;
	F.virial(1,1) = 0.;
	F.virial(0,1) = 0.;
	F.virial(1,0) = 0.;
	
	F.pk1(0,0) = 0.;
	F.pk1(1,1) = 0.;
	F.pk1(0,1) = 0.;
	F.pk1(1,0) = 0.;	
	
	
	F.cofactor(0,0) = 0.;
	F.cofactor(1,1) = 0.;
	F.cofactor(0,1) = 0.;
	F.cofactor(1,0) = 0.;	
	
	
}

int check_tensors(tensors& F)
{
	int i=0;
	tensors F2;
	F2.grad_def(0,0) = 1.;
	F2.grad_def(1,1) = 1.;
	F2.grad_def(0,1) = 0.;
	F2.grad_def(1,0) = 0.;
	
	if(F2.grad_def(0,0)-F.grad_def(0,0) != 0 )
		i=1;
	if(F2.grad_def(1,1)-F.grad_def(1,1) != 0 )
		i=1;
	if(F2.grad_def(0,1)-F.grad_def(0,1) != 0 )
		i=1;
	if(F2.grad_def(1,1)-F.grad_def(1,1) != 0 )
		i=1;
	
	return i;

}

string IntToStr(int n) 
{
	stringstream result;
	result << 10000+n;
	return result.str();
}

string IntToStr_normal(int n) 
{
	stringstream result;
	result << n;
	return result.str();
}
string RealToStr_normal(double n) 
{
	stringstream result;
	result << n;
	return result.str();
}

void write_to_a_file(coordinates& c,int t)
{
   ofstream metrics,disp;
   string filename,filename2;

//    filename="dir_config/config_" + IntToStr(t) +".txt";
//    filename="dir_config/config_" + IntToStr(t) ;
   filename2="dir_config_u/config_" + IntToStr(t) ;

//    cout<< filename << "  \n";
//    metrics.open(filename.c_str());
   disp.open(filename2.c_str());

   for (int k=0;k<3; k++){
   for (int i=0;i<c.x.size();i++){
    if(k ==0){
    disp << std::scientific << std::setprecision(7) << c.x[i] << " ";
    disp  << "  \n";}
    if(k ==1){
    disp << std::scientific << std::setprecision(7) << c.y[i] << " ";
    disp  << "  \n";}
    }
    }
        
  disp.close();

}

void read_from_a_file(coordinates& c)
{
	ifstream metrics,disp;
	string filename2;

	//    filename2=reading::filename2;
	//    filename2="/Users/salman/deneme/memory.dat";
	filename2=memory_file::filename;
	cout<<"reading file "<<filename2<<endl;

	ifstream file;
	file.open(filename2.c_str());

	string line;
	int count=0;
	while (getline(file, line))
	count++;
	cout << "Numbers of lines in the file : " << count << endl;
	file.close();



	double temp;

	//    cout<< filename << "  \n";
	disp.open(filename2.c_str());

	c.x.resize( 0 );
	c.y.resize( 0 );

	for (int k=0;k<3; k++)
	{
		for (int i=0;i<count/2;++i)
		{
			if(k ==0)
			{
				disp >>temp;
		//     	disp >> c.x[i];
				c.x.push_back(temp);
	}
			if(k ==1)
			{
				disp >>temp;
		//     	disp >> c.y[i];
		c.y.push_back(temp);
			}
		}
	}

	disp.close();
	
	

}


//DEVELOP MORE HERE
///////////////////
void read_from_a_file_lengths(tensors& F)
{
	ifstream metrics,disp;
	string filename2,filename3;

	//    filename2=reading::filename2;
	//    filename2="/Users/salman/deneme/memory.dat";
	filename2="/Users/salman/try/grad_def.dat";

	cout<<"reading file "<<filename2<<endl;



	disp.open(filename2.c_str());

	
	double temp;
	disp >>F.grad_def(0,0) ;
	disp >>F.grad_def(1,1) ;
	disp >>temp ;
	disp >>F.grad_def(0,1) ;
	disp >>F.grad_def(1,0) ;
	
	cout<<"F(0,0)= "<<F.grad_def(0,0)<<endl;
	cout<<"F(1,1)= "<<F.grad_def(1,1)<<endl;
	cout<<"F(0,1)= "<<F.grad_def(0,1)<<endl;
	cout<<"F(1,0)= "<<F.grad_def(1,0)<<endl;


	disp.close();


	filename3="/Users/salman/try/lengths.dat";

	cout<<"reading file "<<filename3<<endl;

	disp.open(filename3.c_str());
	double l1,l2;
	disp >>l1 ;
	disp >>l2 ;
	cout<<"l1= "<<l1<<endl;
	cout<<"l2= "<<l2<<endl;
	
	lengths::setNum(l1,l2);
	disp.close();
	
// write(14,666) grad_def(1,1)
// write(14,666) grad_def(2,2)
// write(14,666) grad_def(3,3)
// write(14,666) grad_def(1,2)
// write(14,666) grad_def(2,1)
// write(14,666) grad_def(1,3)
// write(14,666) grad_def(3,1)
// write(14,666) grad_def(2,3)
// write(14,666) grad_def(3,2)
		
}

///////////////////

double maxval(std::vector<double>& m)
{
    double max;
    max  = *max_element(m.begin(), m.end());
    return max;

}

double minval(std::vector<double>& m)
{
    double min;
    min  = *min_element(m.begin(), m.end());
    return min;

}

std::vector<double> sum_of_sq_mn(std::vector<double>& m,std::vector<double>& n)
{
	
	std::vector<double> delta_r_sq;
	delta_r_sq.resize(m.size());
	
	for (int i=0;i<m.size();i++)
	{
		delta_r_sq[i] = pow(m[i],2.)+pow(n[i],2.);
	}
	
	return delta_r_sq;
}

void test_knn2(coordinates& c,int nn)
{
	std::vector<matrix<double,2,1> > samples;
	matrix<double,2,1> test;


	for (int i=0; i<c.x.size(); i++) 
	{
	  test = c.x[i],c.y[i];
	  samples.push_back(test);
	}

	std::vector<sample_pair> edges;
	clock_t t;
	t = clock();
	find_k_nearest_neighbors(samples, squared_euclidean_distance(), 4, edges);
    t = clock() - t;
    printf ("It took me  (%f seconds) to find_k_nearest_neighbors.\n",((float)t)/CLOCKS_PER_SEC);

	std::sort(edges.begin(), edges.end(), &order_by_index<sample_pair>);
	
// 	for(int i=0;i<edges.size();++i)cout<<edges[i].distance()<<endl;
// 	for(int i=0;i<edges.size();++i)cout<<edges[i].index1()<<" "<<edges[i].index2()<<endl;

 
}


#include "kdtree2.hpp"
#include "kdtree2.cpp"
typedef boost::multi_array<float,2> array2dfloat;

bool find_nn(coordinates& c,int nn)
{
	//REQUIRED FOR KDTREE
    kdtree2::KDTree* tree;
    kdtree2::KDTreeResultVector res;  
    array2dfloat realdata; 
    realdata.resize(boost::extents[c.x.size()][2]);   
    //LOCAL
    bool check=false;  
    
	for (int i=0; i<c.x.size(); i++) 
	{
	  realdata[i][0] = c.x[i];
	  realdata[i][1] = c.y[i];      
	}
	
	tree =  new kdtree2::KDTree(realdata,true);
	tree->sort_results = true;
	std::vector<float> query(2); 
    kdtree2::KDTreeResultVector result;

	tree->n_nearest(query,nn,result);
	 
	
	for (int i=0; i<c.x.size(); i++)
	{
	query[0]=c.x[i];
	query[1]=c.y[i];
	tree->n_nearest(query,nn,result);
	for (int k=1;k<=6;k++)
	{
	if(sqrt(result[k].dis) < 0.75 ) 
	{
		check=true;
// 		cout<<"check : "<<check<<"k= "<<k<<"  "<<sqrt(result[k].dis)<<endl;
// 		cout<<" x = "<<c.x[i]<<endl;
// 		cout<<" y = "<<c.y[i]<<endl;

	}
	if(check) break;
	}	
// 	cout<<"atom = "<< i<< " 1th nn distance = "<< result[1].dis<<" ind =  "<< result[1].idx<<endl;
// 	cout<<"atom = "<< i<< " 2th nn distance = "<< result[2].dis<<" ind =  "<< result[2].idx<<endl;
// 	cout<<"atom = "<< i<< " 3th nn distance = "<< result[3].dis<<" ind =  "<< result[3].idx<<endl;
// 	cout<<"atom = "<< i<< " 4th nn distance = "<< result[4].dis<<" ind =  "<< result[4].idx<<endl;
// 	cout<<"atom = "<< i<< " 5th nn distance = "<< result[5].dis<<" ind =  "<< result[5].idx<<endl;
// 	cout<<"atom = "<< i<< " 6th nn distance = "<< result[6].dis<<" ind =  "<< result[6].idx<<endl;
// 	cout<<"atom = "<< i<< " 7th nn distance = "<< result[7].dis<<" ind =  "<< result[7].idx<<endl;
// 	cout<<"atom = "<< i<< " 8th nn distance = "<< result[8].dis<<" ind =  "<< result[8].idx<<endl;

	}
	delete tree;
	
	return check;

}

	bool fexists(const char *fileName)
	{
		std::ifstream infile(fileName);
		return infile.good();
	}
	void read_input_file()
	{
		std::ifstream file("/Users/salman/lastlast/langevin_input");
		string str;
		// nx,ny,nz
		getline(file, str, '?');
		file >> str;
		int nx, ny, nz;
		file >> nx >> ny >> nz;
		dimensions::setNum(nx,ny,nz);
		
		getline(file, str, '?');
		file >> str;
		string ensemble;
		file >> ensemble;
		cout<<"ensemble= "<<ensemble<<endl;
		
		getline(file, str, '?');
		file >> str;
		double dt,temperature,dt_p;
		file >> dt >> temperature >> dt_p ;
		simulation_parameters::setNum(dt,temperature,dt_p);
 		
		getline(file, str, '?');
		file >> str;
		string memory_answer;
		file >> memory_answer;

 		getline(file, str, '?');
		file >> str;
		string name;
		file >> name;
		
		memory_file::setName(memory_answer,name);
		cout<<"memory_answer= "<<memory_answer<<endl;
		cout<<"memory_name= "<<name<<endl;
 		
 		getline(file, str, '?');
		file >> str;
		string numerical_method;
		file >> numerical_method;
		
		numerical_method::setName(numerical_method);
		cout<<"numerical_method= "<<numerical_method<<endl;
	 	
	 	
	 	getline(file, str, '?');
		file >> str;
		double skin;
		file >> skin;
		info_2D::setNum(skin);
		cout<<"skin= "<<info_2D::r_skin<<endl;
	
		
	}	
	
int main()
{
	//REQUIRED TO STOP THE BY ERASING THE FILE ROULE
    const char* filename = "roule";
    ofstream myfile;
    myfile.open ("roule");
    read_input_file();

	int nx = environment::Parameters::getInstance().nx;
	int ny = environment::Parameters::getInstance().ny;
	

	//write sizes on a file information_tailles
	ofstream sizes;
	sizes.open ("information_tailles");
	sizes<<" "<<nx<<"   "<<ny<<"   "<<1<<"   "<<"mode sauvegarde: complete"<<"\n"<<" 3"<<"	"<<"0"<<"\n";
	sizes.close();
   

	//c stands for coordinates; no need to allocate
	//allocation dynamically happens during initial conditions assignement
	coordinates c,c2;
	if(memory_file::answer.compare("n") ==0)
	{
		initiate_coo_2D_triangular_with_grain(c);
		initiate_coo_2D_triangular_with_grain(c2);
	}
	tensors F;
	matrix2D virial;
	if(memory_file::answer.compare("y") ==0)
	{
		read_from_a_file(c);
		read_from_a_file(c2);
		initiate_tensors(F);
		read_from_a_file_lengths(F);
		c.length0_x = lengths::length0_x;
		c.length0_y = lengths::length0_y;


	}
// 	std::vector<perspective_window::overlay_dot> points;
// 
//     for (int i = 0; i < c.x.size(); ++i)
//     {
//         // Get a point on a spiral
//         dlib::vector<double> val(0,c.x[i],c.y[i]);
// 
//         // Pick a color based on how far we are along the spiral
//         rgb_pixel color = colormap_jet(c.x[i],c.y[i],maxval(c.x));
//         
// 
//         // And add the point to the list of points we will display
//         points.push_back(perspective_window::overlay_dot(val, color));
//     }
// 	cout<<" Now finally display the point cloud"<<endl;
//     // Now finally display the point cloud.
//     perspective_window win;
// //     win.fit_to_contents():
// //     rgb_pixel color2=colormap_jet(255,255,255);
//     win.set_background_color(0.2,1.1,0.4);
//     win.set_title("perspective_window 3D point cloud");
//     win.add_overlay(points);
//     win.wait_until_closed();


	test_knn2(c,4);

	int k=0;
	write_to_a_file(c,k++);
	if(memory_file::answer.compare("n") ==0)
	{
		c.length0_x = *max_element(c.x.begin(), c.x.end()) + info_2D::d1/2.0;
		c.length0_y = *max_element(c.y.begin(), c.y.end()) + info_2D::d2;
		lengths::setNum(c.length0_x,c.length0_y);
		initiate_tensors(F);
	}	
	cout<<"c.length0_x= "<<c.length0_x<<endl;
	cout<<"c.length0_y= "<<c.length0_y<<endl;

	c.set_values (c.x.size());

	atom_number::setNum(c.n_atom());
	cout<<"atum number= "<<c.n_atom()<<endl;

	//tilde_c stands for  tildecoordinates; no need to allocate
	//allocation dynamically happens during transform_to_tilde_coordinates
	tilde_coordinates tilde_c;
    


	
	box_calculations(F);
	int n =c.n_atom();

	int nb_cell_x=bi::nb_cell_x;
	int nb_cell_y=bi::nb_cell_y;
	int n_atom_per_cell=bi::n_atom_per_cell;
	//USING SINGLETON PARAMATERS
	//ARGUMENTS nb_cell_x,nb_cell_y are GLOBALS
	initiate_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
	transform_to_tilde_coordinates(c,tilde_c,F);
// 	cout<<"bool for nn : "<<find_nn(c, 10)<<endl;

	
  	


	find_cells(tilde_c);
	neighbor_list_seq(tilde_c,F);



//f and rf are deternministic and stochastic forces respectively
//they are allocated using the number of atoms
//they are not std vector but dlib vectors
	forces_std fo,fo1,fo2,fo3,fo4;
	
	initiate_std_forces(fo,n);
	initiate_std_forces(fo1,n);
	initiate_std_forces(fo2,n);
	initiate_std_forces(fo3,n);
    initiate_std_forces(fo4,n);

	random_forces_std rf;
	rf.x=wiener();
    rf.y=wiener();

	
	tilde_coordinates tilde_c1=tilde_c;
	tilde_coordinates tilde_c2;
	tilde_coordinates tilde_c3;
	tilde_coordinates tilde_c4;
	
	tilde_coordinates cumul;
	int n_atom = n;
	

	cumul.x.assign (n_atom,0); // n_atom doubles with a value of 0
	cumul.y.assign (n_atom,0); // n_atom doubles with a value of 0
	
	tilde_coordinates cumul_dumb;
	cumul_dumb.x.assign (n_atom,0); // n_atom doubles with a value of 0
	cumul_dumb.y.assign (n_atom,0); // n_atom doubles with a value of 0



	double dt = sp::dt;
	cout<<"dt= "<<sp::dt<<endl;
	double dt_small=dt;
	double temperature=sp::temperature;
	cout<<"temperature= "<<sp::temperature<<endl;
	double counter=0;
	double counter_nn=0;
	std::vector<double> delta_r_sq;

	double x = sp::dt_predictor;
    double dt_ad=dt/x;
    cout<<"dt_predictor= "<<dt_ad<<endl;
    
    
    
	initiate_fo(n);	
	initiate_cumul(n);
	
	
	
	//STARTS what happens at time t for CG
	force_energy::calculation::getInstance().rf.x=wiener();
	force_energy::calculation::getInstance().rf.y=wiener();
	
	rf.x = force_energy::calculation::getInstance().rf.x;
	rf.y = force_energy::calculation::getInstance().rf.y;
	
	force_energy::calculation::getInstance().tilde_c_t.x=tilde_c.x;
	force_energy::calculation::getInstance().tilde_c_t.y=tilde_c.y;	
	//END what happens at time t for CG

    
    
    column_vector m;
	m.set_size(2*n);
	
	//guess coo at time t+1
	//RUNGE_KUTTA_4 //GUESS THE SOLUTION AT TIME T+1
		
	forces_with_nbl(tilde_c,fo1,F );  //d1		
	tilde_c2 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo1,rf
	,cumul_dumb,F,dt_ad/2.,temperature);
	forces_with_nbl(tilde_c2,fo2,F ); //d2
	tilde_c3 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo2,rf
	,cumul_dumb,F,dt_ad/2.,temperature); 
	forces_with_nbl(tilde_c3,fo3,F ); //d3  
	tilde_c4 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo3,rf
	,cumul_dumb,F,dt_ad,temperature);
	forces_with_nbl(tilde_c4,fo4,F ); //d4

	for(int i=0;i<n_atom;i++)
	{
		fo.x[i] = (fo1.x[i]+2*fo2.x[i]+2*fo3.x[i]+fo4.x[i])/6. ;
		fo.y[i] = (fo1.y[i]+2*fo2.y[i]+2*fo3.y[i]+fo4.y[i])/6. ;
	}

	tilde_c =  new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,
								  fo,rf,cumul,F,dt_ad,temperature);
	for(int i=0;i<tilde_c.x.size();i++)
	{	
		if ( tilde_c.x[i]  < 0 )  
			 tilde_c.x[i] +=  1.0;

		if ( tilde_c.x[i] >= 1 )  
			 tilde_c.x[i] -=  1.0;

		if ( tilde_c.y[i] < 0 )  
			 tilde_c.y[i] += 1.0;
	
		if ( tilde_c.y[i] >= 1 )  
			 tilde_c.y[i] -= 1.0;

	}

	tilde_c2 = tilde_c;

	for(int i=0;i<tilde_c2.x.size();i++)
	{
	m(i)    = tilde_c2.x[i];
	m(i+n)  = tilde_c2.y[i];
	}  

	
	
	
	for(int iter=0;iter<1000000;iter++)
	{
    
// find_min(cg_search_strategy(),  // USE CG strategy, there are other choices.
// objective_delta_stop_strategy(1e-1).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// rosen, rosen_derivative, m, -250);
	clock_t t;
// 	c2.x =c.x ;
//     c2.y =c.y ;
	t = clock();

	find_min(lbfgs_search_strategy(10),  // USE CG strategy, there are other choices.
	objective_delta_stop_strategy(.1).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
	rosen, rosen_derivative, m, -20300);
    t = clock() - t;
    printf ("It took me  (%f seconds) to perform find_min\n",((float)t)/CLOCKS_PER_SEC);

	
		transform_to_original_coordinates_dlib(c,m,F);


		if(iter % 1000 ==0)	
			write_to_a_file(c,k++);

  	
  	
        for(int i=0;i<n;i++)
        {
			cumul.x[i] +=c.x[i]-c2.x[i] ;
			cumul.y[i] +=c.y[i]-c2.y[i] ;
		}
  	

		for(int i=0;i<tilde_c.x.size();i++)
		{
			tilde_c.x[i] = m(i);
			tilde_c.y[i] = m(i+n);
			
			if ( tilde_c.x[i]  < 0 )  
				 tilde_c.x[i] +=  1.0;
		
			if ( tilde_c.x[i] >= 1 )  
				 tilde_c.x[i] -=  1.0;

			if ( tilde_c.y[i] < 0 )  
				 tilde_c.y[i] += 1.0;
			
			if ( tilde_c.y[i] >= 1 )  
				 tilde_c.y[i] -= 1.0;

		}  
        
        delta_r_sq = sum_of_sq_mn(cumul.x,cumul.y);
        double delta_r_max   = sqrt( maxval(delta_r_sq));
        cout<<"delta_r_max: "<<delta_r_max<<" info_2D::r_skin/2.0: "<<info_2D::r_skin/2.0<<endl; 
        
        //CHECK THE NECESSITY OF NBL
		if ( delta_r_max >= info_2D::r_skin/2.0 )
		
		{
			counter++;
			if(check_tensors(F) == 1)
			{
				box_calculations(F);
				nb_cell_x=bi::nb_cell_x;
				nb_cell_y=bi::nb_cell_y;
	 			n_atom_per_cell=bi::n_atom_per_cell;
				initiate_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
				cout<<"tensors are checked and are changed: "<<"iteration: "<<iter<<"\n";
				cout<<"maxval of x : "<<maxval(c.x)<<"\n";							
			}
			find_cells(tilde_c);			
			neighbor_list_seq(tilde_c,F);
// 			cout<<"relabelling for the "<<counter<<"th time :"<<"iteration and n_pair: "<<iter<<
// 			" "<<pairing::n_pair<<"\n";
			cumul.x.assign (n_atom,0); // n_atom doubles with a value of 0
			cumul.y.assign (n_atom,0); // n_atom doubles with a value of 0
			cumul_dumb.x.assign (n_atom,0); // n_atom doubles with a value of 0
			cumul_dumb.y.assign (n_atom,0); // n_atom doubles with a value of 0

		}	       

		
  		//solution at time T
		//original
// 		for(int i=0;i<n;i++)
// 		{
// 			force_energy::calculation::getInstance().tilde_c_t.x[i]  =m(i);
// 			force_energy::calculation::getInstance().tilde_c_t.y[i]  =m(i+n);
// 		}  
		//switched
		
		for(int i=0;i<n;i++)
		{
			force_energy::calculation::getInstance().tilde_c_t.x[i]  =tilde_c.x[i];
			force_energy::calculation::getInstance().tilde_c_t.y[i]  =tilde_c.y[i];
		} 
		transform_to_original_coordinates(c2,tilde_c,F);
 		 
		
		//now advance at time T
		//create noise for T+1
		force_energy::calculation::getInstance().rf.x=wiener();
		force_energy::calculation::getInstance().rf.y=wiener();
		
	    //EULER //GUESS THE SOLUTION AT TIME T+1
	    if(numerical_method::answer.compare("euler") ==0)
	    {

			//FORCES
			forces_with_nbl(force_energy::calculation::getInstance().tilde_c_t,fo1,F); 
			// NEW COORDINATES
			tilde_c = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo1,
					  force_energy::calculation::getInstance().rf,
					  cumul_dumb,F,dt_ad,temperature); 
		}
		//RUNGE-KUTTA_2
		
		if(numerical_method::answer.compare("RK2") ==0)
	    {
			//FORCES
			forces_with_nbl(force_energy::calculation::getInstance().tilde_c_t,fo1,F); 
			// NEW COORDINATES
			tilde_c = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo1,
					  force_energy::calculation::getInstance().rf,
					  cumul_dumb,F,dt_ad,temperature); 

	
			forces_with_nbl(tilde_c,fo2,F);
		
			for(int i=0;i<n_atom;i++)
			{
				fo.x[i] = (fo1.x[i]+fo2.x[i])/2. ;
				fo.y[i] = (fo1.y[i]+fo2.y[i])/2. ;
			}
		
			//GUESS THE SOLUTION AT TIME T+1
			tilde_c = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo,
					  force_energy::calculation::getInstance().rf,
					  cumul_dumb,F,dt_ad,temperature);

		}	
									 
		//RUNGE_KUTTA_4 //GUESS THE SOLUTION AT TIME T+1
		if(numerical_method::answer.compare("RK4") ==0)
		{
// 			forces_with_nbl(tilde_c,fo1,F );  //accidentally correct		
			forces_with_nbl(force_energy::calculation::getInstance().tilde_c_t,fo1,F );  //d1		

			tilde_c2 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo1,
			force_energy::calculation::getInstance().rf,cumul_dumb,F,dt_ad/2.,temperature);
		
			forces_with_nbl(tilde_c2,fo2,F ); //d2
			tilde_c3 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo2,
			force_energy::calculation::getInstance().rf,cumul_dumb,F,dt_ad/2.,temperature); 
		
			forces_with_nbl(tilde_c3,fo3,F ); //d3  
			tilde_c4 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo3,
			force_energy::calculation::getInstance().rf,cumul_dumb,F,dt_ad,temperature);
		
			forces_with_nbl(tilde_c4,fo4,F ); //d4

			for(int i=0;i<n_atom;i++)
			{
				fo.x[i] = (fo1.x[i]+2*fo2.x[i]+2*fo3.x[i]+fo4.x[i])/6. ;
				fo.y[i] = (fo1.y[i]+2*fo2.y[i]+2*fo3.y[i]+fo4.y[i])/6. ;
			}
		
			tilde_c =  new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo,
			force_energy::calculation::getInstance().rf,cumul,F,dt_ad,temperature);
        
        }
							 	
			
		//GUESS STARTING POINT WITH EULER
		for(int i=0;i<tilde_c.x.size();i++)
		{					
			m(i)    = tilde_c.x[i];
			m(i+n)  = tilde_c.y[i];
		}   

	}  
	
	
////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	
	
// 			find_cells(tilde_c);			
// 			neighbor_list_seq(tilde_c,F);

/*
         fstream energy_stream;
         energy_stream.open ("total_energy.dat", fstream::in | fstream::out | fstream::app);

    cout<<"main loop starts with dt "<<dt<<"\n";
    for(int iter=0;iter<100000;iter++)
    {
		
// 		if(find_nn(c, 10))
// 		{
// 			dt_ad=dt_small;
// 			cout<<"iter = "<<iter<<endl;
// 			counter_nn++;
// 		}
// 		else
// 		{	if(counter_nn != 0)
// 				dt_ad=dt;
// 		}
 		
		forces_with_nbl(tilde_c,fo1,F );  //d1		
		tilde_c2 = new_coordinates_2D(tilde_c,fo1,rf,cumul_dumb,F,dt_ad/2.,temperature);
		forces_with_nbl(tilde_c2,fo2,F ); //d2
		tilde_c3 = new_coordinates_2D(tilde_c,fo2,rf,cumul_dumb,F,dt_ad/2.,temperature); 
		forces_with_nbl(tilde_c3,fo3,F ); //d3  
		tilde_c4 = new_coordinates_2D(tilde_c,fo3,rf,cumul_dumb,F,dt_ad,temperature);
		forces_with_nbl(tilde_c4,fo4,F ); //d4
        
        for(int i=0;i<n_atom;i++)
        {
			fo.x[i] = (fo1.x[i]+2*fo2.x[i]+2*fo3.x[i]+fo4.x[i])/6. ;
			fo.y[i] = (fo1.y[i]+2*fo2.y[i]+2*fo3.y[i]+fo4.y[i])/6. ;
	    }
        
        tilde_c =  new_coordinates_2D(tilde_c,fo,rf,cumul,F,dt_ad,temperature);
		rf.x=wiener();
    	rf.y=wiener();
        
        if(iter % 1000 ==0)
        {
        	transform_to_original_coordinates(c,tilde_c,F); 
        	write_to_a_file(c,k++);

        	cout<<"n_pair = "<<pairing::n_pair<<" at iter : "<<iter<<endl;        	
        	cout<<"adaptive = "<<counter_nn<<" at iter : "<<iter<<endl;          	
        }  
        
        
        double total_energy = std::accumulate( fo1.e.begin(), fo1.e.end(), 0.0);
        
        energy_stream<<std::scientific << std::setprecision(7)
         <<dt_ad*iter<<" "<<total_energy<<"\n"; 
   



        
        
        delta_r_sq = sum_of_sq_mn(cumul.x,cumul.y);
        double delta_r_max   = sqrt( maxval(delta_r_sq));
		if ( delta_r_max >= info_2D::r_skin/2.0 )
		
		{
			counter++;
			if(check_tensors(F) == 1)
			{
				box_calculations(F);
				nb_cell_x=bi::nb_cell_x;
				nb_cell_y=bi::nb_cell_y;
	 			n_atom_per_cell=bi::n_atom_per_cell;
				initiate_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
				cout<<"tensors are checked and are changed: "<<"iteration: "<<iter<<"\n";
				cout<<"maxval of x : "<<maxval(c.x)<<"\n";							
			}
			find_cells(tilde_c);			
			neighbor_list_seq(tilde_c,F);
// 			cout<<"relabelling for the "<<counter<<"th time :"<<"iteration and n_pair: "<<iter<<
// 			" "<<pairing::n_pair<<"\n";
			cumul.x.assign (n_atom,0); // n_atom doubles with a value of 0
			cumul.y.assign (n_atom,0); // n_atom doubles with a value of 0
// 		
		}
//    
    	if(fexists(filename) == false)
    	{ 
    		cout<<"CODE HAS BEEN STOPPED BY THE USER AT TIME STEP  "<<iter<<"\n";
    		break; 
    	}

	}
	
*/
    
}



