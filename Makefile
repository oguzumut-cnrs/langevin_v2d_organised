# This is the makefile used to build the dlib C++ library's regression test suite
# on Debian Linux using the gcc compiler.

# this is the name of the output executable
TARGET =     ~/bin/langevin_v2.exe 

X11ROOT = /usr/X11

MKLROOT=/opt/intel/oneapi/mkl/2022.0.0
# MKLROOT=/opt/intel/mkl
MKLRUNLIB_PATH = $(MKLROOT)/$(_lib_dir)
MKL_PATH = $(MKLROOT)/$(_lib_dir)

# these are the compile time flags passed to gcc
# CXXFLAGS ?= -ggdb -Wall
CXXFLAGS = -ggdb 


# CPPFLAGS ?=  -std=c++11     -O2  -I/Users/salman/boost_1_62_0 
CPPFLAGS =   -qopenmp -std=c++11     -O2  -I/usr/local/opt/boost/include  -I../dlib-19.1  -I/usr/local/include/SPLINTER  -I/opt/X11/include     -I../cute_crystal_origin/cpp_com/src
 CPPFLAGS += -I$(MKLROOT)/include
CXXFLAGS =  -O3 -ggdb    -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk
CPPFLAGS += -DMKL_ILP64 -I../alglib_3.17.0/src -DAE_CPU=AE_INTEL -DAE_OS=AE_POSIX -pthread  -I/Users/usalman/codes/arpack-eigen/include/

# -I./source/utils is required for intel daal
# These are the link time flags passed to gcc
#the commented line is required for intel daal
# LFLAGS = -lpthread    -L/usr/X11/lib    -lX11  -Wl,-rpath,"/opt/intel//compilers_and_libraries_2017.0.102/mac/daal/lib" -Wl,-rpath,"/opt/intel//compilers_and_libraries_2017.0.102/mac/daal/../tbb/lib" "/opt/intel//compilers_and_libraries_2017.0.102/mac/daal/lib"/libdaal_core.a "/opt/intel//compilers_and_libraries_2017.0.102/mac/daal/lib"/libdaal_thread.a -ltbb -ltbbmalloc -ldl
LFLAGS = -lpthread    -L/opt/X11/lib    -lX11 -L/usr/local/opt/libomp/lib -lomp    -lX11 -L/usr/local/lib -lsplinter-3-0
LFLAGS += -L${MKLROOT}/lib -Wl,-rpath,${MKLROOT}/lib -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
# The name of the compiler.  If you only have one version of
# gcc installed then you probably want to change this to just g++ 
CXX = nice icpc
# CXX = icpc
####################################################
####################################################
#  Here we list all the cpp files we want to compile

SRC  = ../dlib-19.1/dlib/all/source.cpp
SRC += ../alglib_3.17.0/src/alglibinternal.cpp
SRC += ../alglib_3.17.0/src/alglibmisc.cpp
SRC += ../alglib_3.17.0/src/ap.cpp
SRC += ../alglib_3.17.0/src/dataanalysis.cpp
SRC += ../alglib_3.17.0/src/diffequations.cpp
SRC += ../alglib_3.17.0/src/fasttransforms.cpp
SRC += ../alglib_3.17.0/src/integration.cpp
SRC += ../alglib_3.17.0/src/interpolation.cpp
SRC += ../alglib_3.17.0/src/optimization.cpp
SRC += ../alglib_3.17.0/src/linalg.cpp
SRC += ../alglib_3.17.0/src/solvers.cpp
SRC += ../alglib_3.17.0/src/specialfunctions.cpp
SRC += ../alglib_3.17.0/src/statistics.cpp
SRC  += structures.cpp
SRC  += environement.cpp
SRC  += namespaces.cpp
SRC  += timer.cpp
SRC  += utilities.cpp
SRC  += initial_conditions.cpp
SRC  += input_output.cpp
SRC += main.cpp
SRC  += common.cpp
SRC  += common2.cpp
SRC  += common3_alglib.cpp
SRC  += principals.cpp
SRC  += initial_relaxation.cpp
#SRC  += reductions.cpp
#SRC  += spline2.cpp
SRC  +=forces.cpp



# SRC += /Users/salman/dlib-19.1/dlib/test/tester.cpp
 


####################################################

TMP = $(SRC:.cpp=.o)
OBJ = $(TMP:.c=.o)

$(TARGET): $(OBJ) 
	@echo Linking $@
	@$(CXX) $(LDFLAGS) $(OBJ) $(LFLAGS) -o $@
	@echo Build Complete

clean:
	@rm -f $(OBJ) $(TARGET)
	@echo All object files and binaries removed

dep: 
	@echo Running makedepend
	@makedepend -- $(CFLAGS) -- $(SRC) 2> /dev/null 
	@echo Completed makedepend

###############################################################################
##########  Stuff from makedepend                                         #####
##########  type make dep at the command line to rebuild the dependencies #####
##########  Also, DON'T edit the contents of this file beyond this line.  #####
###############################################################################

