

#include "common.h"

//everything below is defined for dlib gradient conjugate
#include "datatable.h"
#include "bspline.h"
#include <bsplinebuilder.h>

using namespace SPLINTER;
// 

std::vector<double>  wiener();

double rosen (const column_vector& m)
/*
	This function computes the function; i.e. the total energy. */
{

	int n=atom_number::n_atom;
	tilde_coordinates tilde_c;
	static double ener=0;
	double ener_t1;
	static double delta_ener=0;
	tilde_c.x.resize(n);
	tilde_c.y.resize(n);
// 	column_vector res;
// 	res.set_size(2*n);
	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 

// 	coordinates c;
// 	tilde_coordinates f_t;
// 	c.x.resize(n);
// 	c.y.resize(n);
// 	f_t.x.resize(n);
// 	f_t.y.resize(n);

// 	c_t.x.resize(n);
// 	c_t.y.resize(n);
	





	for(int i=0;i<n;i++)
	{
		tilde_c.x[i] = m(i);
		tilde_c.y[i]  =m(i+n);
		
	}
	


	tensors F;
	initiate_tensors(F);

	force_energy::calculation::getInstance().forces_with_nbl_CG(tilde_c,F);	
	double dt = simulation_parameters::dt;
// 	  cout<<"inside maxval and minval e_CG :"<<max
// 	  									<<" "<<min<<endl;
	double ener2 = 0;
	for(int i=0;i<n;i++)
	{
// 		res(i)  =0.5*pow(m(i)   - force_energy::calculation::getInstance().tilde_c_t.x[i] - force_energy::calculation::getInstance().rf_t.x[i],2.) 
// 						+ 0.5*force_energy::calculation::getInstance().fo.e(i)*dt;
// 		
// 		res(i+n)=0.5*pow(m(i+n) - force_energy::calculation::getInstance().tilde_c_t.y[i] - force_energy::calculation::getInstance().rf_t.y[i],2.) 
// 						+ 0.5*force_energy::calculation::getInstance().fo.e(i)*dt;

	ener2+=0.5*pow(m(i)   - force_energy::calculation::getInstance().tilde_c_t.x[i] - force_energy::calculation::getInstance().rf_t.x[i],2.) /dt
 						+ 0.5*force_energy::calculation::getInstance().fo.e(i);
 	ener2+=0.5*pow(m(i+n) - force_energy::calculation::getInstance().tilde_c_t.y[i] - force_energy::calculation::getInstance().rf_t.y[i],2.) /dt
 						+ 0.5*force_energy::calculation::getInstance().fo.e(i);
	}
	
	return ener2/(2*n);
}



column_vector rosen_derivative(const column_vector& m)
/*
	This function computes the function derivative; i.e. forces. */
{

	int n=atom_number::n_atom;
	tilde_coordinates tilde_c;
	column_vector res;
	res.set_size(2*n);
	double dt = simulation_parameters::dt;

	// NO NEED TO CALCULATE ANTYHING
	// BECAUSE EVERYTHING IS CALCULATED IN ROSEN AND CAN BE UED HERE THANKS TO THE SINGLETON



	for(int i=0;i<n;i++)
	{
		res(i)  =m(i)   - force_energy::calculation::getInstance().tilde_c_t.x[i] 
						+ force_energy::calculation::getInstance().fo.x(i);
		
		res(i+n)=m(i+n) - force_energy::calculation::getInstance().tilde_c_t.y[i] 
						+ force_energy::calculation::getInstance().fo.y(i);
	}
	

	
	return res/dt;
}

matrix<double> rosen_hessian (const column_vector& m)
{

    matrix<double> hessian(1,1);
       
    return hessian;
}



class rosen_model 
{
    /*!
        This object is a "function model" which can be used with the
        find_min_trust_region() routine.  
    !*/

public:
    typedef ::column_vector column_vector;
    typedef matrix<double> general_matrix;

    double operator() (
        const column_vector& x
    ) const { return rosen(x); }

    void get_derivative_and_hessian (
        const column_vector& x,
        column_vector& der,
        general_matrix& hess
    ) const
    {
        der = rosen_derivative(x);
        hess = rosen_hessian(x);
    }
};




