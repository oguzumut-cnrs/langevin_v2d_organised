#pragma once
#ifndef _INITIAL_CONDITIONS_H_
#define _INITIAL_CONDITIONS_H_
#include "structures.h"

#include "namespaces.h"
#include "input_output.h"

void initiate_coo_2D_triangular_with_grain(coordinates& c);



void initiate_coo_2D_triangular_with_dislocation(coordinates& c);
void initiate_coo_2D_square(coordinates& c);
void initiate_coo_2D_triangular(coordinates& c);

// namespace lennard_jones = lennard_jones;


// double  lennard_jones_energy(double r)
// {
// 	int it=0;
// 	int jt=0;
// 	
// 	double n_sur_m = (double) lennard_jones::nn/  (double)lennard_jones::mm;
// // 	double energy=0;
// 	
// 
// 	matrix2D a;
// 	matrix2D r0;
// 
// 
// 	a(0,0) = lennard_jones::a_11;
// 	a(0,1) = lennard_jones::a_12;
// 	a(1,0) = lennard_jones::a_12;
// 	a(1,1) = lennard_jones::a_22;
// 
// 
// 	r0(0,1) = lennard_jones::r0_12;
// 	r0(1,0) = lennard_jones::r0_12;
// 	r0(1,1) = lennard_jones::r0_22;
// 
// 	double energy = pow((1./r),lennard_jones::nn)- pow((1./r),lennard_jones::mm);
// 	return energy;
// 
// }


// double  lennard_jones_force(double r)
// {
// 	int it=0;
// 	int jt=0;
// 	matrix2D a;
// 	matrix2D r0;
// 	a(0,0) = lennard_jones::a_11;
// 	a(0,1) = lennard_jones::a_12;
// 	a(1,0) = lennard_jones::a_12;
// 	a(1,1) = lennard_jones::a_22;
// 
// 
// 	r0(0,1) = lennard_jones::r0_12;
// 	r0(1,0) = lennard_jones::r0_12;
// 	r0(1,1) = lennard_jones::r0_22;
// 
// 	double f0 = lennard_jones::nn*a(it,jt) * ( pow((r0(it,jt)/r),lennard_jones::nn-1) -pow((r0(it,jt)/r),lennard_jones::mm-1)  )*(r0(it,jt)/(r*r*r));
// 
// 	return f0;
// 
// }



// 		   f0 = nn*a(it,jt) * ( (r0(it,jt)/r)**(nn-1) - (r0(it,jt)/r)**(mm-1) ) * r0(it,jt)/r**3 / div
// 		   e0 =    a(it,jt) * ( (r0(it,jt)/r)**nn - n_sur_m*((r0(it,jt)/r)**mm ) ) / div













#endif //
