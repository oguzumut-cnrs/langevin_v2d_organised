#include <cstddef>
#include <iostream>

#include <boost/random.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "polar_decomposition.h"
// #include "rotation.h"
// #include "dlib.h"
#include <dlib/matrix.h>

#include "dlib/threads.h"
#include <dlib/optimization.h>
#include "dlib/graph_utils_threaded.h"
#include "dlib/threads.h"
#include <dlib/gui_widgets.h>
#include <dlib/image_transforms.h>

using namespace dlib;
double toDegrees(double angle);
double example(const matrix<double>& M)
{
   	double angle,temp;
    boost::mt19937 seed(42);
    boost::uniform_real<> dist(-10.0, 10.0);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<> > random(seed, dist);

    boost::numeric::ublas::matrix<double> A(3,3), U, H;
//      matrix<double> M(3,3);
     
     std::cout<<real_eigenvalues(M)<<std::endl;
//     std::generate(A.data().begin(), A.data().end(), 0);

// 	if ( abs(F.m11) < 0.000000000000001) F.m11 = 0;
// 	if ( abs(F.m22) < 0.000000000000001) F.m22 = 0;
// 	if ( abs(F.m12) < 0.000000000000001) F.m12 = 0;
// 	if ( abs(F.m21) < 0.000000000000001) F.m21 = 0;

    
//     A(0,0) =  M(0,0);
//     A(1,1) =  M(1,1);
//     A(0,1) =  M(0,1);
//     A(1,0) =  M(0,1);
//     A(1,2) = 0.;
//     A(2,1) = 0.;
//     A(0,2) = 0.;
//     A(2,0) = 0.;
//     A(2,2) =1;
    

//     for (std::size_t i = 0; i < 100000; ++i)
//     {
//         std::generate(A.data().begin(), A.data().end(), random);
        polar::polar_decomposition(A, U, H);
//     }

//     std::cout <<"A= "<< A << std::endl;
//     std::cout <<"U= "<< U << std::endl;
//     std::cout <<"H= "<< H << std::endl;
//     if(H(0,0)<1 && H(0,0) >0){ 
//     	angle=acos(H(0,0));
//     }
//     else{
//     	angle = 0;
//     }
//     angle=toDegrees(angle);
//     std::cout <<"angle= "<<angle << std::endl;
    
// 
//     std::cout <<"prod= "<< prod(U, trans(U)) << std::endl;
    return angle;
}