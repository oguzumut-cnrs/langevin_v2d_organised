namespace environment
{

	class Parameters
	{
		public:
			static Parameters& getInstance()
			{
				static Parameters    instance; // Guaranteed to be destroyed.
									           // Instantiated on first use.
				return instance;
			}
			
		private:
			Parameters() 
			{				 
			};                   // Constructor? (the {} brackets) are needed here.
			

			// C++ 11
			// =======
			// We can use the better technique of deleting the methods
			// we don't want.
		public:
			Parameters(Parameters const&)      = delete;
			void operator=(Parameters const&)  = delete;

			// Note: Scott Meyers mentions in his Effective Modern
			//       C++ book, that deleted functions should generally
			//       be public as it results in better error messages
			//       due to the compilers behavior to check accessibility
			//       before deleted status
		    int const nx = dimensions::nx;
			int const ny = dimensions::ny;
			int const nz = 1;	
			std::vector<int>   nbl_i,nbl_j;
			tensors F;
			column_vector_int  ind_cell_x,ind_cell_y,deg_cell,cont_cell;
			
// 			void setSize(int n_pair_max)
// 			{
// 				nbl_i.set_size(n_pair_max);
// 				nbl_j.set_size(n_pair_max);				 
// 			}
			
			void setSize_cell_arrays(int n,int nb_cell_x, int nb_cell_y,int n_atom_per_cell)
			{
  				ind_cell_x.set_size(n);
  				ind_cell_y.set_size(n);
  				deg_cell.set_size(nb_cell_x*nb_cell_y); 
  				cont_cell.set_size(n_atom_per_cell*nb_cell_x*nb_cell_y);
			}			
			
			
	};


}