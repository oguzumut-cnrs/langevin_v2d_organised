#pragma once
#ifndef _PRINCIPALS_H_
#define _PRINCIPALS_H_

#include "structures.h"
#include "environement.h"
int ind_conv(int nx,int ny,int nz,int i, int j, int k);


// int ind_conv2D(int nx,int ny,int i, int j)
// {
// 	int offset;
// 	offset =i*ny + j;
// 	return offset;
// }
// 
// int ind_conv3D(int nx,int ny,int nz,int i, int j, int k)
// {
// 	int offset;
// // 	offset =i*ny + j;
// 	offset = i * ny * nz + j * nz + k;
// 	return offset;
// }


//FINDING CELLS USING TILDE COORDINATES
void find_cells(tilde_coordinates&  c );

// #include </Users/salman/dlib-19.1/dlib/matrix.h>





#include <boost/math/special_functions/sign.hpp>
//FINDING neighbor_list USING TILDE COORDINATES
void neighbor_list_seq(tilde_coordinates&  tilde_c, tensors& F );



void transform_to_tilde_coordinates( coordinates&  c,tilde_coordinates&  tilde_c, tensors& F );


void transform_to_original_coordinates(coordinates&  c, tilde_coordinates&  tilde_c, tensors& F );
void transform_to_original_coordinates_dlib(coordinates&  c, const column_vector  m, tensors& F );
void	push_transform_to_original_coordinates(coordinates&  c, tilde_coordinates&  tilde_c, tensors& F );


#endif //
