#include "initial_conditions.h"
#include "common.h"

namespace gp = grain_properties;


void initiate_coo_2D_triangular_with_grain(coordinates& c)
{
	int n  = -1;
// 	double d1 = 1.0;
// 	double d2 = sqrt(3.0)/2.0;
	double gamma = pow((4./3.),(1./4.));
	double d1 = info_2D::d1;
	double d2 = info_2D::d2;
	
	int nx=dimensions::nx;
	int ny=dimensions::ny;
	c.n_atom0=nx*ny;
	double xx,yy;
	double  dec_x;

	double  theta_rd = 2*dlib::pi/360 * gp::theta;




	for(int j=0;j<ny;j++)
	{

		if( j % 2 == 0 )   dec_x = 0.5;
		if( j % 2 == 1 )   dec_x = 0.;
	
		for(int i=0;i<nx;i++)
		{	
			xx = d1*(double)i +  gamma*dec_x;
			yy = d2*(double)j;
			int atomic_species = 1;
			double radius = sqrt( pow((xx-nx*d1/2.0),2.) + pow((yy-ny*d2/2.0),2.) );

// 			if( radius < gp::grain_radius*nx ) 
// 			{
// 			   double dxx = xx - nx/2.0;
// 			   double dyy = yy - ny/2.0;    
// 			   xx  = nx/2 + (cos(theta_rd)*dxx - sin(theta_rd)*dyy)*gp::shrink-gp::shrink*2;
// 			   yy  = ny/2 + (sin(theta_rd)*dxx + cos(theta_rd)*dyy)*gp::shrink-gp::shrink*1.4;
// 		//        atomic_species = 2
// 			}

// 			if( j==ny/2 && i==nx/2) 
// 				continue; 

			
			n += 1;
			c.x.push_back(xx);// = xx; 
			c.y.push_back(yy);// = yy;
		// 	c.z(n) = zz;
		//     c.species(n) = atomic_species;

		}
	}

	int n_atom = n;
	c.set_values (n_atom+1);
	// const matrix_exp::type max 
	double x_range = 1.1 * (  *max_element(c.x.begin(), c.x.end()) -  *min_element(c.x.begin(), c.x.end()) );
	double y_range = 1.1 * (  *max_element(c.y.begin(), c.y.end()) -  *min_element(c.y.begin(), c.y.end()) );
	c.set_values_range (x_range,y_range);
	cout<<"x_range = "<<c.x_range()<<"\n";
	cout<<"y_range = "<<c.y_range()<<"\n";
 
}



void initiate_coo_2D_triangular_with_dislocation(coordinates& c)
{
	int n  = -1;
	double d1 = 1.0;
	double d2 = sqrt(3.0)/2.0;
	int nx=dimensions::nx;
	int ny=dimensions::ny;
	c.n_atom0=nx*ny;
	double xx,yy;
	double  dec_x;

	double  theta_rd = 2*dlib::pi/360 * gp::theta;



	//this loop creates the perfect crytals
	for(int j=0;j<ny;j++)
	{

		if( j % 2 == 0 )   dec_x = 0.5;
		if( j % 2 == 1 )   dec_x = 0.;
	
		for(int i=0;i<nx;i++)
		{	
			xx = d1*(double)i + dec_x;
			yy = d2*(double)j;
			int atomic_species = 1;

			
			n += 1;
			c.x.push_back(xx);// = xx; 
			c.y.push_back(yy);// = yy;
		// 	c.z(n) = zz;
		//     c.species(n) = atomic_species;

		}
	}

	int n_atom = n;
	c.set_values (n_atom+1);
	// const matrix_exp::type max 
	double x_range = 1.1 * (  *max_element(c.x.begin(), c.x.end()) -  *min_element(c.x.begin(), c.x.end()) );
	double y_range = 1.1 * (  *max_element(c.y.begin(), c.y.end()) -  *min_element(c.y.begin(), c.y.end()) );
	c.set_values_range (x_range,y_range);
	cout<<"x_range = "<<c.x_range()<<"\n";
	cout<<"y_range = "<<c.y_range()<<"\n";

	    
    column_vector ux,uy;
	ux.set_size(n_atom+1);
	uy.set_size(n_atom+1);
	for(int i=0;i<n_atom+1;i++) ux(i)=uy(i)=0.;
	//this loop insert an edge dislocation
	const double pi = 3.1415926535897;
	fstream dislo_u;
	dislo_u.open ("dislo_u.dat", fstream::in | fstream::out | fstream::app);
	fstream dislo_v;
	dislo_v.open ("dislo_v.dat", fstream::in | fstream::out | fstream::app);


	
	for(int i=0;i<n_atom+1;i++)
	{
		yy = c.x[i]-  (x_range/2+50);
		xx = c.y[i]-  (y_range/2); 

		double xx2 = pow(xx,2.);
		double yy2 = pow(yy,2.);
		double mu=0.3;
		
		ux(i)  = .8*(atan(yy/xx) + xx*yy/( 2*(1.-mu)*(xx2+yy2)   ))/(2.*pi);
		uy(i)  = .8*((1.-2.*mu)/(4*(1.-mu))*log(xx2+yy2)+ (xx2-yy2)/(4.*(1-mu)*(xx2+yy2)))/(2.*pi);

//         	dislo_u<<std::scientific << std::setprecision(7)<<ux(i)<<endl;
//         	dislo_v<<std::scientific << std::setprecision(7)<<uy(i)<<endl; 

		
	}
	
// 	for(int i=0;i<n_atom+1;i++)
// 	{
// 		yy = c.x[i]-  (x_range/2-50);
// 		xx = c.y[i]-  (y_range/2-10); 
// 
// 		double xx2 = pow(xx,2.);
// 		double yy2 = pow(yy,2.);
// 		double mu=0.3;
// 		
// 		ux(i)  -= .8*(atan	(yy/xx) + xx*yy/( 2*(1.-mu)*(xx2+yy2)   ))/(2.*pi);
// 		uy(i)  -= .8*((1.-2.*mu)/(4*(1.-mu))*log(xx2+yy2)+ (xx2-yy2)/(4.*(1-mu)*(xx2+yy2)))/(2.*pi);
// 
// //         	dislo_u<<std::scientific << std::setprecision(7)<<ux(i)<<endl;
// //         	dislo_v<<std::scientific << std::setprecision(7)<<uy(i)<<endl; 
// 
// 		
// 	}	
	
		
			
			dislo_u.close();
			dislo_v.close();

			int i=0;
			cout<<"max u("<<i<<") = "<< max(ux)<<endl;
			cout<<"max v("<<i<<") = "<< max(uy)<<endl;
			cout<<"min u("<<i<<") = "<< min(ux)<<endl;
			cout<<"min v("<<i<<") = "<< min(uy)<<endl;
	
	for(int i=0;i<n_atom+1;i++)
	{
		
		c.x[i]+=ux(i) ;
		c.y[i]+=uy(i) ;
	}
	

 
}


void initiate_coo_2D_triangular(coordinates& c)
{
	int n  = -1;
	double d1 = 1.0;
	double d2 = sqrt(3.0)/2.0;
	int nx=dimensions::nx;
	int ny=dimensions::ny;
	c.n_atom0=nx*ny;
	double xx,yy;
	double  dec_x;
	double gamma = pow((4./3.),(1./4.));
// 	double d1 = info_2D::d1;
// 	double d2 = info_2D::d2;


	double dc = 0.687204444;

	//this loop creates the perfect crytals
	for(int j=0;j<ny;j++)
	{
	
		for(int i=0;i<nx;i++)
		{	


			if(i== 0 || j == 0 || i == nx-1 || j == ny-1)
// 			if(i== 0 || i == nx-1 )
				force_energy::calculation::getInstance().c.boundary.push_back(1);
			else
				force_energy::calculation::getInstance().c.boundary.push_back(0);

// 			if( j % 2 == 0 )   dec_x = 0.5;
// 			if( j % 2 == 1 )   dec_x = 0.;
// 
// 			xx = d1*(double)i +  gamma*dec_x;
// 			yy = d2*(double)j;




			if (j % 2 != 0){
				xx = dc*i*1.0;
				yy=  dc*j*1.0*sqrt(3.) / 2.;
// 			     xx = d1*(double)i + dec_x;
// 			     yy = d2*(double)j;


				n += 1;
				c.x.push_back(xx);// = xx; 
				c.y.push_back(yy);// = yy;
		}

			if (j % 2 == 0){

				xx= dc*(i*1.0 + 0.5);
				yy= dc*j*1.0*sqrt(3) / 2;;
				
// 				xx = d1*(double)i + dec_x;
// 			    yy = d2*(double)j;
				n += 1;
				c.x.push_back(xx);// = xx; 
				c.y.push_back(yy);// = yy;

			}
		}


// 		if( j % 2 == 0 )   dec_x = 0.5;
// 		if( j % 2 == 1 )   dec_x = 0.;
// 	
// 		for(int i=0;i<nx;i++){	
// 			xx = d1*(double)i + dec_x;
// 			yy = d2*(double)j;
// 			int atomic_species = 1;
// 
// 			
// 			n += 1;
// 			c.x.push_back(xx);// = xx; 
// 			c.y.push_back(yy);// = yy;
// 
// 		}
	}

	int n_atom = n;
	c.set_values (n_atom+1);
	// const matrix_exp::type max 
	double x_range = 1.1 * (  *max_element(c.x.begin(), c.x.end()) -  *min_element(c.x.begin(), c.x.end()) );
	double y_range = 1.1 * (  *max_element(c.y.begin(), c.y.end()) -  *min_element(c.y.begin(), c.y.end()) );
	c.set_values_range (x_range,y_range);
	cout<<"x_range = "<<c.x_range()<<"\n";
	cout<<"y_range = "<<c.y_range()<<"\n";
	cout<<"number_of_atoms = "<<c.n_atom()<<"\n";



 
}



void initiate_coo_2D_square(coordinates& c)
{
	int n  = -1;
// 	double d1 = 1.0;
// 	double d2 = sqrt(3.0)/2.0;
	double gamma = pow((4./3.),(1./4.));
	double d1 = info_2D::d1;
	double d2 = info_2D::d2;
	
	int nx=dimensions::nx;
	int ny=dimensions::ny;
	c.n_atom0=nx*ny;
	double xx,yy;
	double  dec_x,dec_y;

	double  theta_rd = 2*dlib::pi/360 * gp::theta;




	for(int j=0;j<ny;j++)
	{

// 		if( j % 2 == 0 )   dec_x = 0.;
// 		if( j % 2 == 1 )   dec_x = 0.;
		dec_x = 0.;
		dec_y = 0.;
	
		for(int i=0;i<nx;i++)
		{	
			dec_y=0;
			if(i > nx/2)
				dec_y = 1.06619;
			xx = 1.06619*(double)i;
// 			yy = 1.06619*(double)j + 2*dec_y;
			yy = 1.06619*(double)j ;

			int atomic_species = 1;



// 			if( (j>=ny/2-10 && j<=ny/2+10 ) && i==nx/2-20) 
// 				continue; 
// 			if( (j>=ny/2-10 && j<=ny/2+10 ) && i==nx/2+20) 
// 				continue; 

			
			n += 1;
			c.x.push_back(xx);// = xx; 
			c.y.push_back(yy);// = yy;
// 			if(i== 0 || j == 0 || i == nx-1 || j == ny-1)
// 				force_energy::calculation::getInstance().c.boundary.push_back(1);
// 			else
				force_energy::calculation::getInstance().c.boundary.push_back(0);
				
		// 	c.z(n) = zz;
		//     c.species(n) = atomic_species;

		}
	}

	int n_atom = n;
	c.set_values (n_atom+1);
	// const matrix_exp::type max 
	double x_range = 1.1 * (  *max_element(c.x.begin(), c.x.end()) -  *min_element(c.x.begin(), c.x.end()) );
	double y_range = 1.1 * (  *max_element(c.y.begin(), c.y.end()) -  *min_element(c.y.begin(), c.y.end()) );
	c.set_values_range (x_range,y_range);
	cout<<"x_range = "<<c.x_range()<<"\n";
	cout<<"y_range = "<<c.y_range()<<"\n";
 
}









