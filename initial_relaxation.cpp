// 	
// 	
// 	//WRITE INITIAL CONDITION

#include "initial_relaxation.h"

#include <random>
#include <vector>
#include <boost/random.hpp>



void initial_relaxation(coordinates& c,tilde_coordinates& tilde_c){
	
	int n=c.x.size();
	int nx=dimensions::nx;
	int ny=dimensions::ny;
	//save initial coordinates
	write_to_a_file_en_2(c,force_energy_relax::calculation::getInstance().fo,0);
	
	column_vector m;
	m.set_size(2*n);

	
	{
		double f11  = abs(c.x[ny/2*nx+nx/2+1]   -  c.x[(ny/2)*nx+nx/2]);
		double f21  = abs(c.y[ny/2*nx+nx/2+1]   -  c.y[(ny/2)*nx+nx/2]);
	
		double f12  = abs(c.x[(ny/2+1)*nx+nx/2]   -  c.x[(ny/2)*nx+nx/2]);
		double f22  = abs(c.y[(ny/2+1)*nx+nx/2]   -  c.y[(ny/2)*nx+nx/2]);
	
		cout<<"f11: "<<f11<<endl;
		cout<<"f22: "<<f22<<endl;
		cout<<"f12: "<<f12<<endl;
		cout<<"f21: "<<f21<<endl;
		cout<<"n: "<<n<<endl;
		cout<<"nx: "<<nx<<endl;
		cout<<"ny: "<<ny<<endl;
		
	}
	//INITIAL RELAXATION

				
	for(int i=0;i<n;i++)
	{
		m(i)   = tilde_c.x[i] ; 
		m(i+n) = tilde_c.y[i] ;
	} 
	clock_t t2;
	t2 = clock();


	double wall0 = get_wall_time();
	double cpu0  = get_cpu_time();

// 					find_min(lbfgs_search_strategy(10),  // USE CG strategy, there are other choices.
// 					objective_delta_stop_strategy(.0000000001).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 					rosen_relax , rosen_derivative_relax , m, -20000);

	find_min(lbfgs_search_strategy(10),  // USE CG strategy, there are other choices.
	objective_delta_stop_strategy(.000000000001).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
	rosen_relax , rosen_derivative_relax , m, -20000);



	t2 = clock() - t2;
	printf ("It took me  (%f seconds) to perform find_min at guessing \n",((float)t2)/CLOCKS_PER_SEC);
// 	cout<<"max after= "<<max(m)<<endl;
// 	cout<<"min after= "<<min(m)<<endl;
	//  Stop timers
	double wall1 = get_wall_time();
	double cpu1  = get_cpu_time();
	cout << "Wall Time = " << wall1 - wall0 << endl;
	cout << "CPU Time  = " << cpu1  - cpu0  << endl;
	
	copy_m_to_tilde(m,tilde_c);

	tensors F;
	initiate_tensors(F);
	transform_to_original_coordinates(c,tilde_c,F);
	
	//save relaxed coordinates
	write_to_a_file_en_2(c,force_energy_relax::calculation::getInstance().fo,1);

    double f11  = abs(c.x[ny/2*nx+nx/2+1]   -  c.x[(ny/2)*nx+nx/2]);
	double f21  = abs(c.y[ny/2*nx+nx/2+1]   -  c.y[(ny/2)*nx+nx/2]);
	
	double f12  = abs(c.x[(ny/2+1)*nx+nx/2]   -  c.x[(ny/2)*nx+nx/2]);
	double f22  = abs(c.y[(ny/2+1)*nx+nx/2]   -  c.y[(ny/2)*nx+nx/2]);
	
	cout<<"f11: "<<f11<<endl;
	cout<<"f22: "<<f22<<endl;
	cout<<"f12: "<<f12<<endl;
	cout<<"f21: "<<f21<<endl;

	cout<<"before: length0_x"<<c.length0_x <<"  "<<lengths::length0_x<< endl;
	cout<<"before: length0_y"<<c.length0_y <<"  "<<lengths::length0_y<< endl;
	c.length0_x = *max_element(c.x.begin(), c.x.end()) + f11/2.;
	c.length0_y = *max_element(c.y.begin(), c.y.end()) + f22;
	lengths::setNum(c.length0_x,c.length0_y);

	cout<<"after: length0_x"<<c.length0_x <<"  "<<lengths::length0_x<< endl;
	cout<<"after: length0_y"<<c.length0_y <<"  "<<lengths::length0_y<< endl;

	cout<<"determinant of F= "<<f11*f22-f12*f21<< endl;
	c.detf = f11*f22-f12*f21;
	
}


void homo_loading_relaxation(coordinates& c,tilde_coordinates& tilde_c,tilde_coordinates& tilde_cfix,struct tensors& F){
	
	int n=c.x.size();
	int nx=dimensions::nx;
	int ny=dimensions::ny;
	static int k=0;
	//save initial coordinates
	cout<<"----before initiate alglib---"<<endl;
	write_to_a_file_en_2(c,force_energy_relax::calculation::getInstance().fo,k++);
	
// 	column_vector m;
// 	m.set_size(2*n);

  	cout<<"----initiate alglib---"<<endl;
  	alglib::setglobalthreading(alglib::parallel);

    alglib::real_1d_array m ,grad;


    std::vector<double> temp_vector;
    temp_vector.resize(2*n);
 
	m.setcontent(temp_vector.size(), &(temp_vector[0]));
	grad.setcontent(temp_vector.size(), &(temp_vector[0]));
	double func=0;
	void *ptr;

	
	
		double f11  = F.grad_def(0,0);
		double f21  = F.grad_def(1,0);
	
		double f12  = F.grad_def(0,1);
		double f22  = F.grad_def(1,1);
  		cout<<"----inside homo_loading_relaxation ---"<<endl;
	
		cout<<"f11: "<<f11<<endl;
		cout<<"f22: "<<f22<<endl;
		cout<<"f12: "<<f12<<endl;
		cout<<"f21: "<<f21<<endl;
		cout<<"n: "<<n<<endl;
		cout<<"nx: "<<nx<<endl;
		cout<<"ny: "<<ny<<endl;
	
	
	//INITIAL RELAXATION

// 			std::vector<double > u_dis = wiener( 0 ,BLK1::d1,c.p.size()  );    
// 			std::vector<double > v_dis = wiener( 0 ,BLK1::d1,c.p.size()  );  
// 			for(int i=0;i<n;i++){
// 				if(check(c.bc_cond[i][k1],2)  && check(c.bc_cond[i][k2],2) ){
// 				  starting_point[i]   +=u_dis[i];
// 				  starting_point[i+n] +=v_dis[i];
// 				}

	std::vector<double> r;
	r.resize(2*n);
	static std::random_device rd;
	static std::default_random_engine generator;
	generator.seed( rd() );   //Now this is seeded differently each time.
	std::normal_distribution<double> dis(0, 0.00001);
// 	std::uniform_real_distribution<double> dis(-0.001, 0.001);


	for (int i = 0; i<2*n; ++i)
		r[i] = 0;

	for (int i = 0; i<2*n; ++i)
		r[i] = dis(generator);

				
	for(int i=0;i<n;i++)
	{
		
		m[i]   = tilde_c.x[i] ; 
		m[i+n] = tilde_c.y[i] ;
		//dontp ut noise on boundaries
		if(force_energy::calculation::getInstance().c.boundary[i]!=1){
			m[i]     +=r[i];
			m[i+n]   +=r[i+n];
		}
	} 
	clock_t t2;
	t2 = clock();


	double wall0 = get_wall_time();
	double cpu0  = get_cpu_time();

// 					find_min(lbfgs_search_strategy(10),  // USE CG strategy, there are other choices.
// 					objective_delta_stop_strategy(.0000000001).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 					rosen_relax , rosen_derivative_relax , m, -20000);



//     column_vector sp_max,sp_min;
// 	sp_max.set_size(2*n);
// 	sp_min.set_size(2*n);


//to make the model scalar

// 	for(int i=0;i<n;i++)
// 	{			
// 				sp_max(i)      = 1000  ;
// 				sp_min(i)      = -1000  ;
// 				sp_max(i+n)    = 1000  ;
// 				sp_min(i+n)    = -1000  ;
// //fix or free(commented)
// 		if(force_energy::calculation::getInstance().c.boundary[i]==1){
// 			sp_max(i)     = tilde_cfix.x[i];
// 			sp_min(i)     = tilde_cfix.x[i];
// 			sp_max(i+n)     = tilde_cfix.y[i];
// 			sp_min(i+n)     = tilde_cfix.y[i];
// 			
// 		
// 		}
// 
// 	}	
// 	find_min(lbfgs_search_strategy(10),  // USE CG strategy, there are other choices.
// 	objective_delta_stop_strategy(.000000000001).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 	rosen_relax , rosen_derivative_relax , m, -20000);
// find_min_box_constrained(lbfgs_search_strategy(10),  // The 10 here is basically a measure of how much memory L-BFGS will use.
// 					 objective_delta_stop_strategy(1e-10).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 					 rosen_relax, rosen_derivative_relax, m, sp_min,sp_max);



		double epsg =  0.;
		double epsf =  0.;
		double epsx =  0;
		alglib::ae_int_t maxits = 0;
		alglib::minlbfgsstate state;
		alglib::minlbfgsreport rep;


			cout<<"ALGLIB L-FBGS  starts"<<endl;

			minlbfgscreate(12, m, state);
			minlbfgssetcond(state, epsg, epsf, epsx, maxits);
// 			minlbfgssetscale(state, s);
			alglib::minlbfgsoptimize(state, function1_grad);
			minlbfgsresults(state, m, rep);





	t2 = clock() - t2;
	printf ("It took me  (%f seconds) to perform find_min at guessing \n",((float)t2)/CLOCKS_PER_SEC);
// 	cout<<"max after= "<<max(m)<<endl;
// 	cout<<"min after= "<<min(m)<<endl;
	//  Stop timers
	double wall1 = get_wall_time();
	double cpu1  = get_cpu_time();
	cout << "Wall Time = " << wall1 - wall0 << endl;
	cout << "CPU Time  = " << cpu1  - cpu0  << endl;
	
// 	copy_m_to_tilde(m,tilde_c);

	copy_m_to_tilde_alglib(m,tilde_c);


	transform_to_original_coordinates(c,tilde_c,F);
	
	//save relaxed coordinates
	write_to_a_file_en_2(c,force_energy_relax::calculation::getInstance().fo,k++);

	
}



void copy_m_to_tilde(column_vector& m, tilde_coordinates& tilde_c){
	
	
	int n=tilde_c.x.size();
	
	for(int i=0;i<tilde_c.x.size();i++)
	{
		tilde_c.x[i] = m(i);
		tilde_c.y[i] = m(i+n);

		if ( tilde_c.x[i]  < 0 )  
			 tilde_c.x[i] +=  1.0;

		if ( tilde_c.x[i] >= 1 )  
			 tilde_c.x[i] -=  1.0;

		if ( tilde_c.y[i] < 0 )  
			 tilde_c.y[i] += 1.0;

		if ( tilde_c.y[i] >= 1 )  
			 tilde_c.y[i] -= 1.0;
// 
	}  


}


void copy_m_to_tilde_alglib(const alglib::real_1d_array& m , tilde_coordinates& tilde_c){
	
	
	int n=tilde_c.x.size();
	
	for(int i=0;i<tilde_c.x.size();i++)
	{
		tilde_c.x[i] = m[i];
		tilde_c.y[i] = m[i+n];

		if ( tilde_c.x[i]  < 0 )  
			 tilde_c.x[i] +=  1.0;

		if ( tilde_c.x[i] >= 1 )  
			 tilde_c.x[i] -=  1.0;

		if ( tilde_c.y[i] < 0 )  
			 tilde_c.y[i] += 1.0;

		if ( tilde_c.y[i] >= 1 )  
			 tilde_c.y[i] -= 1.0;
// 
	}  


}



