
#include "common2.h"

// #include "structures.h"



double rosen_relax (const column_vector& m)
/*
	This function computes the function; i.e. the total energy. */
{

	int n=atom_number::n_atom;
	tilde_coordinates tilde_c;
	static double ener=0;
	double ener_t1;
	static double delta_ener=0;
	tilde_c.x.resize(n);
	tilde_c.y.resize(n);
	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 


	for(int i=0;i<n;i++)
	{
		tilde_c.x[i] = m(i);
		tilde_c.y[i]  =m(i+n);	
	}
	
	tensors F;
	initiate_tensors(F);


	force_energy_relax::calculation::getInstance().forces_with_nbl_CG(tilde_c,F);	
	double dt = simulation_parameters::dt;
	double ener2 = 0;
	
	for(int i=0;i<n;i++)
	{
// 		if(force_energy::calculation::getInstance().c.boundary[i]==1)
// 			continue;
		ener2+=0.5*force_energy_relax::calculation::getInstance().fo.e(i);
	}


	return ener2/(1.0*n);
}



column_vector rosen_derivative_relax(const column_vector& m)
/*
	This function computes the function derivative; i.e. forces. */
{

	int n=atom_number::n_atom;
	tilde_coordinates tilde_c;
	column_vector res;
	res.set_size(2*n);
	double dt = simulation_parameters::dt;

	// NO NEED TO CALCULATE ANTYHING
	// BECAUSE EVERYTHING IS CALCULATED IN ROSEN AND CAN BE UED HERE THANKS TO THE SINGLETON



	for(int i=0;i<n;i++){
// 		if(force_energy::calculation::getInstance().c.boundary[i]==1)
// 			continue;
			
		res(i)  =  -force_energy_relax::calculation::getInstance().fo.x(i)/(1.0*n);
		res(i+n)=  -force_energy_relax::calculation::getInstance().fo.y(i)/(1.0*n);
	}


		
	
	return res;
}

matrix<double> rosen_hessian_relax (const column_vector& m)
{

    matrix<double> hessian(1,1);
       
    return hessian;
}



