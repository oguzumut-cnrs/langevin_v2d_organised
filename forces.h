#pragma once
#ifndef _FORCES_H_
#define _FORCES_H_

#include "datatable.h"
#include "bspline.h"
#include "structures.h"

#include "namespaces.h"
#include "input_output.h"
#include "common.h"

#include <bsplinebuilder.h>

void forces_with_nbl(tilde_coordinates&  tilde_c,forces_std& fo,tensors& F );
void forces_with_nbl_par(tilde_coordinates&  tilde_c,forces_std& fo,tensors& F );
tilde_coordinates new_coordinates_2D(tilde_coordinates&  tilde_c,
forces_std& fo, 
random_forces_std& rf, 
tilde_coordinates&  cumul,
tensors& F,
const double dt,
const double temperature );

#endif //
