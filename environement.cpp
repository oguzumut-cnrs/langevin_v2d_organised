#include "environement.h"


double environment::Parameters::lennard_jones_energy(double r)
{
	int it=0;
	int jt=0;
	
	double n_sur_m = (double) lennard_jones::nn/  (double)lennard_jones::mm;
// 	n_sur_m=2;
// 	double energy=0;
	

	matrix2D a;
	matrix2D r0;


	a(0,0) = lennard_jones::a_11;
	a(0,1) = lennard_jones::a_12;
	a(1,0) = lennard_jones::a_12;
	a(1,1) = lennard_jones::a_22;

	r0(0,0) = lennard_jones::r0_11;
	r0(0,1) = lennard_jones::r0_12;
	r0(1,0) = lennard_jones::r0_12;
	r0(1,1) = lennard_jones::r0_22;
	double si = 1./1.12246;
	double E=2.71828;


// 	double energy = a(it,jt) *(pow((r0(it,jt)/r),lennard_jones::nn)- n_sur_m*pow((r0(it,jt)/r),lennard_jones::mm));
	//double energy=  -4*(-(pow(si,12)/pow(r,12)) + pow(si,6)/pow(r,6)) -exp(-(r-1.6/0.2)*(r-1.6/0.2)  );
	//double energy=  -4/pow(E,8*pow(-1 + r,2)) + pow(r,-12);
// 	double energy = pow(1./r,8.)- 2.*pow((1./r),4);
// 	DenseVector x(1);
// 	x(0)=r;
// 	double energy =  bspline_lj.eval(x);
// 	double energy = g_s(r);




	//square potential same as in the MTM model 

	double energy=-2/pow(E,8*pow(-1.425 + r,2)) - 2/pow(E,8*pow(-1 + r,2)) + pow(r,-12);
// 	
	//Lennard-Jones of patinet, piece-wise second derivative smooth
	
// 	double energy;
// 	double r_in = -1. + sqrt(5.);
// 	double r_cut = 1.5450849718747373;
// 	
// 	if(r<r_in)
// 		energy =0.014495849609375 + 2.*(pow(-1 + sqrt(5.),12)/(4096.*pow(r,12)) - pow(-1 + sqrt(5),6)/(64.*pow(r,6)));
// 	
// 	else if(r>=r_in && r<r_cut )
// 		energy  =  -0.016265869140625 + 0.1469503524938869*(1 - sqrt(5.) + r) - 0.4045936315621786*pow(1 - sqrt(5.) + r,2) + 
//     0.20683925671385717*pow(1 - sqrt(5.) + r,3) + 0.37148697180357115*pow(1 - sqrt(5.) + r,4);
//     
//     else if(r>=r_cut)
//     	energy=0;

	return energy;

}


double environment::Parameters::lennard_jones_force(double r)
{
// 	namespace lennard_jones = lennard_jones;
	
	int it=0;
	int jt=0;
	matrix2D a;
	matrix2D r0;
	double si = 1./1.12246;
	double E=2.71828;

	a(0,0) = lennard_jones::a_11;
	a(0,1) = lennard_jones::a_12;
	a(1,0) = lennard_jones::a_12;
	a(1,1) = lennard_jones::a_22;

    r0(0,0) = lennard_jones::r0_11;
	r0(0,1) = lennard_jones::r0_12;
	r0(1,0) = lennard_jones::r0_12;
	r0(1,1) = lennard_jones::r0_22;
	

// 	double f0 = lennard_jones::nn*a(it,jt) * ( pow((r0(it,jt)/r),lennard_jones::nn-1) -pow((r0(it,jt)/r),lennard_jones::mm-1)  )*(r0(it,jt)/(r*r*r));
//	double f0 = -4*((12*pow(si,12))/pow(r,13) - (6*pow(si,6))/pow(r,7)) -(exp((8. - r)*(-8. + r))*(16. - 2*r)) ;
// 	double f0 = 8.* ( pow(1./r,7.) -pow(1./r,3)  )/pow(r,3.);
// 	double f0 = -g_s.deriv(1,r)/r;

// 	DenseVector x(1);
// 	x(0)=r;
// 	DenseMatrix jac = bspline_lj.evalJacobian(x);
// 	double f0 = -jac(0)/r;

 
//    f0 = nn*a(it,jt) * ( (r0(it,jt)/r)**(nn-1) - (r0(it,jt)/r)**(mm-1) ) * r0(it,jt)/r**3
//    e0 =    a(it,jt) * ( (r0(it,jt)/r)**nn - n_sur_m*((r0(it,jt)/r)**mm ) )

	//square potential same as in the MTM model 

	double f0 = (32*(-1.425 + r))/pow(E,8*pow(-1.425 + r,2)) + 
   (32*(-1 + r))/pow(E,8*pow(-1 + r,2)) - 12/pow(r,13);


// 	double r_in = -1. + sqrt(5.);
// 	double r_cut = 1.5450849718747373;
// 
// 
// 	double f0;
// 	
// 	if(r<r_in)
// 		f0 =2.*((-3*pow(-1 + sqrt(5.),12))/(1024.*pow(r,13)) + (3*pow(-1 + sqrt(5.),6))/(32.*pow(r,7)));
// 	else if(r>=r_in && r<r_cut )
// 		f0  = 0.1469503524938869 - 0.8091872631243572*(1 - sqrt(5.) + r) + 0.6205177701415715*pow(1 - sqrt(5.) + r,2) + 1.4859478872142846*pow(1 - sqrt(5.) + r,3);
//     else if(r>=r_cut)
//     	f0=0;
	

	return -f0/r;



}





