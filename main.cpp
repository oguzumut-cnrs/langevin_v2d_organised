// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This is an example illustrating the use the general purpose non-linear 
    optimization routines from the dlib C++ Library.

    The library provides implementations of the conjugate gradient,  BFGS,
    L-BFGS, and BOBYQA optimization algorithms.  These algorithms allow you to
    find the minimum of a function of many input variables.  This example walks
    though a few of the ways you might put these routines to use.

*/

#include "dlib.h"
#include "structures.h"
#include "environement.h"
#include "namespaces.h"
#include "input_output.h"

#include "initial_conditions.h"
#include "utilities.h"
#include "principals.h"
#include "allocate.h"
#include "initial_relaxation.h"
//#include "reductions.h"
//#include "spline2.h"
#include "common.h"
#include "forces.h"
using namespace dlib;

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>        // std::abs
#include <time.h>       /* time */
#include <math.h>
#include <float.h>
using namespace std;


#include <boost/multi_array.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <vector>
#include <algorithm>
#include <numeric>

#include "ap.h"
#include "stdafx.h"

// #include </Users/salman/dlib-19.1/dlib/matrix.h>



// #include <cassert>

// using namespace SPLINTER;

//global variables
// const char *g_fileName = "/Users/salman/splines/enlin.bspline";
// BSpline energy_sp(g_fileName);
// #include "spline.cpp"
// tk::spline g_s = spline();
// BSpline bspline_lj = spline();






int main()
{
	//REQUIRED TO STOP THE BY ERASING THE FILE ROULE
    const char* filename = "roule";
    ofstream myfile;
    myfile.open ("roule");
    read_input_file();
    boost::mt19937 rng(std::time(0));
    


	int nx = environment::Parameters::getInstance().nx;
	int ny = environment::Parameters::getInstance().ny;

// 	//write sizes on a file information_tailles
	ofstream sizes;
	sizes.open ("information_tailles");
	sizes<<" "<<nx<<"   "<<ny<<"   "<<1<<"   "<<"mode sauvegarde: complete"<<"\n"<<" 3"<<"	"<<"0"<<"\n";
	sizes.close();
    
 
// 	//c stands for coordinates; no need to allocate
// 	//allocation dynamically happens during initial conditions assignement
 	coordinates c,c2;
	if(memory_file::answer.compare("n") ==0)
	{
	initiate_coo_2D_square(c);
// 		initiate_coo_2D_triangular_with_grain(c);
// 		initiate_coo_2D_triangular_with_dislocation(c2);
// 		initiate_coo_2D_triangular(c);
		c2=c;
	}
	tensors F;
	matrix2D virial;
	if(memory_file::answer.compare("y") ==0)
	{
		read_from_a_file(c);
		read_from_a_file(c2);
		read_from_a_file_lengths(F);
		initiate_tensors(F);
		c.length0_x = lengths::length0_x;
		c.length0_y = lengths::length0_y;
	}
// // 	std::vector<perspective_window::overlay_dot> points;
// // 
// //     for (int i = 0; i < c.x.size(); ++i)
// //     {
// //         // Get a point on a spiral
// //         dlib::vector<double> val(0,c.x[i],c.y[i]);
// // 
// //         // Pick a color based on how far we are along the spiral
// //         rgb_pixel color = colormap_jet(c.x[i],c.y[i],maxval(c.x));
// //         
// // 
// //         // And add the point to the list of points we will display
// //         points.push_back(perspective_window::overlay_dot(val, color));
// //     }
// // 	cout<<" Now finally display the point cloud"<<endl;
// //     // Now finally display the point cloud.
// //     perspective_window win;
// // //     win.fit_to_contents():
// // //     rgb_pixel color2=colormap_jet(255,255,255);
// //     win.set_background_color(0.2,1.1,0.4);
// //     win.set_title("perspective_window 3D point cloud");
// //     win.add_overlay(points);
// //     win.wait_until_closed();
// 
// // 
// // 	test_knn2(c,4);
// 
	int k=0;
// 	write_to_a_file(c,k++);
	if(memory_file::answer.compare("n") ==0)
	{
		//hex
		double dc = 0.687204444;
// 		c.length0_x = *max_element(c.x.begin(), c.x.end()) + dc/2.;
// 		c.length0_y = *max_element(c.y.begin(), c.y.end()) + dc*sqrt(3.)/2.;


		dc = 1.06619;

		//square
		c.length0_x = *max_element(c.x.begin(), c.x.end()) + dc*1.;
		c.length0_y = *max_element(c.y.begin(), c.y.end()) + dc*1.;


		lengths::setNum(c.length0_x,c.length0_y);
		environment::Parameters::getInstance().F.grad_def(0,0)=1.; 
    	environment::Parameters::getInstance().F.grad_def(1,1)=1.; 
    	environment::Parameters::getInstance().F.grad_def(0,1)=0.; 
        environment::Parameters::getInstance().F.grad_def(1,0)=0.; 
		initiate_tensors(F);
	}	
	cout<<"c.length0_x= "<<c.length0_x<<endl;
	cout<<"c.length0_y= "<<c.length0_y<<endl;
// 
	c.set_values (c.x.size());

	atom_number::setNum(c.n_atom());
	cout<<"atom number= "<<c.n_atom()<<endl;

	//tilde_c stands for  tildecoordinates; no need to allocate
	//allocation dynamically happens during transform_to_tilde_coordinates
	tilde_coordinates tilde_c,tilde_cfix;
    


	
	box_calculations(F);
	int n =c.n_atom();

	int nb_cell_x=box_informations::nb_cell_x;
	int nb_cell_y=box_informations::nb_cell_y;
	int n_atom_per_cell=box_informations::n_atom_per_cell;
// 	//USING SINGLETON PARAMATERS
// 	//ARGUMENTS nb_cell_x,nb_cell_y are GLOBALS
	initiate_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
	transform_to_tilde_coordinates(c,tilde_c,F);
// // 	cout<<"bool for nn : "<<find_nn(c, 10)<<endl;
// 
	for(int i=0;i<tilde_c.x.size();i++)
	{
	
		if ( tilde_c.x[i]  < 0 )  
			 tilde_c.x[i] +=  1.0;

		if ( tilde_c.x[i] >= 1 )  
			 tilde_c.x[i] -=  1.0;

		if ( tilde_c.y[i] < 0 )  
			 tilde_c.y[i] += 1.0;
	
		if ( tilde_c.y[i] >= 1 )  
			 tilde_c.y[i] -= 1.0;
		 
	} 
  	


	find_cells(tilde_c);
	neighbor_list_seq(tilde_c,F);



//f and rf are deternministic and stochastic forces respectively
//they are allocated using the number of atoms
//they are not std vector but dlib vectors
	forces_std fo,fo1,fo2,fo3,fo4;
	
	initiate_std_forces(fo,n);
	initiate_std_forces(fo1,n);
	initiate_std_forces(fo2,n);
	initiate_std_forces(fo3,n);
    initiate_std_forces(fo4,n);

	random_forces_std rf;
	rf.x=wiener(rng);
    rf.y=wiener(rng);

	
	tilde_coordinates tilde_c1=tilde_c;
	tilde_coordinates tilde_c2;
	tilde_coordinates tilde_c3;
	tilde_coordinates tilde_c4;
	
	tilde_coordinates cumul;
	int n_atom = n;
	

	cumul.x.assign (n_atom,0); // n_atom doubles with a value of 0
	cumul.y.assign (n_atom,0); // n_atom doubles with a value of 0
	
	tilde_coordinates cumul_dumb;
	cumul_dumb.x.assign (n_atom,0); // n_atom doubles with a value of 0
	cumul_dumb.y.assign (n_atom,0); // n_atom doubles with a value of 0



	double dt = simulation_parameters::dt;
	cout<<"dt= "<<simulation_parameters::dt<<endl;
	double dt_small=dt;
	double temperature=simulation_parameters::temperature;
	int save=simulation_parameters::save_period;
	cout<<"temperature= "<<simulation_parameters::temperature<<endl;
	double counter=0;
	double counter_nn=0;
	std::vector<double> delta_r_sq;

	double x = simulation_parameters::dt_predictor;
    double dt_ad=dt/x;
    cout<<"dt_predictor= "<<dt_ad<<endl;
    
     
    cout<<"initiate_fo"<<endl;
	initiate_fo(n);	
	initiate_cumul(n);
	initiate_fo_relax(n);	
	initiate_cumul_relax(n);
// 	
// 
//     
    column_vector m;
	m.set_size(2*n);
	column_vector m_old;
	m_old.set_size(2*n);
	
	for(int i=0;i<tilde_c.x.size();i++){
	
			 tilde_cfix.x.push_back(tilde_c.x[i]) ;
			 tilde_cfix.y.push_back(tilde_c.y[i]) ;

		 
	} 
	lennard_jones::setNum();
// 	force_energy::calculation::getInstance().keep=c;
	
	{
 

// 		cout<<"initial_relaxation"<<endl;
// 		initial_relaxation(c,tilde_c);

// 		write_to_a_file(c,k++);
// 
// 	    cout<<"coarse_grain"<<endl;
		std::vector<double>  angles;
		
		angles.push_back(0);
// // 		angles.push_back(30.);
// 		angles.push_back(45);
// // 		angles.push_back(135.);
// 		angles.push_back(atan(1./ 2.)*180./dlib::pi);
// // 		angles.push_back(atan(2.    )*180./dlib::pi);
// 		angles.push_back(90.);
// 		angles.push_back(45.);
// // 		angles.push_back(120.);
// // 		angles.push_back(150.);
// // 		angles.push_back(180.);
// //  		
// 		for(int i=0;i<angles.size();++i){
// 			coarse_grain(tilde_c,c,angles[i]);
// 		}
// 		cout<<"SPLINING"<<endl;
// 		spline(tilde_c,c);

		double alpha=0.0; 
		double angle=angles[0];
		
		//it calculates shearing vectors n and r once for all
		struct shearing_vectors base1= create_vectors(c,angle);
		
		
		for(int counter=0;counter<100000;counter++){
			
			cout<<"shear_using_dual_lattice"<<endl;
			F = shear_using_dual_lattice(base1,alpha);
			
			cout<<"copy shear_using_dual_lattice"<<endl;
			environment::Parameters::getInstance().F =F;
			
			cout<<"homo_loading_relaxation"<<endl; 
			homo_loading_relaxation(c,tilde_c,tilde_cfix, F);
			
			if(counter%50==0){
				cout<<"write_to_a_file"<<endl; 
				write_to_a_file(c,k++);
			}
			alpha+=0.00025;
			cout<<"find_cells"<<endl; 
			find_cells(tilde_c);
			
			cout<<"neighbor_list_seq"<<endl; 			
	        neighbor_list_seq(tilde_c,F);

		}


	}

	



	
	
	
	
	
	
	
	
	
	
	
	
// 	for(int i=0;i<n;i++)
// 	{
// 		force_energy::calculation::getInstance().tilde_c_t.x[i]  =tilde_c.x[i];
// 		force_energy::calculation::getInstance().tilde_c_t.y[i]  =tilde_c.y[i];
// 	} 	

// 	double delta_r_max;
// 	
// 	//MAIN LOOP
// 
// 	for(int iter=0;iter<simulation_duration::n_duration;iter++)
// 	{
//     
// 		if ( delta_r_max >= info_2D::r_skin/2.0 )
// 		
// 		{
// 			counter++;
// // 			if(check_tensors(F) == 1)
// // 			{
// // 				box_calculations(F);
// // 				nb_cell_x=bi::nb_cell_x;
// // 				nb_cell_y=bi::nb_cell_y;
// // 	 			n_atom_per_cell=bi::n_atom_per_cell;
// // 				initiate_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
// // 				cout<<"tensors are checked and are changed: "<<"iteration: "<<iter<<"\n";
// // 				cout<<"maxval of x : "<<maxval(c.x)<<"\n";							
// // 			}
// 			
// 			find_cells(tilde_c);			
// 			neighbor_list_seq(tilde_c,F);
// // 			cout<<"relabelling for the "<<counter<<"th time :"<<"iteration and n_pair: "<<iter<<
// // 			" "<<pairing::n_pair<<"\n";
// 			cumul.x.assign (n_atom,0); // n_atom doubles with a value of 0
// 			cumul.y.assign (n_atom,0); // n_atom doubles with a value of 0
// 			cumul_dumb.x.assign (n_atom,0); // n_atom doubles with a value of 0
// 			cumul_dumb.y.assign (n_atom,0); // n_atom doubles with a value of 0
// 
// 		}	       
// // 		if(find_nn(c, 10))
// // 		{
// // 			dt_ad  =0.001;
// // 			sp::dt =0.005;
// // 			cout<<"adapts time step at iteration: "<<iter<<endl;
// // 			counter_nn++;
// // 		}
// // 		else
// // 		{	
// // 				dt_ad=dt/x;
// // 				sp::dt=dt;
// // 
// // 		}
// 
// 		
// 		transform_to_original_coordinates(c2,tilde_c,F);
// 
// 			 		 
// 		
// 		//now advance at time T
// 		//create noise for T+1
// 		force_energy::calculation::getInstance().rf.x=wiener(rng);
// 		force_energy::calculation::getInstance().rf.y=wiener(rng);
// 		
// 	    //EULER //GUESS THE SOLUTION AT TIME T+1
// 	    if(numerical_method::answer.compare("euler") ==0)
// 	    {
// 
// 			//FORCES
// 			forces_with_nbl(force_energy::calculation::getInstance().tilde_c_t,fo1,F); 
// 			// NEW COORDINATES
// 			tilde_c = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo1,
// 					  force_energy::calculation::getInstance().rf,
// 					  cumul_dumb,F,dt_ad,temperature); 
// 
// 			for(int i=0;i<n_atom;i++)
// 			{
// 				fo.x[i] = (fo.x[i]) ;
// 				fo.y[i] = (fo.y[i]) ;
// 				fo.e[i] = (fo.e[i]) ;
// 
// 			}
// 		}
// 		//RUNGE-KUTTA_2
// 		
// 		if(numerical_method::answer.compare("RK2") ==0)
// 	    {
// 			//FORCES
// 			forces_with_nbl_par(force_energy::calculation::getInstance().tilde_c_t,fo1,F); 
// 			// NEW COORDINATES
// 			tilde_c = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo1,
// 					  force_energy::calculation::getInstance().rf,
// 					  cumul_dumb,F,dt_ad,temperature); 
// 
// 	
// 			forces_with_nbl_par(tilde_c,fo2,F);
// 		
// 			for(int i=0;i<n_atom;i++)
// 			{
// 				fo.x[i] = (fo1.x[i]+fo2.x[i])/2. ;
// 				fo.y[i] = (fo1.y[i]+fo2.y[i])/2. ;
// 				fo.e[i] = (fo1.e[i]+fo2.e[i])/2. ;
// 
// 			}
// 		
// 			//GUESS THE SOLUTION AT TIME T+1
// 			tilde_c = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo,
// 					  force_energy::calculation::getInstance().rf,
// 					  cumul_dumb,F,dt_ad,temperature);
// 
// 		}	
// 									 
// 		//RUNGE_KUTTA_4 //GUESS THE SOLUTION AT TIME T+1
// 		if(numerical_method::answer.compare("RK4") ==0)
// 		{
// 			forces_with_nbl(force_energy::calculation::getInstance().tilde_c_t,fo1,F );  //d1		
// 
// 			tilde_c2 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo1,
// 			force_energy::calculation::getInstance().rf,cumul_dumb,F,dt_ad/2.,temperature);
// 		
// 			forces_with_nbl(tilde_c2,fo2,F ); //d2
// 			tilde_c3 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo2,
// 			force_energy::calculation::getInstance().rf,cumul_dumb,F,dt_ad/2.,temperature); 
// 		
// 			forces_with_nbl(tilde_c3,fo3,F ); //d3  
// 			tilde_c4 = new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo3,
// 			force_energy::calculation::getInstance().rf,cumul_dumb,F,dt_ad,temperature);
// 		
// 			forces_with_nbl(tilde_c4,fo4,F ); //d4
// 
// 			for(int i=0;i<n_atom;i++)
// 			{
// 				fo.x[i] = (fo1.x[i]+2*fo2.x[i]+2*fo3.x[i]+fo4.x[i])/6. ;
// 				fo.y[i] = (fo1.y[i]+2*fo2.y[i]+2*fo3.y[i]+fo4.y[i])/6. ;
// 				fo.e[i] = (fo1.e[i]+2*fo2.e[i]+2*fo3.e[i]+fo4.e[i])/6. ;
// 			}
// 		
// 			tilde_c =  new_coordinates_2D(force_energy::calculation::getInstance().tilde_c_t,fo,
// 			force_energy::calculation::getInstance().rf,cumul,F,dt_ad,temperature);
// 			
// 
//         }
//         
//         if(numerical_method::answer2.compare("cg") !=0)
// 		{
// 			transform_to_original_coordinates(c,tilde_c,F);
// 			if(iter %  save ==0)
// 				write_to_a_file_en(c,fo,k++);
// 			for(int i=0;i<n;i++)
// 			{
// 				cumul.x[i] +=c.x[i]-c2.x[i] ;
// 				cumul.y[i] +=c.y[i]-c2.y[i] ;
// 			}
// 
// 			for(int i=0;i<tilde_c.x.size();i++)
// 			{
// 		
// 				if ( tilde_c.x[i]  < 0 )  
// 					 tilde_c.x[i] +=  1.0;
// 	
// 				if ( tilde_c.x[i] >= 1 )  
// 					 tilde_c.x[i] -=  1.0;
// 
// 				if ( tilde_c.y[i] < 0 )  
// 					 tilde_c.y[i] += 1.0;
// 		
// 				if ( tilde_c.y[i] >= 1 )  
// 					 tilde_c.y[i] -= 1.0;
// 			 
// 		
// 				force_energy::calculation::getInstance().tilde_c_t.x[i]  =tilde_c.x[i];
// 				force_energy::calculation::getInstance().tilde_c_t.y[i]  =tilde_c.y[i];
// 			}  
// 		}
//         
//         if(fexists(filename) == false){ cout<<"CODE HAS BEEN STOPPED BY THE USER AT TIME STEP  "<<iter<<"\n";break; }
// 
// 
// 		if(numerical_method::answer2.compare("cg") ==0)
// 		{
//         
// 			for(int i=0;i<n;i++)
// 			{
// 				m(i)   = tilde_c.x[i];
// 				m(i+n) = tilde_c.y[i];
// 			} 
// 
// 		// find_min(cg_search_strategy(),  // USE CG strategy, there are other choices.
// 		// objective_delta_stop_strategy(1e-1).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 		// rosen, rosen_derivative, m, -250);
// 			m_old=m;
// 			clock_t t;
// 			t = clock();
// 		// find_min(bfgs_search_strategy(),  // USE CG strategy, there are other choices.
// 		// objective_delta_stop_strategy(0.01).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 		// rosen, rosen_derivative, m, -20300);
// 		// 	cout<<"max before= "<<max(m)<<endl;
// 		// 	cout<<"min before= "<<min(m)<<endl;
// 			//  Start Timers
// 			double wall0 = get_wall_time();
// 			double cpu0  = get_cpu_time();
// 
// 			find_min(lbfgs_search_strategy(8),  // USE CG strategy, there are other choices.
// 			objective_delta_stop_strategy(.1).be_verbose(),  // Adding be_verbose() causes a message to be  printed for each iteration of optimization
// 			rosen, rosen_derivative, m, -200000300);
// 			t = clock() - t;
// 			printf ("It took me  (%f seconds) to perform find_min at iteration %d \n",((float)t)/CLOCKS_PER_SEC,iter);
// 		// 	cout<<"max after= "<<max(m)<<endl;
// 		// 	cout<<"min after= "<<min(m)<<endl;
// 			//  Stop timers
// 			double wall1 = get_wall_time();
// 			double cpu1  = get_cpu_time();
// 
// 			cout << "Wall Time = " << wall1 - wall0 << endl;
// 			cout << "CPU Time  = " << cpu1  - cpu0  << endl;
//    
// 			transform_to_original_coordinates_dlib(c,m,F);
// 
// 
// 			if(iter %  save ==0)
// 				write_to_a_file_en_2(c,force_energy::calculation::getInstance().fo,k++);
// 	
// 	
// 			for(int i=0;i<n;i++)
// 			{
// 				cumul.x[i] +=c.x[i]-c2.x[i] ;
// 				cumul.y[i] +=c.y[i]-c2.y[i] ;
// 			}
// 	
// 
// 			for(int i=0;i<tilde_c.x.size();i++)
// 			{
// 				tilde_c.x[i] = m(i);
// 				tilde_c.y[i] = m(i+n);
// 			
// 				if ( tilde_c.x[i]  < 0 )  
// 					 tilde_c.x[i] +=  1.0;
// 		
// 				if ( tilde_c.x[i] >= 1 )  
// 					 tilde_c.x[i] -=  1.0;
// 
// 				if ( tilde_c.y[i] < 0 )  
// 					 tilde_c.y[i] += 1.0;
// 			
// 				if ( tilde_c.y[i] >= 1 )  
// 					 tilde_c.y[i] -= 1.0;
// 				 
// 			
// 				force_energy::calculation::getInstance().tilde_c_t.x[i]  =tilde_c.x[i];
// 				force_energy::calculation::getInstance().tilde_c_t.y[i]  =tilde_c.y[i];
// 			}  
// 		
// 		}
//         
//         if(iter %  100 ==0)
//         {
// 			fstream energy_stream;
// 			energy_stream.open ("total_energy.dat", fstream::in | fstream::out | fstream::app);
// 
// 			double total_energy;
// 			if(numerical_method::answer2.compare("cg") ==0)
// 				total_energy = std::accumulate( force_energy::calculation::getInstance().fo.e.begin(), 
// 							force_energy::calculation::getInstance().fo.e.end(), 0.0);
// 
// 			if(numerical_method::answer2.compare("cg") !=0)
// 				total_energy = std::accumulate(fo.e.begin(),fo.e.end(),0.0);
// 
// 		
//         	energy_stream<<std::scientific << std::setprecision(7)
//          	<<sp::dt*iter<<" "<<0.5*total_energy<<"\n"; 
// 
// 			delta_r_sq = sum_of_sq_mn(cumul.x,cumul.y);
// 			delta_r_max   = sqrt( maxval(delta_r_sq));
// 			// cout<<"RUNNING ITERATION # "<<iter<<endl;
// 			cout<<"delta_r_max: "<<delta_r_max<<" info_2D::r_skin/2.0: "<<info_2D::r_skin/2.0<<endl; 
// 		}
//         
//         
//         
//         //CHECK THE NECESSITY OF NBL
// 
// 
// 	}  
// 			find_cells(tilde_c);			
// 			neighbor_list_seq(tilde_c,F);


	 fstream energy_stream;
	 energy_stream.open ("total_energy.dat", fstream::in | fstream::out | fstream::app);

    cout<<"main loop starts with dt "<<dt<<"\n";
    for(int iter=0;iter<100000;iter++)
    {
		
// 		if(find_nn(c, 10))
// 		{
// 			dt_ad=dt_small;
// 			cout<<"iter = "<<iter<<endl;
// 			counter_nn++;
// 		}
// 		else
// 		{	if(counter_nn != 0)
// 				dt_ad=dt;
// 		}
 		
		forces_with_nbl(tilde_c,fo1,F );  //d1		
		tilde_c2 = new_coordinates_2D(tilde_c,fo1,rf,cumul_dumb,F,dt_ad/2.,temperature);
		forces_with_nbl(tilde_c2,fo2,F ); //d2
		tilde_c3 = new_coordinates_2D(tilde_c,fo2,rf,cumul_dumb,F,dt_ad/2.,temperature); 
		forces_with_nbl(tilde_c3,fo3,F ); //d3  
		tilde_c4 = new_coordinates_2D(tilde_c,fo3,rf,cumul_dumb,F,dt_ad,temperature);
		forces_with_nbl(tilde_c4,fo4,F ); //d4
        
        for(int i=0;i<n_atom;i++)
        {
			fo.x[i] = (fo1.x[i]+2*fo2.x[i]+2*fo3.x[i]+fo4.x[i])/6. ;
			fo.y[i] = (fo1.y[i]+2*fo2.y[i]+2*fo3.y[i]+fo4.y[i])/6. ;
	    }
        
        tilde_c =  new_coordinates_2D(tilde_c,fo,rf,cumul,F,dt_ad,temperature);
		rf.x=wiener(rng);
    	rf.y=wiener(rng);
        
        if(iter % 1000 ==0)
        {
        	transform_to_original_coordinates(c,tilde_c,F); 
        	write_to_a_file(c,k++);

        	cout<<"n_pair = "<<pairing::n_pair<<" at iter : "<<iter<<endl;        	
        	cout<<"adaptive = "<<counter_nn<<" at iter : "<<iter<<endl;          	
        }  
        
        
        double total_energy = std::accumulate( fo1.e.begin(), fo1.e.end(), 0.0);
        
        energy_stream<<std::scientific << std::setprecision(7)
         <<dt_ad*iter<<" "<<total_energy<<"\n"; 
   



        
        
        delta_r_sq = sum_of_sq_mn(cumul.x,cumul.y);
        double delta_r_max   = sqrt( maxval(delta_r_sq));
		if ( delta_r_max >= info_2D::r_skin/2.0 )
		
		{
			counter++;
			if(check_tensors(F) == 1)
			{
				box_calculations(F);
				nb_cell_x=box_informations::nb_cell_x;
				nb_cell_y=box_informations::nb_cell_y;
	 			n_atom_per_cell=box_informations::n_atom_per_cell;
				initiate_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
				cout<<"tensors are checked and are changed: "<<"iteration: "<<iter<<"\n";
				cout<<"maxval of x : "<<maxval(c.x)<<"\n";							
			}
			find_cells(tilde_c);			
			neighbor_list_seq(tilde_c,F);
// 			cout<<"relabelling for the "<<counter<<"th time :"<<"iteration and n_pair: "<<iter<<
// 			" "<<pairing::n_pair<<"\n";
			cumul.x.assign (n_atom,0); // n_atom doubles with a value of 0
			cumul.y.assign (n_atom,0); // n_atom doubles with a value of 0
// 		
		}
//    
    	if(fexists(filename) == false)
    	{ 
    		cout<<"CODE HAS BEEN STOPPED BY THE USER AT TIME STEP  "<<iter<<"\n";
    		break; 
    	}

	}
	

    
}




