#pragma once
#ifndef _COMMON3_ALGLIB_H_
#define _COMMON3_ALGLIB_H_
#include "structures.h"
#include "environement.h"
#include "namespaces.h"
#include "dlib.h"
#include "common.h"
#include "common2.h"
#include "ap.h"

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "optimization.h"
#include "principals.h"
#include "utilities.h"

void function1_grad(const alglib::real_1d_array &x, double &func, alglib::real_1d_array &grad, void *ptr) ;

#endif //