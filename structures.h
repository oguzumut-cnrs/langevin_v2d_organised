#pragma once
#ifndef _STRUCTURES_H_
#define _STRUCTURES_H_



#include <iostream>
#include <functional> //without .h
#include <dlib/matrix.h>

#include "dlib/threads.h"
#include <dlib/optimization.h>
#include "dlib/graph_utils_threaded.h"
#include "dlib/threads.h"
#include <dlib/gui_widgets.h>
#include <dlib/image_transforms.h>


#include <fstream>
#include <string>
#include <sstream>
#include <cmath>        // std::abs
#include <time.h>       /* time */
#include <math.h>
#include <float.h>

//#include "mkl_dfti.h"



// #include "daal.h"
// #include "service.h"

#include <algorithm>
#include <iomanip>
#include <stack>
#include <vector>
typedef unsigned char byte;
using namespace std;
// #include "/Users/salman/dlib-19.1/dlib/threads.h"
// #include "/Users/salman/dlib-19.1/dlib/misc_api.h"  // for dlib::sleep
// #include "/Users/salman/dlib-18.17/dlib/all/source.cpp"  // for dlib::sleep



// #define pi 3.14159265358979323846264338327950288419716939937510

#include <boost/multi_array.hpp>


// #include </Users/salman/dlib-19.1/dlib/matrix.h>

using namespace dlib;
typedef matrix<double,0,1> column_vector;
typedef matrix<int,0,1> column_vector_int;
typedef matrix<double,2,2> matrix2D;

#include <cassert>


class coordinates {
  int n;
  double x_range0;
  double y_range0;
  public:
    void set_values (int);
    void set_values_range (double,double);
    int n_atom() {return n;}
    double x_range() {return x_range0;};
    double y_range() {return y_range0;};
    std::vector<double>   x;
    std::vector<double>   y;
    column_vector  z; 
    int n_atom0;
    double length0_x;
    double length0_y;
    double detf;
    int n_pair;
    std::vector<int> boundary;
    
  
};






class tilde_coordinates {
  public:
    std::vector<double>  x;
    std::vector<double>  y;
    std::vector<double>  z; 
};




class forces_std 
{
	public:
		std::vector<double>  x;
		std::vector<double>  y;
		std::vector<double>  e;
		
// 		std::vector<double>  x_old;
// 		std::vector<double>  y_old;
// 	
// 		std::vector<double>  x_cell;
// 		std::vector<double>  y_cell;
// 	
// 		std::vector<double>  dx_cumul;
// 		std::vector<double>  dy_cumul;
// 	
// 		std::vector<double> delta_r_sq;
	
		double sum_ener;

};


class forces 
{

	public:
		column_vector  x;
		column_vector  y;
	
		column_vector  e;
		
		column_vector  x_old;
		column_vector  y_old;
	
		column_vector  x_cell;
		column_vector  y_cell;
	
		column_vector  dx_cumul;
		column_vector  dy_cumul;
	
		column_vector delta_r_sq;
	
		double sum_ener;

};


class random_forces {

public:
    column_vector  x;
    column_vector  y;
    
    column_vector  x_old;
    column_vector  y_old;
    
};

class random_forces_std 
{

	public:
		std::vector<double>  x;
		std::vector<double>  y;
	    
};



class tensors {


public:
    matrix2D  pk1;
    matrix2D  grad_def;
    matrix2D  virial;
    matrix2D  cofactor;   
    double det;   
};

class shearing_vectors {
  private:
  public:
  dlib::matrix<double> ar, nr;
  void set_values(void);
  
};


struct cella_stru
{
	double c11;
	double c22;
	double c12;
};


struct eu_stru
{
	struct cella_stru U;
	double energy;
};


// class box_informations {
// 
// public:
//     double  cell_length_x_tilde;
//     double  cell_length_y_tilde;
//     int     nb_cell_x,nb_cell_y,n_atom_per_cell;
//     double  cell_length_x,cell_length_y;
//     int n_pair_max,n_pair;
//        
// };





#endif //