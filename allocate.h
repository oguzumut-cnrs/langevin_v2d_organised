#pragma once
#ifndef _ALLOCATE_H_
#define _ALLOCATE_H_

#include "common.h" 
#include "common2.h" 
// using namespace dimensions;

// void initiate_coo(coordinates& c)
// {
//   int nx = dimensions::nx;
//   int ny = dimensions::ny;
//   int nz = dimensions::nz;
//   int n=nx*ny*nz;
//   c.x.set_size(n);
//   c.y.set_size(n);
//   if(nz > 1)c.z.set_size(n);
// }

// void initiate_tilde_coo(tilde_coordinates& c)
// {
//   int nx = dimensions::nx;
//   int ny = dimensions::ny;
//   int nz = dimensions::nz;
//   int n=nx*ny*nz;
// //   c.x.set_size(n);
// //   c.y.set_size(n);
//   if(nz > 1initiate_tilde_coo)c.z.set_size(n);
// }


void initiate_forces(forces& f, int n)
{
  f.x.set_size(n);
  f.y.set_size(n);
  
  f.e.set_size(n);
 
  f.x_old.set_size(n);
  f.y_old.set_size(n);
  
  f.x_cell.set_size(n);
  f.y_cell.set_size(n);
 
  f.dx_cumul.set_size(n);
  f.dy_cumul.set_size(n);
  
  f.delta_r_sq.set_size(n);
}

void initiate_std_forces(forces_std& f, int n)
{
  f.x.resize(n);
  f.y.resize(n); 
  f.e.resize(n);

}


void initiate_random_forces(random_forces& rf, int n)
{
  rf.x.set_size(n);
  rf.y.set_size(n);
 
  rf.x_old.set_size(n);
  rf.y_old.set_size(n); 
}


void initiate_fo(int n )
{
	force_energy::calculation::getInstance().setSize(n);
}

void initiate_cell_arrays(int n, int nb_cell_x, int nb_cell_y,int n_atom_per_cell)
{
  environment::Parameters::getInstance().setSize_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
}


void initiate_cumul(int n )
{
	force_energy::calculation::getInstance().setSize_cumul(n);
}

void initiate_fo_relax(int n )
{
	force_energy_relax::calculation::getInstance().setSize(n);
}

void initiate_cell_arrays_relax(int n, int nb_cell_x, int nb_cell_y,int n_atom_per_cell)
{
  environment::Parameters::getInstance().setSize_cell_arrays(n,nb_cell_x,nb_cell_y,n_atom_per_cell);
}


void initiate_cumul_relax(int n )
{
	force_energy_relax::calculation::getInstance().setSize_cumul(n);
}

// void initiate_tensors_for_calculations(int n )
// {
// 	force_energy::calculation::getInstance().initiate_tensors(n);
// }


// void initiate_nbl(int n_pair_max )
// {
// 	 environment::Parameters::getInstance().setSize(n_pair_max);
// }

#endif //