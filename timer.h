#pragma once
#ifndef _TIMER_H_
#define _TIMER_H_
#include <time.h>
#include <sys/time.h>
#include "dlib.h"


using namespace dlib;

double get_wall_time();
double get_cpu_time();
double toRadians(double angle);
double toDegrees(double angle);
#endif