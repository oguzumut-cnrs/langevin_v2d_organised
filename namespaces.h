#pragma once

#include <string>

using std::string;
#include <math.h>
#include "dlib.h"

class dimensions{
	public:
	static int  nx ;
	static int  ny ;
	static int  nz ;  
	static void setNum(int,int,int);  
};



class simulation_parameters
{
	public:
	static double  temperature;
	static double  dt;
	static double dt_predictor;
	static int  save_period;
	static  int n_threads;
	static void setNum(double,double,double,int,int);
};



class simulation_duration{
	public:
	static  int  n_duration;
	static void setNum(int);
};


 
 
class memory_file
{
	public:
	static  string answer;
	static string filename;	
	static string filename_lengths;
	static string filename_graddef;
	static void setName(string,string,string,string);
};


class numerical_method{
	public:
	static  string answer;
	static string answer2;
	static void setName(string,string);
};


class lengths
{
   public:
   static double length0_x;
   static double length0_y;
   static void setNum(double ,double );
};


 

class atom_number{
   public:
   static int n_atom;
   static void setNum(int  );
};


 
class pairing{
	public:
	static int n_pair;
	static void setNum(int  );
};







class box_informations{
	public:
	static double  cell_length_x_tilde;
    static double  cell_length_y_tilde;
    static double  cell_length_x,cell_length_y;
    static int     nb_cell_x,nb_cell_y,n_atom_per_cell,n_pair_max;

	static void setNum_cell(double,double);
	static void setNum_cell_nb(int,int,int,int);
};


 

class info_2D{
	public:
	static double      d1 ;
	static 	double     d2 ; 
	static double      r_cutoff;
	static double      r_skin;
	static  double      r_verlet ;
	static  double      cell_length;

// 	double      r_verlet    = r_cutoff + r_skin;
// 	double      cell_length = r_verlet * 1.05;
	static int         n_pair_max_per_atom;
	static void setNum(double,double);
};



namespace grain_properties{
	static  double    theta=4;
	static  double    theta_rd = 2*dlib::pi/360. * theta;
	static  double    shrink =.98;
	static  double    grain_radius=0.2;     
};




class lennard_jones{

	public:
	static int nn ;
	static int mm ;
	static double n_sur_m;
	static double a_11;
	static double a_12;
	static double a_22;
	static double a_21;

	static double r0_11;
	static double r0_12;
	static double r0_21;
	static double r0_22;
	static void setNum(void);

};




inline bool SameSign(float a, float b);



// read(10,*) tamp,ensemble
// read(10,*) tamp,rot_free