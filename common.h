#pragma once
#ifndef _COMMON_H_
#define _COMMON_H_
#include "structures.h"
#include "environement.h"
#include "namespaces.h"
#include "dlib.h"





//everything below is defined for dlib gradient conjugate
// namespace sp=simulation_parameters;
#include "datatable.h"
#include "bspline.h"
#include <bsplinebuilder.h>

using namespace SPLINTER;
namespace force_energy
{

	class calculation
	{
		public:
			static calculation& getInstance()
			{
				static calculation    instance; // Guaranteed to be destroyed.
									           // Instantiated on first use.
				return instance;
			}
			
		private:
			calculation() 
			{				 
			};                   // Constructor? (the {} brackets) are needed here.
			

			// C++ 11
			// =======
			// We can use the better technique of deleting the methods
			// we don't want.
		public:
			calculation(calculation const&)               = delete;
			void operator=(calculation const&)  = delete;

			// Note: Scott Meyers mentions in his Effective Modern
			//       C++ book, that deleted functions should generally
			//       be public as it results in better error messages
			//       due to the compilers behavior to check accessibility
			//       before deleted status
			forces  fo;
			tilde_coordinates tilde_c_t;
			tilde_coordinates cumul;
			coordinates c;
			

			random_forces_std rf;
			random_forces_std rf_t;

			matrix2D virial;
			double lennard_jones_energy(double );
			double lennard_jones_force(double );
			
// 			std::vector<double> sum_of_sq_mn(std::vector<double>& ,std::vector<double>& );
// 			
// 			double maxval(std::vector<double>& );
//             double minval(std::vector<double>& );



			
			void setSize(int n)
			{
				fo.x.set_size(n);
				fo.y.set_size(n);	
				fo.e.set_size(n);	
				tilde_c_t.x.resize(n);
				tilde_c_t.y.resize(n);
				c.x.resize(n);
				c.y.resize(n);
				rf.x.resize(n);
				rf.y.resize(n);
				rf_t.x.resize(n);
				rf_t.y.resize(n);						 
			}

			void setSize_cumul(int n)
			{
				cumul.x.resize(n);	
				cumul.y.resize(n);
// 				cumul.x.assign (n,0); // n_atom doubles with a value of 0
// 	            cumul.y.assign (n,0); // n_atom doubles with a value of 0
				for(int i=0;i<n;i++)
				{
					cumul.x[i]=0;
					cumul.y[i]=0;
				}
			}			
			

			void forces_with_nbl_CG(tilde_coordinates&  tilde_c,tensors& F )
			{

				double length0_x = lengths::length0_x;
				double length0_y = lengths::length0_y; 
				int n_atom = atom_number::n_atom;
// 				double e0,f0,r2,r;
				double inv_visco_x = 1.0/pow(length0_x,2.);  // new
	            double inv_visco_y = 1.0/pow(length0_y,2.);  // new
				double dt=simulation_parameters::dt;
				double temperature=simulation_parameters::temperature;
	            double amp_noise = sqrt(2.*dt*temperature); //(24.*dt*temperature);





			// ----- initialisation virials -----

				F.virial(0,0) = 0;F.virial(1,0) = 0;
				F.virial(0,1) = 0;F.virial(1,1) = 0;

			// ----- calcul des forces -------------------------

				forces_std  f_temp;
				forces_std  f_temp_par;
	

				f_temp.x.resize( n_atom );
				f_temp.y.resize( n_atom );
				f_temp_par.x.resize( pairing::n_pair );
				f_temp_par.y.resize( pairing::n_pair );
				f_temp_par.e.resize( pairing::n_pair );


				for(int i=0;i<fo.x.size();i++)
				{
					fo.x(i)=0;
					fo.y(i)=0;
					fo.e(i)=0;
				}

				int n_threads=simulation_parameters::n_threads;
				parallel_for(n_threads, 0, pairing::n_pair, [&](long n){

				double e0,f0,r2,r;
				double r_cutoff_sq = pow(info_2D::r_cutoff,2.0);
   

// 				for(int n=0;n<pairing::n_pair;n++)
// 				{
// 					cout<<"inside forces_with_nbl n = "<<n<<"\n";
				// int i = cell.nbl_i(n);
				// int j = cell.nbl_j(n);
				
				

					int i = environment::Parameters::getInstance().nbl_i[n] ;
					int j = environment::Parameters::getInstance().nbl_j[n] ;
					
// 					cout<<"inside forces_with_nbl i,j = "<<i<<" "<<j<<" "<<"\n";
// 					cout<<"inside forces_with_nbl j = "<<j<<"\n";
 

					double dx_tilde = tilde_c.x[i] - tilde_c.x[j];
					double dy_tilde = tilde_c.y[i] - tilde_c.y[j];

					if(abs(dx_tilde) > 0.5) 
						dx_tilde -=   copysign(1., dx_tilde) ;
					if(abs(dy_tilde) > 0.5) 
						dy_tilde -=   copysign(1., dy_tilde) ;

					double dx = F.grad_def(0,0)*length0_x*dx_tilde + F.grad_def(0,1)*length0_y*dy_tilde ;
					double dy = F.grad_def(1,0)*length0_x*dx_tilde + F.grad_def(1,1)*length0_y*dy_tilde ;
					r2 =  dx*dx + dy*dy ;

			// 	if( r2 >= r_cutoff_sq ) continue;
					if( r2 < r_cutoff_sq )
					{
						 r = sqrt(r2);

						//    it = species(i)
						//    jt = species(j)
// 						namespace lj = lennard_jones;
						//    
						f0 =  environment::Parameters::lennard_jones_force (r);
						e0 =  environment::Parameters::lennard_jones_energy(r);

// 						DenseVector x(1);
// 						x(0)=r;
// 						e0 =  bspline_lj.eval(x);
// // 						DenseMatrix jac =  -bspline_lj.evalJacobian(x);
// // 						f0 = jac(0)/x(0);
// 						DenseMatrix jac = bspline_lj.evalJacobian(x);
// 						f0 = -jac(0)/r;
// 						e0 = bspline_lj.eval(x);
// 						f0 =  lennard_jones_force (r);
// 						e0 = g_s(r);
// 						f0 = -g_s.deriv(1,r)/r;
						
						
// 						cout<< e0 << " " << bspline_lj.eval(x)<<endl;
// 						cout<< std::setprecision(7)<< "n:"<< n << "r: "<< r<< "; " <<f0 << " " << -jac(0)/r <<endl;
						
						double dfx = f0 * dx;
						double dfy = f0 * dy;

						//    !---- forces -----

// 						fo.x(i) += dfx;
// 						fo.y(i) += dfy;
// 
// 						fo.x(j) -= dfx;
// 						fo.y(j) -= dfy;
// 
// 						fo.e(i) += e0;
// 						fo.e(j) += e0;

						f_temp_par.x[n] = dfx;
						f_temp_par.y[n] = dfy;
						f_temp_par.e[n] = e0;



						//    !--- virials -----

// 						F.virial(0,0) += dfx*dx_tilde*length0_x;  
// 						F.virial(0,1) += dfx*dy_tilde*length0_y;  
// 
// 						F.virial(1,0) += dfy*dx_tilde*length0_x;  
// 						F.virial(1,1) += dfy*dy_tilde*length0_y; 
					}
//				 }
 				 });
				 
				//parallel needed
				for(int n=0;n<pairing::n_pair;n++)
				{
				   int i = environment::Parameters::getInstance().nbl_i[n] ;
				   int j = environment::Parameters::getInstance().nbl_j[n] ;

				   fo.x(i)  += f_temp_par.x[n];
				   fo.y(i)  += f_temp_par.y[n];
   
				   fo.x(j)  -= f_temp_par.x[n];
				   fo.y(j)  -= f_temp_par.y[n];
   
				   fo.e(i)  += f_temp_par.e[n];   
				   fo.e(j)  += f_temp_par.e[n];   
				   
				}
//    

				for(int i=0;i<n_atom;i++)
				{ 
					f_temp.x[i] = inv_visco_x * ( fo.x(i)*F.grad_def(0,0)*length0_x     
					+ fo.y(i)*F.grad_def(1,0)*length0_x )*dt
					+ sqrt(inv_visco_x) * amp_noise*(rf.x[i]) ;  
					
					f_temp.y[i] = inv_visco_y * ( fo.x(i)*F.grad_def(0,1)*length0_y     
					+ fo.y(i)*F.grad_def(1,1)*length0_y )*dt
					+ sqrt(inv_visco_y) * amp_noise*(rf.y[i]) ; 
				}

				for(int i=0;i<n_atom;i++)
				{ 
					fo.x(i) = -f_temp.x[i] ;
					fo.y(i) = -f_temp.y[i] ; 
					rf_t.x[i] = - sqrt(inv_visco_x) * amp_noise*(rf.x[i]) ;
					rf_t.y[i] = - sqrt(inv_visco_y) * amp_noise*(rf.y[i]) ;
					
				}



			}			
			
			
	};


}








void initiate_tensors(tensors& F);
void initiate_fo(int n) ;
void find_cells(tilde_coordinates&  c );
void neighbor_list_seq(tilde_coordinates&  tilde_c, tensors& F );
std::vector<double> sum_of_sq_mn(std::vector<double>& ,std::vector<double>& );

double maxval(std::vector<double>& );
double minval(std::vector<double>& );
void transform_to_original_coordinates(coordinates&  c, tilde_coordinates&  tilde_c, tensors& F );


std::vector<double>  wiener();

double rosen (const column_vector& m);




column_vector rosen_derivative(const column_vector& m);


matrix<double> rosen_hessian (const column_vector& m);


// class rosen_model 
// {
//     /*!
//         This object is a "function model" which can be used with the
//         find_min_trust_region() routine.  
//     !*/
// 
// public:
//     typedef ::column_vector column_vector;
//     typedef matrix<double> general_matrix;
// 
//     double operator() (
//         const column_vector& x
//     ) const { return rosen(x); }
// 
//     void get_derivative_and_hessian (
//         const column_vector& x,
//         column_vector& der,
//         general_matrix& hess
//     ) const
//     {
//         der = rosen_derivative(x);
//         hess = rosen_hessian(x);
//     }
// };



#endif //
