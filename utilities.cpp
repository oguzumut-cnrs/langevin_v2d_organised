#include "utilities.h"
#include "common.h"
#include "common2.h"
// namespace bi=box_informations;
// namespace sp=simulation_parameters;

//namespace bi=box_informations;
// namespace sp=simulation_parameters;




void box_calculations(tensors& F)
{
	matrix2D grad_def_inv=inv(F.grad_def); 

	double length0_x  =lengths::length0_x;
	double length0_y  =lengths::length0_y;
	double cell_length=info_2D::cell_length;

	double cell_length_x_tilde = cell_length * sqrt(pow(grad_def_inv(0,0),2.) + pow(grad_def_inv(0,1),2.) ) / length0_x;
	double cell_length_y_tilde = cell_length * sqrt(pow(grad_def_inv(1,0),2.) + pow(grad_def_inv(1,1),2.) ) / length0_y;


	int nb_cell_x = int( 1.0 / cell_length_x_tilde );
	int nb_cell_y = int( 1.0 / cell_length_y_tilde );

	cell_length_x_tilde = 1.0/nb_cell_x;
	cell_length_y_tilde = 1.0/nb_cell_y; 

    box_informations::setNum_cell(cell_length_x_tilde,cell_length_y_tilde);

	int n_atom_per_cell = 3 * atom_number::n_atom / ((double ) nb_cell_x*nb_cell_y);
	int n_pair_max = info_2D::n_pair_max_per_atom * atom_number::n_atom;
	
	box_informations::setNum_cell_nb(nb_cell_x,nb_cell_y,n_atom_per_cell,n_pair_max);
    
}

void initiate_tensors(tensors& F)
{
	
	
    F.grad_def(0,0) = environment::Parameters::getInstance().F.grad_def(0,0); 
    F.grad_def(1,1) = environment::Parameters::getInstance().F.grad_def(1,1); 
    F.grad_def(0,1) = environment::Parameters::getInstance().F.grad_def(0,1); 
    F.grad_def(1,0) = environment::Parameters::getInstance().F.grad_def(1,0); 

	
	F.virial(0,0) = 0.;
	F.virial(1,1) = 0.;
	F.virial(0,1) = 0.;
	F.virial(1,0) = 0.;
	
	F.pk1(0,0) = 0.;
	F.pk1(1,1) = 0.;
	F.pk1(0,1) = 0.;
	F.pk1(1,0) = 0.;	
	
	
	F.cofactor(0,0) = 0.;
	F.cofactor(1,1) = 0.;
	F.cofactor(0,1) = 0.;
	F.cofactor(1,0) = 0.;	
	
	
}

int check_tensors(tensors& F)
{
	int i=0;
	tensors F2;
	F2.grad_def(0,0) = 1.;
	F2.grad_def(1,1) = 1.;
	F2.grad_def(0,1) = 0.;
	F2.grad_def(1,0) = 0.;
	
	if(F2.grad_def(0,0)-F.grad_def(0,0) != 0 )
		i=1;
	if(F2.grad_def(1,1)-F.grad_def(1,1) != 0 )
		i=1;
	if(F2.grad_def(0,1)-F.grad_def(0,1) != 0 )
		i=1;
	if(F2.grad_def(1,1)-F.grad_def(1,1) != 0 )
		i=1;
	
	return i;

}

string IntToStr(int n) 
{
	stringstream result;
	result << 10000+n;
	return result.str();
}

string IntToStr_normal(int n) 
{
	stringstream result;
	result << n;
	return result.str();
}
string RealToStr_normal(double n) 
{
	stringstream result;
	result << n;
	return result.str();
}

void write_to_a_file(coordinates& c, int t)
{
//    ofstream metrics,disp,filestr;
//    string filename,filename2;

//    filename="dir_config/config_" + IntToStr(t) +".txt";
//    filename="dir_config/config_" + IntToStr(t) ;
//    filename2="dir_config_u/config_" + IntToStr(t) ;

//    cout<< filename << "  \n";
//    metrics.open(filename.c_str());
//    disp.open(filename2.c_str());
// 
//    for (int k=0;k<3; k++){
//    for (int i=0;i<c.x.size();i++){
//     if(k ==0){
//     disp << std::scientific << std::setprecision(7) << c.x[i] << " ";
//     disp  << "  \n";}
//     if(k ==1){
//     disp << std::scientific << std::setprecision(7) << c.y[i] << " ";
//     disp  << "  \n";}
//     }
//     }
//         
//   disp.close();



	string filename ="./dir_coo/for_ovito_" + IntToStr(t)+".xyz" ;

	ofstream filestr;
	filestr.open(filename.c_str());
	
	filestr << c.x.size()<<endl;	
	filestr << " "<<endl;	

	for(int i=0;i<c.x.size();i++)
	{
		filestr << std::scientific << std::setprecision(7)
			    << "1 "
				<<c.x[i]<<" "
				<<c.y[i]<<" "
				<<force_energy_relax::calculation::getInstance().fo.e(i)<<endl;
	}




}


void write_to_a_file_en(coordinates& c, forces_std& fo, int t)
{
//    ofstream metrics,disp,filestr;
//    string filename,filename2;

//    filename="dir_config/config_" + IntToStr(t) +".txt";
//    filename="dir_config/config_" + IntToStr(t) ;
//    filename2="dir_config_u/config_" + IntToStr(t) ;

//    cout<< filename << "  \n";
//    metrics.open(filename.c_str());
//    disp.open(filename2.c_str());
// 
//    for (int k=0;k<3; k++){
//    for (int i=0;i<c.x.size();i++){
//     if(k ==0){
//     disp << std::scientific << std::setprecision(7) << c.x[i] << " ";
//     disp  << "  \n";}
//     if(k ==1){
//     disp << std::scientific << std::setprecision(7) << c.y[i] << " ";
//     disp  << "  \n";}
//     }
//     }
//         
//   disp.close();



	string filename ="./dir_coo_e/for_ovito_" + IntToStr(t)+".xyz" ;

	ofstream filestr;
	filestr.open(filename.c_str());
	
	filestr << c.x.size()<<endl;	
	filestr << " "<<endl;	

	for(int i=0;i<c.x.size();i++)
	{
		filestr << std::scientific << std::setprecision(7)
// 			    << "1 "
				<<c.x[i]<<" "
				<<c.y[i]<<" "
				<<fo.x[i]<<" "
				<<fo.y[i]<<" "
				<<fo.e[i]<<endl;
	}




}


void write_to_a_file_en_2(coordinates& c, forces& fo, int t)
{
//    ofstream metrics,disp,filestr;
//    string filename,filename2;

//    filename="dir_config/config_" + IntToStr(t) +".txt";
//    filename="dir_config/config_" + IntToStr(t) ;
//    filename2="dir_config_u/config_" + IntToStr(t) ;

//    cout<< filename << "  \n";
//    metrics.open(filename.c_str());
//    disp.open(filename2.c_str());
// 
//    for (int k=0;k<3; k++){
//    for (int i=0;i<c.x.size();i++){
//     if(k ==0){
//     disp << std::scientific << std::setprecision(7) << c.x[i] << " ";
//     disp  << "  \n";}
//     if(k ==1){
//     disp << std::scientific << std::setprecision(7) << c.y[i] << " ";
//     disp  << "  \n";}
//     }
//     }
//         
//   disp.close();



	string filename ="./dir_coo_e_cg/for_ovito_" + IntToStr(t)+".xyz" ;

	ofstream filestr;
	filestr.open(filename.c_str());
	
	filestr << c.x.size()<<endl;	
	filestr << " "<<endl;	

	for(int i=0;i<c.x.size();i++)
	{
		if(force_energy::calculation::getInstance().c.boundary[i]==1)
			fo.x(i)=0;
		if(force_energy::calculation::getInstance().c.boundary[i]==1)
			fo.y(i)=0;
		if(force_energy::calculation::getInstance().c.boundary[i]==1)
			fo.e(i)=0;
		
		filestr << std::scientific << std::setprecision(16)
// 			    << "1 "
				<<c.x[i]<<" "
				<<c.y[i]<<" "
				<<fo.x(i)<<" "
				<<fo.y(i)<<" "
				<<fo.e(i)<<endl;
	}




}

void write_to_a_file_en_3(coordinates& c, double &func, alglib::real_1d_array &grad, int t)
{



	string filename ="./CG/for_ovito_" + IntToStr(t)+".xyz" ;

	ofstream filestr;
	filestr.open(filename.c_str());
		int n=atom_number::n_atom;

	
	filestr << c.x.size()<<endl;	
	filestr << " "<<endl;	

	for(int i=0;i<c.x.size();i++)
	{
// 		if(force_energy::calculation::getInstance().c.boundary[i]==1)
// 			fo.x(i)=0;
// 		if(force_energy::calculation::getInstance().c.boundary[i]==1)
// 			fo.y(i)=0;
// 		if(force_energy::calculation::getInstance().c.boundary[i]==1)
// 			fo.e(i)=0;
		
		filestr << std::scientific << std::setprecision(16)
// 			    << "1 "
				<<c.x[i]<<" "
				<<c.y[i]<<" "
				<<grad[i]<<" "
				<<grad[i+n]<<" "
				<<func<<endl;
	}




}
void read_from_a_file(coordinates& c)
{
	ifstream metrics,disp;
	string filename2;

	//    filename2=reading::filename2;
	//    filename2="/Users/salman/deneme/memory.dat";
	filename2=memory_file::filename;
	cout<<"reading file "<<filename2<<endl;

	ifstream file;
	file.open(filename2.c_str());

	string line;
	int count=0;
	while (getline(file, line))
	count++;
	cout << "Numbers of lines in the file : " << count << endl;
	file.close();



	double temp;

	//    cout<< filename << "  \n";
	disp.open(filename2.c_str());

	c.x.resize( 0 );
	c.y.resize( 0 );

	for (int k=0;k<3; k++)
	{
		for (int i=0;i<count/2;++i)
		{
			if(k ==0)
			{
				disp >>temp;
		//     	disp >> c.x[i];
				c.x.push_back(temp);
	}
			if(k ==1)
			{
				disp >>temp;
		//     	disp >> c.y[i];
		c.y.push_back(temp);
			}
		}
	}

	disp.close();
	
	cout<<"max new  ="<<*max_element(c.x.begin(), c.x.end()) <<endl;
    cout<<"max new  ="<<*max_element(c.y.begin(), c.y.end()) <<endl;
	
	

}


//DEVELOP MORE HERE
///////////////////
void read_from_a_file_lengths(tensors& F)
{
	ifstream metrics,disp;
	string filename2,filename3;

	//    filename2=reading::filename2;
	//    filename2="/Users/salman/deneme/memory.dat";
	
// 	filename2="/Users/salman/try/grad_def.dat";
	filename2=memory_file::filename_graddef;

	

	cout<<"reading file "<<filename2<<endl;



	disp.open(filename2.c_str());

	
	double temp;
// 	disp >>F.grad_def(0,0) ;
// 	disp >>F.grad_def(1,1) ;
// 	disp >>temp ;
// 	disp >>F.grad_def(0,1) ;
// 	disp >>F.grad_def(1,0) ;

	disp >> environment::Parameters::getInstance().F.grad_def(0,0) ;
    disp >> environment::Parameters::getInstance().F.grad_def(1,1) ;
	disp >> temp ;
	disp >> environment::Parameters::getInstance().F.grad_def(0,1) ;
    disp >> environment::Parameters::getInstance().F.grad_def(1,0) ;
    
    F.grad_def(0,0) = environment::Parameters::getInstance().F.grad_def(0,0); 
    F.grad_def(1,1) = environment::Parameters::getInstance().F.grad_def(1,1); 
    F.grad_def(0,1) = environment::Parameters::getInstance().F.grad_def(0,1); 
    F.grad_def(1,0) = environment::Parameters::getInstance().F.grad_def(1,0); 

	
	cout<<"F(0,0)= "<<F.grad_def(0,0)<<endl;
	cout<<"F(1,1)= "<<F.grad_def(1,1)<<endl;
	cout<<"F(0,1)= "<<F.grad_def(0,1)<<endl;
	cout<<"F(1,0)= "<<F.grad_def(1,0)<<endl;


	disp.close();


// 	filename3="/Users/salman/try/lengths.dat";
	filename3=memory_file::filename_lengths;

	cout<<"reading file "<<filename3<<endl;

	disp.open(filename3.c_str());
	double l1,l2;
	disp >>l1 ;
	disp >>l2 ;
	cout<<"l1= "<<l1<<endl;
	cout<<"l2= "<<l2<<endl;
	
	lengths::setNum(l1,l2);
	disp.close();
	
// write(14,666) grad_def(1,1)
// write(14,666) grad_def(2,2)
// write(14,666) grad_def(3,3)
// write(14,666) grad_def(1,2)
// write(14,666) grad_def(2,1)
// write(14,666) grad_def(1,3)
// write(14,666) grad_def(3,1)
// write(14,666) grad_def(2,3)
// write(14,666) grad_def(3,2)
		
}

///////////////////

double maxval(std::vector<double>& m)
{
    double max;
    max  = *max_element(m.begin(), m.end());
    return max;

}

double minval(std::vector<double>& m)
{
    double min;
    min  = *min_element(m.begin(), m.end());
    return min;

}

std::vector<double> sum_of_sq_mn(std::vector<double>& m,std::vector<double>& n)
{
	
	std::vector<double> delta_r_sq;
	delta_r_sq.resize(m.size());
	
	for (int i=0;i<m.size();i++)
	{
		delta_r_sq[i] = pow(m[i],2.)+pow(n[i],2.);
	}
	
	return delta_r_sq;
}

void test_knn2(coordinates& c,int nn)
{
	std::vector<matrix<double,2,1> > samples;
	matrix<double,2,1> test;


	for (int i=0; i<c.x.size(); i++) 
	{
	  test = c.x[i],c.y[i];
	  samples.push_back(test);
	}

	std::vector<sample_pair> edges;
	clock_t t;
	t = clock();
	find_k_nearest_neighbors(samples, squared_euclidean_distance(), 4, edges);
    t = clock() - t;
    printf ("It took me  (%f seconds) to find_k_nearest_neighbors.\n",((float)t)/CLOCKS_PER_SEC);

	std::sort(edges.begin(), edges.end(), &order_by_index<sample_pair>);
	
// 	for(int i=0;i<edges.size();++i)cout<<edges[i].distance()<<endl;
// 	for(int i=0;i<edges.size();++i)cout<<edges[i].index1()<<" "<<edges[i].index2()<<endl;

 
}


// #include "kdtree2.hpp"
// #include "kdtree2.cpp"
// typedef boost::multi_array<float,2> array2dfloat;
// 
// bool find_nn(coordinates& c,int nn)
// {
// 	//REQUIRED FOR KDTREE
//     kdtree2::KDTree* tree;
//     kdtree2::KDTreeResultVector res;  
//     array2dfloat realdata; 
//     realdata.resize(boost::extents[c.x.size()][2]);   
//     //LOCAL
//     bool check=false;  
//     
// 	for (int i=0; i<c.x.size(); i++) 
// 	{
// 	  realdata[i][0] = c.x[i];
// 	  realdata[i][1] = c.y[i];      
// 	}
// 	
// 	tree =  new kdtree2::KDTree(realdata,true);
// 	tree->sort_results = true;
// 	std::vector<float> query(2); 
//     kdtree2::KDTreeResultVector result;
// 
// 	tree->n_nearest(query,nn,result);
// 	 
// 	
// 	for (int i=0; i<c.x.size(); i++)
// 	{
// 	query[0]=c.x[i];
// 	query[1]=c.y[i];
// 	tree->n_nearest(query,nn,result);
// 	int keep;
// 	for (int k=1;k<=6;k++)
// 	{
// 		if(sqrt(result[k].dis) < 0.4 ) 
// 		{
// 			check=true;
// 			keep=k;
// 			cout<<"check : "<<check<<"i= "<<i<<"  "<<sqrt(result[k].dis)<<endl;
// 	// 		cout<<" x = "<<c.x[i]<<endl;
// 	// 		cout<<" y = "<<c.y[i]<<endl;
// 
// 		}
// 		if(check) break;
// 	}	
// // 	if(check)
// // 	{
// // 	cout<<"atom = "<< i<< " kth nn distance = "<< result[keep].dis<<" ind =  "<< result[keep].idx<<endl;
// // // 	cout<<"atom = "<< i<< " 2th nn distance = "<< result[2].dis<<" ind =  "<< result[2].idx<<endl;
// // // 	cout<<"atom = "<< i<< " 3th nn distance = "<< result[3].dis<<" ind =  "<< result[3].idx<<endl;
// // // 	cout<<"atom = "<< i<< " 4th nn distance = "<< result[4].dis<<" ind =  "<< result[4].idx<<endl;
// // // 	cout<<"atom = "<< i<< " 5th nn distance = "<< result[5].dis<<" ind =  "<< result[5].idx<<endl;
// // // 	cout<<"atom = "<< i<< " 6th nn distance = "<< result[6].dis<<" ind =  "<< result[6].idx<<endl;
// // 	}
// // 	cout<<"atom = "<< i<< " 7th nn distance = "<< result[7].dis<<" ind =  "<< result[7].idx<<endl;
// // 	cout<<"atom = "<< i<< " 8th nn distance = "<< result[8].dis<<" ind =  "<< result[8].idx<<endl;
// 
// 	}
// 	delete tree;
// 	
// 	return check;
// 
// }
// 
	
	



void coarse_grain(tilde_coordinates& tilde_c,coordinates& c, double angle)
{

	matrix2D grad_def;
	matrix<double> eigenvalues(2,1);
	double e0=0;
	double alpha=-5.;
	double theta_d = angle*dlib::pi/180.;
	double r_cutoff_sq = pow(info_2D::r_cutoff,2.0);
	struct tensors F;
	coordinates c3;
	tilde_coordinates  tilde_c2, c_dx_tilde;
	int k=3;

     
	string  filename="alpha_energy_angle" + IntToStr(angle) +".dat";
	
	
	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 

    fstream energy_stream;
	energy_stream.open (filename, fstream::in | fstream::out | fstream::app);

	struct shearing_vectors base1= create_vectors(c,angle);

	
// 	for(int k=0;k<400;++k){
	while(alpha<5){
// 		eigenvalues  = real_eigenvalues(grad_def);

// 		std::cout<<"grad_def(1,0)= "<<grad_def(1,0)<<std::endl;	
// 		
// 		std::cout<<"first eigenvalue of grad def is: "<<eigenvalues(0,0)<<std::endl;	
// 		std::cout<<"second eigenvalue of grad def is: "<<eigenvalues(1,0)<<std::endl;
   		F = shear_using_dual_lattice(base1,alpha);   
	

		
		transform_to_original_coordinates(c,   tilde_c,  F );		
		
		
		for(int i=0;i<tilde_c.x.size();i++){
			
			for(int j=0;j<tilde_c.x.size();j++){
				if(i==j)
					continue;

					double dx_tilde = tilde_c.x[i] - tilde_c.x[j];
					double dy_tilde = tilde_c.y[i] - tilde_c.y[j];



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
				
				double dx2 = F.grad_def(0,0)*length0_x*dx_tilde + F.grad_def(0,1)*length0_y*dy_tilde ;
				double dy2 = F.grad_def(1,0)*length0_x*dx_tilde + F.grad_def(1,1)*length0_y*dy_tilde ;

// 				double dx2 = dx_tilde ;
// 				double dy2 = dy_tilde ;


// 				if(abs(dx_tilde) > 0.5) 
// 					dx_tilde -=   copysign(1., dx_tilde) ;
// 				if(abs(dy_tilde) > 0.5) 
// 					dy_tilde -=   copysign(1., dy_tilde) ;
// 
// 				double dx2 = grad_def(0,0)*dx  + grad_def(0,1)*dy ;
// 				double dy2 = grad_def(1,0)*dx  + grad_def(1,1)*dy ;
				double r2 =  dx2*dx2 + dy2*dy2 ;
				if( r2 >= r_cutoff_sq )
					continue;
				double r = sqrt(r2);
				
				e0 +=  environment::Parameters::lennard_jones_energy(r);

			}
			
		}
		write_to_a_file(c,k++);
		e0/= tilde_c.x.size();
		e0/= 2.;
		energy_stream<<std::scientific << std::setprecision(7)<<alpha <<" "<<e0<<endl;
		alpha+=0.1;
		e0=0;
	}
	
	energy_stream.close();

	



}


struct shearing_vectors  create_vectors(const struct coordinates& c, double angle){
    
    
	int nx = environment::Parameters::getInstance().nx;
	int ny = environment::Parameters::getInstance().ny;
	double theta = toRadians(angle);
    
    
//     double f11  = abs(c.x[ny/2*nx+nx/2+1]   -  c.x[(ny/2)*nx+nx/2]);
// 	double f21  = abs(c.y[ny/2*nx+nx/2+1]   -  c.y[(ny/2)*nx+nx/2]);
// 	
// 	double f12  = abs(c.x[(ny/2+1)*nx+nx/2]   -  c.x[(ny/2)*nx+nx/2]);
// 	double f22  = abs(c.y[(ny/2+1)*nx+nx/2]   -  c.y[(ny/2)*nx+nx/2]);
// 
// 
// 	cout<<"f11_c: "<<f11<<endl;
// 	cout<<"f22_c: "<<f22<<endl;
// 	cout<<"f12_c: "<<f12<<endl;
// 	cout<<"f21_c: "<<f21<<endl;



    double f11  = 1.;
	double f21  = 0.;
	
	double f12  = 0.;
	double f22  = 1.;

	
	cout<<"f11: "<<f11<<endl;
	cout<<"f22: "<<f22<<endl;
	cout<<"f12: "<<f12<<endl;
	cout<<"f21: "<<f21<<endl;

    
    dlib::matrix<double> co1(2,1),co2(2,1);

	//lattice vectors
	co1(0,0)=f11;
	co1(1,0)=f21;
	co2(0,0)=f12;
	co2(1,0)=f22;
	
	dlib::matrix<double> co_mform(2,2), inv_co_mform(2,2);
	//matrixform
	co_mform(0,0) = co1(0,0);
	co_mform(1,0) = co1(1,0);
	co_mform(0,1) = co2(0,0);
	co_mform(1,1) = co2(1,0);
	//inverse matrixform
	inv_co_mform = inv(co_mform);
	dlib::matrix<double> dual1(2,1),dual2(2,1);


	dual1(0,0) = inv_co_mform(0,0);
	dual1(1,0) = inv_co_mform(0,1);
	
	dual2(0,0) = inv_co_mform(1,0);
	dual2(1,0) = inv_co_mform(1,1);
	
	cout<<"check1 identity= "<<dot(co1,dual1)<<endl;
	cout<<"check2 identity= "<<dot(co2,dual2)<<endl;
	
	
	dlib::matrix<double> ad(2,1), nd(2,1),h1(2,1),h2(2,1);
	dlib::matrix<double> ar(2,1), nr(2,1);
	
	// GLIDING PLANE
	ad(0,0) = co1(0,0) ;
	ad(1,0) = co1(1,0) ;
	
	nr(0,0) = cos(theta)*dual2(0,0) - sin(theta)*dual2(1,0);
	nr(1,0) = sin(theta)*dual2(0,0) + cos(theta)*dual2(1,0);
	ar(0,0) = cos(theta)*ad(0,0) -sin(theta)*ad(1,0);
	ar(1,0) = sin(theta)*ad(0,0) + cos(theta)*ad(1,0);

	cout<<"check zero= "<<dot(nr,ar)<<endl;
	cout<< "nr="<<nr(0,0)<<" "<< nr(1,0)<<endl;
	cout<< "ar="<<ar(0,0)<< " " << ar(1,0)<<endl;

	
	shearing_vectors base1;
	base1.set_values();	
	base1.nr=nr;
	base1.ar=ar;
	return base1;

}


struct tensors shear_using_dual_lattice(const struct shearing_vectors& base1,double alpha){
	
	dlib::matrix<double>shear(2,2),identity(2,2);


	identity(0,0) = 1;
	identity(1,1) = 1;
	identity(0,1) = 0;
	identity(1,0) = 0;
	dlib::matrix<double> nr = base1.nr;
	dlib::matrix<double> ar = base1.ar;



	shear = identity + alpha*trans(dlib::tensor_product(ar,nr));
	struct tensors F;
	F.grad_def(0,0) = shear(0,0);
	F.grad_def(1,0) = shear(1,0);
	F.grad_def(0,1) = shear(0,1);
	F.grad_def(1,1) = shear(1,1);

	F.grad_def(0,1) = 0.*sinh(alpha);
	F.grad_def(1,0) = 0.*sinh(alpha);
	F.grad_def(0,0) = cosh(alpha/2.) - sinh(alpha/2.);
	F.grad_def(1,1) = cosh(alpha/2.) + sinh(alpha/2.);


	return F;

}





std::vector<double>  wiener(boost::mt19937& rng)
{

	int N    =   atom_number::n_atom;
	static int k = 0;

	std::vector<double> r;
	r.resize(N);


  boost::normal_distribution<> nd(0.0, 1.0);

  boost::variate_generator<boost::mt19937&, 
                           boost::normal_distribution<> > var_nor(rng, nd);



    for (int i = 0; i < N; ++i)
    {
        r[i]  = var_nor();
    }

// 	cout<<maxval(r)<<endl;

	return r;
    
}



double coarse_grain_spline(tilde_coordinates& tilde_c,coordinates& c, matrix2D& U)
{

	matrix2D grad_def;
	double e0=0;
	double r_cutoff_sq = pow(info_2D::r_cutoff,2.0);
	struct tensors F;

     
	
	
	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 
	double detU = U(0,0)*U(1,1)-U(0,1)*U(1,0);

 		F.grad_def(0,1)	= U(0,1);
 		F.grad_def(1,1)	= U(1,1);
 		F.grad_def(1,0)	= U(1,0);
 		F.grad_def(0,0)	= U(0,0);

		for(int i=0;i<tilde_c.x.size()-1;i++){
			for(int j=i+1;j<tilde_c.x.size();j++){
				if(i==j)
					continue;

				double dx_tilde = tilde_c.x[i] - tilde_c.x[j];
				double dy_tilde = tilde_c.y[i] - tilde_c.y[j];



				if(abs(dx_tilde) > 0.5) 
					dx_tilde -=   copysign(1., dx_tilde) ;
				if(abs(dy_tilde) > 0.5) 
					dy_tilde -=   copysign(1., dy_tilde) ;
				

				double dx2 = F.grad_def(0,0)*length0_x*dx_tilde + F.grad_def(0,1)*length0_y*dy_tilde ;
				double dy2 = F.grad_def(1,0)*length0_x*dx_tilde + F.grad_def(1,1)*length0_y*dy_tilde ;


				double r2 =  dx2*dx2 + dy2*dy2 ;
				if( r2 >= r_cutoff_sq )
					continue;
				double r = sqrt(r2);
				
				e0 +=  environment::Parameters::lennard_jones_energy(r);

			}
		}
		
		e0/= tilde_c.x.size();
	
		return e0;	



}