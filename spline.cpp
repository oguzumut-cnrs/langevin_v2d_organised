#include <iostream>
#include "datatable.h"
#include "bspline.h"
#include "spline.h"
using std::cout;
using std::endl;

#include <dlib/matrix.h>

#include "dlib/threads.h"
#include <dlib/optimization.h>
#include "dlib/graph_utils_threaded.h"
#include "dlib/threads.h"
#include <dlib/gui_widgets.h>
#include <dlib/image_transforms.h>

using namespace dlib;

#include <bsplinebuilder.h>

using namespace SPLINTER;

// double f(DenseVector x)
// {
// 	
// 	double r = x(0);
// 
// 	double f0 = pow(1./r,8.)- 2.*pow((1./r),4);
// 
//  
// 	return f0;
// 
// }
double f(double r)
{
	

	double f0 = pow(1./r,8.)- 2.*pow((1./r),4);

 
	return f0;

}



tk::spline  spline(void)
{
  
    // Create new DataTable to manage samples
//     DataTable samples,samples2,samples3;
    std::vector<double> x,y;
    double start = 0.0001;

    // Sample the function
//     DenseVector x(1);
  
//     		for(double j = 0.01; j <= 8; j = j+ 0.001)
    		for(int j =0 ; start <=4 ; ++j)

        	{
        
				// Sample function at x
				
				x.push_back(start);
				y.push_back(f(x[j]));
				start += 0.000001;
// 				x(0) = j;
// 				double y = f(x);
// 				samples.addSample(x,y);
				
			}


			tk::spline s;
			s.set_points(x,y);    // currently it is required that X is already sorted

		   double xx=1.5;
		   cout<<"interpolation: "<<endl;


		   printf("spline at %f is %f\n", xx, s(xx));
		   printf("function at %f is %f\n", xx, f(xx));

//     BSpline bspline3 = BSpline::Builder(samples).degree(3).build();
//     BSpline bspline3 = BSpline::Builder(samples).degree(1).build();




// 	const char *fileName = "enlin.bspline";
// 	bspline3.save(fileName);
// 	BSpline loadedBSpline(fileName);


//  return loadedBSpline;
	return  s;
}




