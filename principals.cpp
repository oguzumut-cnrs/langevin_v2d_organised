
#include "principals.h"

// namespace gp = grain_properties;
// namespace bi = box_informations;

int ind_conv(int nx,int ny,int nz,int i, int j, int k)
{
	int offset;
// 	offset = nx*ny*k + nx*j + i;
	if(nz == 0)
		offset = i*ny + j;
	if(nz != 0)
		offset = i * ny * nz + j * nz + k;
	return offset;
}


// int ind_conv2D(int nx,int ny,int i, int j)
// {
// 	int offset;
// 	offset =i*ny + j;
// 	return offset;
// }
// 
// int ind_conv3D(int nx,int ny,int nz,int i, int j, int k)
// {
// 	int offset;
// // 	offset =i*ny + j;
// 	offset = i * ny * nz + j * nz + k;
// 	return offset;
// }


//FINDING CELLS USING TILDE COORDINATES
void find_cells(tilde_coordinates&  c )
{

	double zero=1.0e-08;
 
// 	int index_for_deg=0;
// 	int ix,iy;

	for(int k=0;k<environment::Parameters::getInstance().deg_cell.size();k++)   
		environment::Parameters::getInstance().deg_cell(k)=0 ;
	for(int k=0;k<environment::Parameters::getInstance().cont_cell.size();k++) 
		environment::Parameters::getInstance().cont_cell(k)=0 ;   
		  
	int m= box_informations::nb_cell_x;     
	int nx=box_informations::n_atom_per_cell;
// 		cout<< " ix :  "<<bi::nb_cell_x<<" "<<" iy = "<< bi::nb_cell_y<<endl; 

	for (int n=0;n<atom_number::n_atom;n++)
	{

		int ix = (int) (c.x[n]/(box_informations::cell_length_x_tilde+zero));
		int iy = (int) (c.y[n]/(box_informations::cell_length_y_tilde+zero));

// 		cout<< " ix :  "<<ix<<" "<<" iy = "<< iy<<" "<<c.x[n]<<" "<< c.y[n]<<
// 		" "<<bi::cell_length_x_tilde<<endl; 

		if( ix < 0  || ix > box_informations::nb_cell_x-1 ){
			cout<< "Problem 1: ix= "<<ix<<" \n" ;
			cout<< "Problem 1: nb_cell_x= "<<box_informations::nb_cell_x<<" \n" ;
			cout<< "exiting from the code" <<"\n" ;
			//        cout<< "Problem : ix = "<<ix<< "x_tilde(n) = "<<x_tilde(n)<<"cell_length_x_tilde "<<bi.cell_length_x_tilde<<"\n";
			exit(1);
		}

		if(iy < 0  || iy > box_informations::nb_cell_y-1)
		{
			cout<< "Problem 2: iy= "<<ix<<" \n" ;
			cout<< "Problem 2: nb_cell_y= "<<box_informations::nb_cell_y<<" \n" ;
			exit(1);
		}


		environment::Parameters::getInstance().ind_cell_x(n) = ix;
		environment::Parameters::getInstance().ind_cell_y(n) = iy;


// 		int index_for_deg= iy*m + ix ;  //from double indice notation (ix,iy) to one indice notation
		int index_for_deg = ind_conv(box_informations::nb_cell_x,box_informations::nb_cell_y,0,ix,iy,0);
		environment::Parameters::getInstance().deg_cell(index_for_deg)+=1;
		//    
		if(environment::Parameters::getInstance().deg_cell(index_for_deg) > box_informations::n_atom_per_cell )
		{
			cout<<"problem in deg_cell"<<"\n";
			exit(1);   
		}

// 		int index_for_cont= (iy)*m*nx +  (ix)*nx + environment::Parameters::getInstance().deg_cell(index_for_deg)-1;
		int k = environment::Parameters::getInstance().deg_cell(index_for_deg);
		int index_for_cont= ind_conv(box_informations::n_atom_per_cell,box_informations::nb_cell_x,box_informations::nb_cell_y,k-1,ix,iy);
// 		cout<<"index_for_cont="<<" "<<index_for_cont<<"\n";

		environment::Parameters::getInstance().cont_cell(index_for_cont) = n;

	}
	
// for( int iy=0;iy<bi::nb_cell_y ;iy++){
// for( int ix=0;ix<bi::nb_cell_x ;ix++){
// // index_for_deg= iy*m + ix ;
// int index_for_deg = ind_conv(bi::nb_cell_x,bi::nb_cell_y,0,ix,iy,0);
// 
// cout<<" cell "<<ix<<" "<<iy<<" "<<"deg = "<<environment::Parameters::getInstance().deg_cell(index_for_deg)<<"\n";  
// }
// }
// cout<<" ---- "<<"\n"; 
// 
// for( int iy=0;iy<bi::nb_cell_y ;iy++){
// for( int ix=0;ix<bi::nb_cell_x ;ix++){
// 
// int index_for_deg = ind_conv(bi::nb_cell_x,bi::nb_cell_y,0,ix,iy,0);
// 
// for( int k=0;k<environment::Parameters::getInstance().deg_cell(index_for_deg) ;k++){
// int r= ind_conv(bi::n_atom_per_cell,bi::nb_cell_x,bi::nb_cell_y,k,ix,iy);
// cout<<" cell "<<ix<<" "<<iy<<" "<<"cont("<<k<<")"<<"deg = "<<environment::Parameters::getInstance().cont_cell(r)<<"\n";  
// }
// }
// }

// do iz = 0, nb_cell_z - 1
// do iy = 0, nb_cell_y - 1
// do ix = 0, nb_cell_x - 1   
//   do k = 1, deg_cell(ix,iy,iz)
//      print *,' cell ',ix,iy,iz,' cont(',k,') = ', cont_cell(k,ix,iy,iz)  
//   end do
// end do
// end do
// end do

}

// #include </Users/salman/dlib-19.1/dlib/matrix.h>





#include <boost/math/special_functions/sign.hpp>
//FINDING neighbor_list USING TILDE COORDINATES
using namespace std;
void neighbor_list_seq(tilde_coordinates&  tilde_c, tensors& F ){


	column_vector_int  ind_x;
	column_vector_int  ind_y;
// 	std::vector<int> myvector1;
// 	std::vector<int> myvector2;

//     environment::Parameters::getInstance().nbl_i = std::vector<int>();
//     environment::Parameters::getInstance().nbl_j = std::vector<int>();

    environment::Parameters::getInstance().nbl_i.resize( 0 );
    environment::Parameters::getInstance().nbl_j.resize( 0 );
    
//     environment::Parameters::getInstance().nbl_i.clear;
//     environment::Parameters::getInstance().nbl_j.clear;
   


	ind_x.set_size(5);
	ind_y.set_size(5);

	ind_x(0) = 0  ;  ind_y(0) =  0  ; 
	ind_x(1) = 1  ;  ind_y(1) =  0  ;
	ind_x(2) = 0  ;  ind_y(2) =  1  ; 
	ind_x(3) = 1  ;  ind_y(3) =  1  ; 
	ind_x(4) = 1  ;  ind_y(4) = -1  ; 

	int m_max = 5;


	//take from namespaces
	double length0_x = lengths::length0_x;
	double length0_y = lengths::length0_y; 
	int n_atom = atom_number::n_atom;



// !----- pair identification -------------------------

	int n_pair = 0;
	double r_verlet_sq = info_2D::r_verlet*info_2D::r_verlet;

// 	cout<<"verbose n_pair_max= "<<bi::n_pair_max<<"\n";
// 	cout<<"n_atom= "<<n_atom<<"\n";
	for (int i = 0; i<n_atom;i++)
	{
		int xx = environment::Parameters::getInstance().ind_cell_x(i);
		int yy = environment::Parameters::getInstance().ind_cell_y(i);


	//    !!!boucle sur les cellules voisines de la cellule à la quelle atome #i appartient
		for (int m = 0; m<m_max;m++)
		{                    

			int cell_x = xx + ind_x(m);
			int cell_y = yy + ind_y(m);

			//        !----  periodic boundary conditions on the cells ---------------

			if (cell_x == box_informations::nb_cell_x)
				cell_x = 0;
	
			if (cell_x == -1)    
				cell_x = box_informations::nb_cell_x-1; 

			if( cell_y == box_informations::nb_cell_y )
				cell_y = 0;
		
			if( cell_y == -1)
				cell_y = box_informations::nb_cell_y-1;




// 			int index_for_deg= bi::nb_cell_x*cell_y + cell_x;  //from double indice notation (ix,iy) to one indice notation
		    int index_for_deg = ind_conv(box_informations::nb_cell_x,box_informations::nb_cell_y,0,cell_x,cell_y,0);

			for (int l = 0; l<environment::Parameters::getInstance().deg_cell(index_for_deg);l++)
			{
		        int index_for_cont= ind_conv(box_informations::n_atom_per_cell,box_informations::nb_cell_x,box_informations::nb_cell_y,l,cell_x,cell_y);

				int j = environment::Parameters::getInstance().cont_cell(index_for_cont);
				
				if(  m == 0  &&  i <= j ) continue;

				// if(  m != 0  ||  i < j ) 
				//{
					double dx_tilde = tilde_c.x[i] - tilde_c.x[j];
					double dy_tilde = tilde_c.y[i] - tilde_c.y[j];

					if( abs(dx_tilde) > 0.5 ) dx_tilde -=   copysign(1., dx_tilde) ;
					if( abs(dy_tilde) > 0.5 ) dy_tilde -=   copysign(1., dy_tilde) ;

					double dx = F.grad_def(0,0)*length0_x*dx_tilde + F.grad_def(0,1)*length0_y*dy_tilde ;
					double dy = F.grad_def(1,0)*length0_x*dx_tilde + F.grad_def(1,1)*length0_y*dy_tilde ;

					double r_sq = dx*dx + dy*dy ;

					if( r_sq < r_verlet_sq ) 
					{
						
						if(n_pair > box_informations::n_pair_max)
						{
// 							cout<<"nbr of pairs per atom in neigbor list exeed n_pair_max_per_atom "<< info_2D::n_pair_max_per_atom<<"\n";
// 							exit(1);
						}
// 						environment::Parameters::getInstance().nbl_i[n_pair]   = i;
// 						environment::Parameters::getInstance().nbl_j[n_pair]   = j;
						
						environment::Parameters::getInstance().nbl_i.push_back(i) ;
						environment::Parameters::getInstance().nbl_j.push_back(j) ;
						
						
						
						
						n_pair++;


// 				        cout<<i<<" "<<j<<"\n";
					}
				//}
			}
		}   
	}


    
//     for (int i = 0; i<100;++i){
//     	cout<<environment::Parameters::getInstance().nbl_i[i]<<" "<<
//     	      environment::Parameters::getInstance().nbl_j[i]<<endl;
//     }
    
// 	cout<<"n_pair = "<< n_pair<<endl;
// 	cout<<"r_verlet = "<< r_verlet_sq<<"\n";

// 	cout<<"size nbl_i = "<< environment::Parameters::getInstance().nbl_i.size()<<"\n";
// 	cout<<"size nbl_j = "<< environment::Parameters::getInstance().nbl_j.size()<<"\n";
	
// 	cout<<"h(0,0)= "<< F.grad_def(0,0)<<"\n";
// 	cout<<"h(1,0)= "<< F.grad_def(1,0)<<"\n";
// 	cout<<"h(0,1)= "<< F.grad_def(0,1)<<"\n";
// 	cout<<"h(1,1)= "<< F.grad_def(1,1)<<"\n";

	pairing::setNum(n_pair);

}



void transform_to_tilde_coordinates( coordinates&  c,tilde_coordinates&  tilde_c, tensors& F )
{
    int n_atom = atom_number::n_atom;
	matrix2D h;
	
	h(0,0) = F.grad_def(0,0)*c.length0_x;
	h(1,0) = F.grad_def(1,0)*c.length0_x;
	h(0,1) = F.grad_def(0,1)*c.length0_y;
	h(1,1) = F.grad_def(1,1)*c.length0_y;




	matrix2D h_inv;
	h_inv=inv(h);

	if(tilde_c.x.size() ==0)
	{
		tilde_c.x.assign (n_atom,0);
		tilde_c.y.assign (n_atom,0);	
	}

	for (int n=0;n<n_atom;n++)
	{


		tilde_c.x[n]=(h_inv(0,0)*c.x[n] + h_inv(0,1)*c.y[n]) ;
		tilde_c.y[n]=(h_inv(1,0)*c.x[n] + h_inv(1,1)*c.y[n]) ;

	}
	
// 	cout<<"maxval "<<maxval(tilde_c.x)<<endl;
// 	cout<<"minval "<<minval(tilde_c.x)<<endl;


}


void transform_to_original_coordinates(coordinates&  c, tilde_coordinates&  tilde_c, tensors& F )
{

    int n_atom = atom_number::n_atom;
	matrix2D h;
	double length0_x=lengths::length0_x;
	double length0_y=lengths::length0_y;

	h(0,0) = F.grad_def(0,0)*length0_x;
	h(1,0) = F.grad_def(1,0)*length0_x;
	h(0,1) = F.grad_def(0,1)*length0_y;
	h(1,1) = F.grad_def(1,1)*length0_y;

// 	h(:,1) = grad_def(:,1)*length0_x
// 	h(:,2) = grad_def(:,2)*length0_y
// 	h(:,3) = grad_def(:,3)*length0_z
// 
// 
//    x(n) = h(1,1)*x_tilde(n) + h(1,2)*y_tilde(n) + h(1,3)*z_tilde(n)
//    y(n) = h(2,1)*x_tilde(n) + h(2,2)*y_tilde(n) + h(2,3)*z_tilde(n)
//    z(n) = h(3,1)*x_tilde(n) + h(3,2)*y_tilde(n) + h(3,3)*z_tilde(n)


	for (int n=0;n<n_atom;n++)
	{
// 	c.x.push_back(h(0,0)*tilde_c.x[n] + h(0,1)*tilde_c.y[n]);
// 	c.y.push_back(h(1,0)*tilde_c.x[n] + h(1,1)*tilde_c.y[n]);
	c.x[n]=(h(0,0)*tilde_c.x[n] + h(0,1)*tilde_c.y[n]) ;
	c.y[n]=(h(1,0)*tilde_c.x[n] + h(1,1)*tilde_c.y[n]) ;

	}


}

void push_transform_to_original_coordinates(coordinates&  c, tilde_coordinates&  tilde_c, tensors& F )
{

    int n_atom = atom_number::n_atom;
	matrix2D h;
	double length0_x=lengths::length0_x;
	double length0_y=lengths::length0_y;

	h(0,0) = F.grad_def(0,0)*length0_x;
	h(1,0) = F.grad_def(1,0)*length0_x;
	h(0,1) = F.grad_def(0,1)*length0_y;
	h(1,1) = F.grad_def(1,1)*length0_y;

// 	h(:,1) = grad_def(:,1)*length0_x
// 	h(:,2) = grad_def(:,2)*length0_y
// 	h(:,3) = grad_def(:,3)*length0_z
// 
// 
//    x(n) = h(1,1)*x_tilde(n) + h(1,2)*y_tilde(n) + h(1,3)*z_tilde(n)
//    y(n) = h(2,1)*x_tilde(n) + h(2,2)*y_tilde(n) + h(2,3)*z_tilde(n)
//    z(n) = h(3,1)*x_tilde(n) + h(3,2)*y_tilde(n) + h(3,3)*z_tilde(n)


	for (int n=0;n<n_atom;n++)
	{
	c.x.push_back(h(0,0)*tilde_c.x[n] + h(0,1)*tilde_c.y[n]);
	c.y.push_back(h(1,0)*tilde_c.x[n] + h(1,1)*tilde_c.y[n]);
// 	c.x[n]=(h(0,0)*tilde_c.x[n] + h(0,1)*tilde_c.y[n]) ;
// 	c.y[n]=(h(1,0)*tilde_c.x[n] + h(1,1)*tilde_c.y[n]) ;

	}


}
void transform_to_original_coordinates_dlib(coordinates&  c, const column_vector  m, tensors& F )
{

    int n_atom = atom_number::n_atom;
	matrix2D h;
	double length0_x=lengths::length0_x;
	double length0_y=lengths::length0_y;

	h(0,0) = F.grad_def(0,0)*length0_x;
	h(1,0) = F.grad_def(1,0)*length0_x;
	h(0,1) = F.grad_def(0,1)*length0_y;
	h(1,1) = F.grad_def(1,1)*length0_y;


	for (int n=0;n<n_atom;n++)
	{
	c.x[n]=(h(0,0)*m(n) + h(0,1)*m(n+n_atom)) ;
	c.y[n]=(h(1,0)*m(n) + h(1,1)*m(n+n_atom)) ;

	}



}



